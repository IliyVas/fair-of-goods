let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/flatlab/js/jquery.dcjqaccordion.2.7.js', 'public/js')
    .js('resources/assets/flatlab/assets/advanced-datatable/media/js/jquery.dataTables.js', 'public/js')
    .js('resources/assets/flatlab/js/bootstrap.min.js', 'public/js')
    .js('resources/assets/flatlab/js/html5shiv.js', 'public/js')
    .js('resources/assets/flatlab/js/respond.min.js', 'public/js')
    .js('resources/assets/js/delivery-form.js', 'public/js')
    .js('resources/assets/js/products_list.js', 'public/js')
    .js('resources/assets/js/question-category-table.js', 'public/js')
    .js('resources/assets/js/question-table.js', 'public/js')
    .js('resources/assets/js/datepicker.js', 'public/js')
    .js('resources/assets/intlTelInput/js/utils.js', 'public/js')
    .js('resources/assets/js/intlTelInput_ini.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'resources/assets/sass/custom.scss'
], 'public/css/custom.css');

mix.styles([
    'resources/assets/intlTelInput/css/intlTelInput.css'
], 'public/css/phone.css');

mix.scripts([
    'resources/assets/intlTelInput/js/data.js',
    'resources/assets/intlTelInput/js/intlTelInput.js'
], 'public/js/intlTelInput.js');

mix.styles([
    'resources/assets/flatlab/css/bootstrap.min.css',
    'resources/assets/flatlab/css/bootstrap-reset.css'
], 'public/css/bootstrap.css');

mix.styles([
    'resources/assets/flatlab/css/jquery.steps.css'
], 'public/css/form.css');

mix.styles([
    'resources/assets/flatlab/assets/font-awesome/css/font-awesome.css',
    'resources/assets/flatlab/css/slidebars.css',
    'resources/assets/flatlab/css/style.css',
    'resources/assets/flatlab/css/style-responsive.css',
    'resources/assets/flatlab/assets/toastr-master/toastr.css',
    'resources/assets/flatlab/assets/bootstrap-datepicker/css/datepicker.css',
    'resources/assets/css/main.css'
], 'public/css/dashboard.css');

mix.styles([
    'resources/assets/flatlab/assets/advanced-datatable/media/css/demo_page.css',
    'resources/assets/flatlab/assets/advanced-datatable/media/css/demo_table.css',
    'resources/assets/flatlab/css/table-responsive.css',
    'resources/assets/flatlab/assets/data-tables/DT_bootstrap.css'
], 'public/css/grid.css');

mix.styles([
    'resources/assets/flatlab/assets/jquery-multi-select/css/multi-select.css'
], 'public/css/form.additional.css');

mix.scripts([
    'resources/assets/flatlab/assets/jquery-multi-select/js/jquery.multi-select.js',
    'resources/assets/flatlab/assets/jquery-multi-select/js/jquery.quicksearch.js'
], 'public/js/form.additional.js');

mix.scripts([
    'resources/assets/flatlab/js/jquery.js',
    'resources/assets/flatlab/js/jquery.scrollTo.min.js',
    'resources/assets/flatlab/js/jquery.nicescroll.js'
], 'public/js/jquery.js');

mix.scripts([
    'resources/assets/flatlab/js/respond.min.js',
    'resources/assets/flatlab/js/slidebars.min.js',
    'resources/assets/flatlab/js/common-scripts.js',
    'resources/assets/js/toastr.js',
    'resources/assets/flatlab/assets/toastr-master/toastr.js',
    'resources/assets/js/delete.js',
    'resources/assets/flatlab/assets/bootstrap-datepicker/js/bootstrap-datepicker.js',
    'resources/assets/flatlab/js/jquery.nicescroll.js'
], 'public/js/dashboard.js');

mix.scripts([
    'resources/assets/flatlab/js/jquery.steps.min.js',
    'resources/assets/flatlab/js/jquery.stepy.js',
    'resources/assets/js/product-form.js'
], 'public/js/form.js');

mix.scripts([
    'resources/assets/flatlab/js/jquery-migrate-1.2.1.min.js',
    'resources/assets/flatlab/assets/data-tables/jquery.dataTables.js',
    'resources/assets/flatlab/assets/data-tables/DT_bootstrap.js',
    'resources/assets/flatlab/js/dynamic_table_init.js'
], 'public/js/grid.js');

mix.copyDirectory('resources/assets/flatlab/assets/font-awesome/fonts', 'public/fonts');
mix.copyDirectory('resources/assets/flatlab/img', 'public/img');
mix.copyDirectory('resources/assets/flatlab/assets/data-tables/images', 'public/img');
mix.copyDirectory('resources/assets/images', 'public/img');
mix.copyDirectory('resources/assets/intlTelInput/img', 'public/img');
mix.copyDirectory('resources/assets/docs', 'public/docs');
