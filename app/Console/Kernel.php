<?php

namespace App\Console;

use App\Console\Commands\CheckUserTransactionStatus;
use App\Console\Commands\DeleteTestProducts;
use App\Console\Commands\RevertProductStatus;
use App\Console\Commands\SetDonateAfterFairFlag;
use App\Console\Commands\SetProductOwnerId;
use App\Console\Commands\DropAllTables;
use App\Console\Commands\RefreshDatabase;
use App\Console\Commands\RefreshProductData;
use App\Console\Commands\RefreshProductPrice;
use App\Console\Commands\RefreshProductStatus;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        RefreshProductStatus::class,
        RefreshProductPrice::class,
        RefreshProductData::class,
        DropAllTables::class,
        RefreshDatabase::class,
        SetProductOwnerId::class,
        SetDonateAfterFairFlag::class,
        DeleteTestProducts::class,
        RevertProductStatus::class,
        CheckUserTransactionStatus::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule
            ->command('refresh:product_status')
            ->dailyAt('23:00');
        $schedule
            ->command('refresh:product_price')
            ->dailyAt('6:00');
        $schedule
            ->command('refresh:product')
            ->everyFiveMinutes();
        $schedule
            ->command('check:transaction_status')
            ->dailyAt('7.00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
