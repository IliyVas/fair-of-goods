<?php

namespace App\Console\Commands;

use App\Http\RequestAPI\Moysklad;
use App\Models\Fair\Fair;
use App\Models\Product\Discount;
use App\Models\Product\DiscountType;
use App\Models\Product\ProductStatus;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RefreshProductPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:product_price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh product current price depending on the day of the fair';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $discounts = (new Discount)->getActiveDiscount();

        if (!is_null($discounts) && $discounts->count() > 0) {
            foreach ($discounts as $discount) {
                $products = $discount->products()->whereStatus(ProductStatus::SALE)->get();

                if ($products->count() > 0) {
                    foreach ($products as $product) {
                        $price = $product->price - $product->price * $discount->percent/100;
                        (new Moysklad())->changeSaleProductPrice($product->stock_id, $price);
                    }
                }
            }
        }
    }
}
