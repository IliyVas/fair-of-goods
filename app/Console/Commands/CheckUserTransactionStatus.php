<?php

namespace App\Console\Commands;

use App\Http\Controllers\MoneyController;
use App\Models\UserTransaction;
use App\Models\UserTransactionStatus;
use Illuminate\Console\Command;

class CheckUserTransactionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:transaction_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check users transactions statuses.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $moneyController = new MoneyController();
        $transactions = UserTransaction::whereStatus(UserTransactionStatus::TAKENIN_NOTSENT)->get();

        if (count($transactions) > 0) {
            foreach ($transactions as $transaction) {
                if (!is_null($transaction->number)) {
                    $operation = $moneyController->getOperationById(((int) $transaction->number));

                    if (!is_null($operation) && !empty($operation->attribute)) {
                        foreach ($operation->attribute as $attribute) {
                            if ($attribute->key === 'statusid' && $attribute->value === UserTransactionStatus::SUCCEED)
                                $transaction->update(['status' => UserTransactionStatus::SUCCEED]);
                        }
                    }
                }
            }
        }
    }
}
