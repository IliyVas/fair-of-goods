<?php

namespace App\Console\Commands;

use App\Models\Fair\Fair;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RefreshProductStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:product_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh status of products before and after fair';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentFair = Fair::getCurrentFair();
        $today = Carbon::today()->format('Y-m-d');

        if (!is_null($currentFair)) {
            $end = $currentFair->finish_date == $today;

            if (!$end) {
                $products = Product::whereStatus(ProductStatus::ON_STOCK)
                    ->whereForSale(true)
                    ->get();

                if (count($products) > 0) {
                    foreach ($products as $product) {
                        $product->changeStatus(ProductStatus::SALE);
                    }
                }
            } else {
                $products = Product::whereStatus(ProductStatus::SALE)->get();

                if (count($products) > 0) {
                    foreach ($products as $product) {
                        $product->changeStatus(ProductStatus::ON_STOCK);
                    }
                }
            }
        }
    }
}
