<?php

namespace App\Console\Commands;

use App\Http\RequestAPI\Moysklad;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RefreshProductData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh product data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today()->format('Y-m-d H:i:s');
        $stock = new Moysklad();

        $orders = $stock->getOrders($today);
        if (count($orders) > 0) {
            foreach ($orders as $order) {
                //get sale positions
                $orderedProducts = $stock->getOrderedProducts($order->id);
                if (count($orderedProducts) > 0) {
                    foreach ($orderedProducts as $orderedProduct) {
                        //get sold out product
                        $product = $stock->getData($orderedProduct->assortment->meta->href, true);
                        //find sold out product in db
                        $dbProduct = Product::where('stock_id', $product->id)
                            ->where('status', ProductStatus::SALE)
                            ->first();

                        if (!is_null($dbProduct)) {
                            $dbProduct->changeStatus(ProductStatus::SOLD_OUT);
                            $dbProduct->soldOutProduct()->create([
                                'order_id' => $order->id,
                                'price' => $orderedProduct->price/100,
                                'sale_date' => $order->updated,
                                'user_id' => $dbProduct->user_id
                            ]);
                        }
                    }
                }
            }
        }
    }
}
