<?php

namespace App\Console\Commands;

use App\Http\RequestAPI\Moysklad;
use App\Models\Product\ProductStatus;
use App\Models\User;
use Illuminate\Console\Command;

class RevertProductStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'revert:product_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revert product status.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $availableUsersIds = [41, 12];
        $availableProductStatuses = [
            ProductStatus::ON_STOCK,
            ProductStatus::COURIER_EXPECTED,
            ProductStatus::SALE
        ];

        foreach ($availableUsersIds as $availableUserId) {
            $user = User::find($availableUserId);

            if (!is_null($user)) {
                $products = $user->products()->whereIn('status', $availableProductStatuses)->get();

                if (count($products) > 0) {
                    foreach ($products as $product) {
                        if ($product->status === ProductStatus::COURIER_EXPECTED) {
                            $delivery = $product->deliveries()->whereConfirmed(false)->first();

                            if (!is_null($delivery)) {
                                $delivery->products()->detach($product->id);
                                if (!count($delivery->products) > 0)
                                    $delivery->delete();
                            }

                            $product->changeStatus(ProductStatus::REGISTERED);
                        } else {
                            (new Moysklad())->removeProduct($product);
                            $product->forceFill([
                                'stock_id' => null,
                                'enter_id' => null
                            ])->save();
                            $product->changeStatus(ProductStatus::REGISTERED);
                        }
                    }
                }
            }
        }
    }
}
