<?php

namespace App\Console\Commands;

use App\Http\RequestAPI\Moysklad;
use App\Models\Product\Product;
use Illuminate\Console\Command;

class SetDonateAfterFairFlag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'donate_after_fair_flag:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set donate after fair flag into moysklad products.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stock = new Moysklad();
        $products = Product::whereNotNull('stock_id')->get();

        if (count($products) > 0) {
            foreach ($products as $product) {
                $result = $stock->setDonateAfterFairFlag($product);

                if (!$result)
                    return 'Error!';
            }
        }
    }
}
