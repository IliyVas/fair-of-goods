<?php

namespace App\Console\Commands;

use App\Http\RequestAPI\Moysklad;
use App\Models\Product\Product;
use Illuminate\Console\Command;

class SetProductOwnerId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'owner_id:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set product owner id into moysklad products.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stock = new Moysklad();
        $products = Product::whereNotNull('stock_id')->get();

        if (count($products) > 0) {
            foreach ($products as $product) {
                $stockProduct = $stock->getProduct($product->stock_id);
                $hasOwnerId = false;

                if (isset($stockProduct->attributes)) {
                    $stockProductAttributes = $stockProduct->attributes;

                    if (count($stockProductAttributes) > 0)
                        foreach ($stockProductAttributes as $attribute)
                            if ($attribute->id === 'f68591d0-dc1f-11e7-7a34-5acf000cf613')
                                $hasOwnerId = true;
                }

                if (!$hasOwnerId) {
                    $result = $stock->setProductOwnerId($product);


                    if (!$result)
                        return 'Error!';
                }
            }
        }
    }
}
