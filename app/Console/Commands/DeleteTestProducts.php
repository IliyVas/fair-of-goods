<?php

namespace App\Console\Commands;

use App\Http\RequestAPI\Moysklad;
use App\Models\Product\Product;
use Illuminate\Console\Command;

class DeleteTestProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test_products:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove test products from moysklad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stock = new Moysklad();
        $limit = 100;
        $products = Product::whereNotNull('stock_id')->get();

        for ($i = 0; $i <= count($products)/100; $i++) {
            $stockProducts = $stock->getProducts($limit*$i, $limit);
            if (!empty($stockProducts)) {
                $hasOwnerId = false;
                
                foreach ($stockProducts as $stockProduct) {
                    $stockProductAttributes = [];
                    if (isset($stockProduct->attributes))
                        $stockProductAttributes = $stockProduct->attributes;

                    if (!empty($stockProductAttributes))
                        foreach ($stockProductAttributes as $stockProductAttribute)
                            if ($stockProductAttribute->id === 'f68591d0-dc1f-11e7-7a34-5acf000cf613')
                                $hasOwnerId = true;

                    $product = Product::whereStockId($stockProduct->id)->first();

                    if (is_null($product) && $hasOwnerId)
                        $stock->delete('/entity/product/' . $stockProduct->id);
                }
            }
        }
    }
}
