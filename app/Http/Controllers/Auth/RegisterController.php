<?php

namespace App\Http\Controllers\Auth;

use App\Mail\ConfirmEmail;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('confirmEmail');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name'  => $data['last_name'],
            'phone'      => $data['phone'],
            'email'      => $data['email'],
            'password'   => bcrypt($data['password']),
        ]);

        $user->addUserRole();

        return $user;
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'first_name'   => 'required|string',
            'last_name'    => 'required|string',
            'email'        => 'required|email|unique:users',
            'password'     => 'required|confirmed',
            'terms'        => 'accepted',
            'phone'        => 'required|string|max:50',
            'isValidPhone' => 'required|accepted'
        ]);

        $user = $this->create($request->all());

        Mail::to($user->email)->send(new ConfirmEmail($user));

        flash(trans('messages.emailActive'))->success();

        return redirect()->back();
    }

    /**
     * Confirmation user email address
     *
     * @param $token
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function confirmEmail($token)
    {
        $user = User::whereToken($token)->firstOrFail();
        // login as user
        if (Auth::guest())
            Auth::login($user);
        //confirm email
        $user->confirmEmail();

        return redirect($this->redirectPath());
    }

    /**
     * Redirect after register.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectPath()
    {
        if (Auth::user()->isAdmin())
            return route('admin.dashboard');
        else
            return route('dashboard');
    }
}
