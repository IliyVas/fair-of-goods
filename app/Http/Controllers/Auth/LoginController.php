<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Socialite
     */
    public function redirectToProvider($provider)
    {
        if ($provider == 'vkontakte')
            return Socialite::with('vkontakte')->redirect();
        elseif ($provider == 'facebook')
            return Socialite::with('facebook')->redirect();
        elseif ($provider == 'instagram')
            return Socialite::with('instagram')->redirect();
        elseif ($provider == 'odnoklassniki')
            return Socialite::with('odnoklassniki')->redirect();
        elseif ($provider == 'google')
            return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return mixed
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);

        if (is_null($authUser)) {
            return redirect()->route('login')
                ->withErrors(trans('validation.unique', [
                    'attribute' => '"' . trans('auth.email') . '"']));
        } else {
            Auth::login($authUser, true);
            return redirect($this->redirectPath());
        }
    }

    /**
     * @param $user
     * @param $provider
     * @return User
     */
    public function findOrCreateUser($user, $provider)
    {
        $authUser = User::whereEmail($user->email)->first();

        if (!is_null($authUser) && $authUser->provider !== $provider)
            return null;
        elseif (!is_null($authUser))
            return $authUser;

        $currentUser = new User([
            'email'       => $user->email,
            'social'      => true,
            'active'      => true,
            'provider'    => $provider,
            'provider_id' => $user->id,
            'phone' => $this->getPhone($provider, $user),
        ]);

        if ($provider == 'google') {
            $currentUser->first_name = $user->user['name']['givenName'];
            $currentUser->last_name = $user->user['name']['familyName'];
        }

        if ($provider == 'vkontakte' || $provider == 'odnoklassniki') {
            $currentUser->first_name = $user->user['first_name'];
            $currentUser->last_name = $user->user['last_name'];
        }

        if ($provider == 'facebook') {
            $currentUser->first_name = $user->user['name'];
        }

        if ($provider == 'instagram') {
//            $currentUser->first_name = $user->user['first_name'];
//            $currentUser->last_name = $user->user['last_name'];
        }

        $currentUser->save();

        $currentUser->addUserRole();
        $currentUser->addMediaFromUrl($user->avatar)->toMediaCollection();

        return $currentUser;
    }

    /**
     * Redirect after login.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectPath()
    {
        if (Auth::user()->isAdmin())
            return route('admin.dashboard');
        else
            return route('dashboard');
    }

    /**
     * Validate the user login request.
     *
     * @param  Request  $request
     * @return void
     */
    public function validateLogin(Request $request)
    {
        Validator::make($request->all(), [
            'email' => 'required|string',
            'password' => 'required|string',
        ])->after(function ($validator) use ($request) {
            if (!is_null(User::whereEmail($request->email)->whereSocial(true)->first()))
                $validator->errors()->add('', trans('auth.failed'));
        })->validate();
    }

    /**
     * @param $provider
     * @param $user
     * @return null
     */
    private function getPhone($provider, $user)
    {
        $phone = null;

        if ($provider == 'vkontakte') {
            $request_params = array(
                'user_id' => $user->id,
                'fields' => 'contacts',
                'client_secret' => env('VKONTAKTE_SECRET'),
                'access_token' => env('VKONTACTE_ACCESS_TOKEN'),
                'v' => '5.52'
            );
            $get_params = http_build_query($request_params);
            $data = json_decode(file_get_contents('https://api.vk.com/method/users.get?'. $get_params))
                ->response[0];

            if (isset($data->mobile_phone))
                $phone= $data->mobile_phone;
        }

        if ($phone == '')
            $phone = null;

        return $phone;
    }
}
