<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use App\Http\RequestAPI\Moysklad;
use App\Models\Category\Category;
use App\Models\Product\DiscountType;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Http\Controllers\Product\ProductController as BasicProductController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ProductController extends BasicProductController
{
    /**
     * Get index page route.
     *
     * @param $ownerId
     * @return string
     */
    protected function getIndexPath($ownerId)
    {
        return $this->indexPath = route('admin.products.index');
    }

    /**
     * Show products list.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
   {
       return view('admin.product.index', $this->getIndexViwData(Product::all()));
   }

    /**
     * Show product.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $product = Product::getUserProduct($id);

        if (is_null($product))
            return redirect()->back()->withErrors(trans('messages.productDoesNotExistError', ['id' => $id]));

        return view('admin.product.show')->with(['model' => $product]);
    }

    /**
     * Show edit product form.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::getUserProduct($id);

        if (is_null($product))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        if ($product->status !== ProductStatus::REGISTERED)
            return redirect()->back()->withErrors(trans('messages.noRightsForEditProductError',
                ['status' => trans('basic.' .$product->status . 'ProductStatus')]));

        return view('admin.product.edit')->with(['product' => $product]);
    }

    /**
     * Filter by category.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filterByCategory(Request $request)
    {
        $products = Product::whereCategoryId((int) $request->category)->get();

        return view('admin.product.index', $this->getIndexViwData(
            $products, ['filteredByCategory' => Category::find($request->category)]));
    }

    /**
     * Get filter by products.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filterByStatus(Request $request)
    {
        if (empty($request->statuses))
            $products = Product::all();
        else $products = Product::whereIn('status', $request->statuses)->get();

        return view('admin.product.index', $this->getIndexViwData(
            $products, ['filteredByStatuses' => $request->statuses]));
    }

    /**
     * Get array with data for index view.
     *
     * @param Collection $products
     * @param array $optionalData
     * @return array
     */
    private function getIndexViwData(Collection $products, array $optionalData = [])
    {
        return array_merge($optionalData, [
            'admin' => true,
            'simple' => true,
            'showRoute' => 'admin.products.show',
            'editRoute' => 'admin.products.edit',
            'deleteRoute' => route('admin.products.destroy'),
            'products' => $products,
            'categories' => Category::getCategories(),
            'statuses' => ProductStatus::getAllStatuses(),
            'downloadItemsRoute' => route('products.download'),
            'duplicateRoute' => 'admin.products.duplicate',
        ]);
    }

    /**
     * Confirm the transfer of the item to charity.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function confirm(Request $request)
    {
        //get selected products id
        $productIds = $request->selected_items;
        if (empty($productIds))
            return redirect()->back()->withErrors(trans('messages.noProductsSelectedError'));

        foreach ($productIds as $id) {
            //get product
            $product = Product::getUserProduct($id);
            if (is_null($product))
                return redirect()->back()->withErrors(trans('messages.notAuthorized'));

            if ($product->status !== ProductStatus::ON_STOCK) {
                flash(trans('messages.notAuthorized'), 'error');
            } else {
                (new Moysklad())->removeProduct($product);
                $result = $product->forceFill([
                    'stock_id' => null,
                    'enter_id' => null
                ])->save();
                $product->changeStatus(ProductStatus::DONATED_TO_CHARITY);

                if (!$result)
                    return redirect()->back()->withErrors(trans('messages.error'));
            }
        }

        flash(trans('messages.success'))->success();
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function download()
    {
        $fileName = 'products_' . Carbon::today()->format('d_m_y') . '_' . time();
        $discounts = DiscountType::getDiscountTypesNames();
        $thData = array_merge(
            [
                trans('basic.user'),
                trans('basic.name'),
                trans('basic.desc'),
                trans('basic.donate'),
                trans('basic.price'),
            ],
            $discounts
        );
        $lastColumn = range('A', 'Z')[count($thData) - 1];

        $products = Product::all();
        if (count($products) > 0) {
            $tdData = [];
            foreach ($products as $product) {
                if ($product->donate_after_fair || !$product->for_sale)
                    $donate = trans('basic.yes');
                else $donate = trans('basic.no');
                $productDiscounts = [];

                if (count($product->discounts) > 0)
                    foreach ($discounts as $discountTypeId => $discountTypeName)
                        foreach ($product->discounts as $productDiscount)
                            if ($productDiscount->type_id === $discountTypeId)
                                $productDiscounts[] = $productDiscount->percent . '%';

                $tdData[] = array_merge(
                    [
                        $product->user->first_name . ' ' . $product->user->last_name .
                        ' (' . $product->user->email . ', ' . $product->user->phone . ', ID: ' .
                        $product->user_id . ')',
                        $product->name,
                        $product->description,
                        $donate,
                        $product->price,
                    ],
                    $productDiscounts
                );
            }

            CommonHelper::downloadInExel($fileName, $thData, $tdData, $lastColumn);
        }

        return redirect()->back();
    }
}
