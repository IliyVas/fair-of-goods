<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with(['products'])->get();
        if (count($users) > 0) {
            $this->setParams($users);
        }

        return view('admin.user.index', ['users' => $users]);
    }

    /**
     * Show user page.
     *
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::find($id);
        if (is_null($user))
            return redirect()->back()->withErrors(trans('messages.userDoesNotExistError', ['id' => $id]));

        return view('admin.user.show', ['user' => $user]);
    }

    /**
     * Set params for users
     *
     * @param $users
     */
    protected function setParams($users)
    {
        foreach ($users as $user) {
            $user->countProducts = $user->products->count();
            $user->countDonateProducts = $user->getNumberOfDonatedProducts();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (is_null($user))
            return redirect()->back()->withErrors(trans('messages.userDoesNotExistError', ['id' => $id]));

        $user->products()->delete();
        $user->deliveries()->delete();
        $result = $user->delete();

        if (!$result)
            flash(trans('messages.errDeletedUser'), 'error');
        else
            flash(trans('messages.deletedUser'))->success();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(Request $request)
    {
        $query = User::query();

        //filter by name
        if ($request->name)
            $query->where('first_name', 'like', '%' . $request->name . '%');

        //filter by age
        if (!is_null($request->age_from)) {
            $fromBirthday = Carbon::now()->subYears((int) $request->age_from)->format('Y-m-d');
            $query->where('birthday', '<=', $fromBirthday);
        }
        if (!is_null($request->age_to)) {
            $toBirthday = Carbon::now()->subYears((int) $request->age_to)->format('Y-m-d');
            $query->where('birthday','>=', $toBirthday);
        }

        $users = $query->get();
        if (count($users) > 0) {
            foreach ($users as $userKey => $user) {
                $productCount = $user->productsCount();
                $donatedProductCount = $user->getNumberOfDonatedProducts();
                $revenue = $user->getTotalRevenue();
                $isValidUser = true;

                //filter by products
                if (!is_null($request->products_from)) {
                    if ($productCount < (int)$request->products_from)
                        $isValidUser = false;
                }
                if (!is_null($request->products_to)) {
                    if ($productCount > (int)$request->products_to)
                        $isValidUser = false;
                }

                //filter by donated products
                if (!is_null($request->donate_from)) {
                    if ($donatedProductCount < (int)$request->donate_from)
                        $isValidUser = false;
                }
                if (!is_null($request->donate_to)) {
                    if ($donatedProductCount > (int)$request->donate_to)
                        $isValidUser = false;
                }

                //filter by revenue
                if (!is_null($request->sum_from)) {
                    if ($revenue < (int)$request->sum_from)
                        $isValidUser = false;
                }
                if (!is_null($request->sum_to)) {
                    if ($revenue > (int)$request->sum_to)
                        $isValidUser = false;
                }

                //unset invalid user
                if (!$isValidUser)
                    unset($users[$userKey]);
            }
        }

        if (count($users) > 0) {
            $this->setParams($users);
        }

        if (!empty($request->all()))
            flash(trans('messages.filteredUser'))->success();

        return view('admin.user.index', ['users' => $users, 'filter' => $request->all()]);
    }

    /**
     * Download excel file with user data.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function download()
    {
        $fileName = 'users_' . Carbon::today()->format('d_m_y') . '_' . time();
        $thData = [
            trans('auth.regFstName'),
            trans('auth.regLstName'),
            trans('auth.regEmail'),
            trans('auth.regPhone'),
            trans('basic.sum') . ', ' . trans('basic.rub'),
        ];
        $lastColumn = range('A', 'Z')[count($thData) - 1];

        $users = User::all();
        if (count($users) > 0) {
            $tdData = [];
            foreach ($users as $user) {
                $tdData[] = [
                    $user->first_name,
                    $user->last_name,
                    $user->email,
                    $user->phone,
                    $user->getTotalRevenue(),
                ];
            }

            CommonHelper::downloadInExel($fileName, $thData, $tdData, $lastColumn);
        }

        return redirect()->back();
    }

    /**
     * @param $userId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function confirmWithdrawMoney($userId)
    {
        $user = User::find($userId);
        if (is_null($user))
            return redirect()->back()
                ->withErrors(trans('messages.userDoesNotExistError', ['id' => $userId]));

        if (!$user->canWithdrawMoney())
            return redirect()->back()
                ->withErrors(trans('messages.notAuthorized'));

        $user->transactions()->create(['amount' => $user->getAvailableTransactionAmount()]);
        flash(trans('messages.success'))->success();

        return redirect()->back();
    }
}

