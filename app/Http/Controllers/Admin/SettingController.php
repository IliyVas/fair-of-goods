<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Models\Setting;

class SettingController extends Controller
{
    /**
     * Show settings page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.setting.index');
    }

    /**
     * Save settings changes.
     *
     * @param SettingRequest $request
     * @param string $settingName
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(SettingRequest $request, $settingName)
    {
        $settingName = htmlspecialchars($settingName);
        $setting = Setting::whereName($settingName)->first();

        if (is_null($setting))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        $result = $setting->update(['value' => $request->value]);

        if (!$result)
            flash(trans('messages.error'), 'error');
        else
            flash(trans('messages.saveSuccess'))->success();

        return redirect()->back();
    }

    /**
     * Update revenue percent in all valid products.
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function globalUpdateRevenuePercent()
    {
        $revenuePercent = Setting::getRevenuePercent();
        $products = Product::whereNotNull('revenue_percent')
            ->where('status', '!=', ProductStatus::SOLD_OUT)->get();

        if (count($products)> 0) {
            foreach ($products as $product) {
                $result = $product->update(['revenue_percent' => $revenuePercent]);

                if (!$result)
                    return redirect()->back()->withErrors(trans('messages.error'));
            }
        }

        flash(trans('messages.revenueUpdateSuccess'))->success();

        return redirect()->back();
    }
}
