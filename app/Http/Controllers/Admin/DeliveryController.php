<?php

namespace App\Http\Controllers\Admin;

use App\Models\Delivery;
use App\Http\Controllers\DeliveryController as BasicDeliveryController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class DeliveryController extends BasicDeliveryController
{
    /**
     * @var string
     */
    protected $indexPath;

    /**
     * Get index page route.
     *
     * @param $userId
     * @return string
     */
    protected function getIndexPath($userId)
    {
        return $this->indexPath = route('admin.deliveries.index');
    }

    /**
     * Show deliveries list.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.delivery.index', $this->getIndexViwData(Delivery::all()));
    }

    /**
     * Show delivery form.
     *
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $delivery = Delivery::getUserDelivery($id);
        if (is_null($delivery))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        if (!$delivery->isEditable())
            return redirect()->back()->withErrors(trans('messages.noRightsForEditDeliveryError'));

        return view('admin.delivery.edit', ['delivery' => $delivery]);
    }

    /**
     * Filter deliveries by date, status and type.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter(Request $request)
    {
        $query = Delivery::query();

        //filter by status
        if (isset($request->status))
            $query->where('confirmed', (boolean) $request->status);

        //filter by type
        if (isset($request->type))
            $query->where('on_stock', (boolean) $request->type);

        //filter by date
        if (isset($request->today))
            $query->where('delivery_date', Carbon::today()->format('Y-m-d'));
        else {
            if (isset($request->date_from) && !is_null($request->date_from))
                $query->where('delivery_date', '>=', Carbon::parse($request->date_from)->toDateString());

            if (isset($request->date_to) && !is_null($request->date_to))
                $query->where('delivery_date', '<=', Carbon::parse($request->date_to)->toDateString());
        }

        if (!empty($request->all()))
            flash(trans('messages.filteredDelivery'))->success();

        return view('admin.delivery.index', $this->getIndexViwData(
            $query->get(),
            ['filter' => $request->all()]
        ));
    }

    /**
     * Get array with data for index view.
     *
     * @param Collection $deliveries
     * @param array $optionalData
     * @return array
     */
    private function getIndexViwData(Collection $deliveries, array $optionalData = [])
    {
        return array_merge($optionalData, [
            'deliveries' => $deliveries,
            'user' => Auth::user(),
            'deleteRoute' => route('admin.deliveries.destroy'),
            'routeName' => 'admin.deliveries.edit',
            'admin' => true,
            'showProductRouteName' => 'admin.products.show',
        ]);
    }
}
