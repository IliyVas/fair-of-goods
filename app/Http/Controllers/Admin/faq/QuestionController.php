<?php

namespace App\Http\Controllers\Admin\faq;

use App\Http\Controllers\Controller;
use App\Models\faq\QuestionAnswerPair;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    /**
     * Show questions grid.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.faq.question.index', ['questions' => QuestionAnswerPair::all()]);
    }

    /**
     * Change question.
     *
     * @param Request $request
     * @param $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        //check data
        $data = $request->all();
        $validation = $this->validateFormData($data);
        if ($validation->fails())
            return ['level' => 'error', 'message' => $validation->errors()->first()];

        //get faq
        $faq = QuestionAnswerPair::find($id);
        if (!$faq)
            return ['level' => 'error', 'message' => trans('messages.notAuthorized')];

        //update question
        $result = $faq->update([
            'question:ru' => $data['question_ru'],
            'question:en' => $data['question_en'],
            'answer:ru' => $data['answer_ru'],
            'answer:en' => $data['answer_en'],
            'category_id' => $data['category'],
        ]);
        if (!$result)
            return ['level' => 'error', 'message' => trans('messages.updateQuestionError')];
        else
            return [
                'level' => 'success',
                'message' => trans('messages.updateQuestion'),
                'category_name' => $faq->category->name,
                'category_id' => $faq->category_id,
            ];
    }

    /**
     * Create question.
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request)
    {
        //check data
        $data = $request->all();
        $validation = $this->validateFormData($data);
        if ($validation->fails())
            return ['level' => 'error', 'message' => $validation->errors()->first()];

        //create question
        $result = QuestionAnswerPair::create([
            'question:ru' => $data['question_ru'],
            'question:en' => $data['question_en'],
            'answer:ru' => $data['answer_ru'],
            'answer:en' => $data['answer_en'],
            'category_id' => $data['category'],
        ]);

        if (!$result)
            return ['level' => 'error', 'message' => trans('messages.createQuestionError')];
        else
            return [
                'level' => 'success',
                'message' => trans('messages.createQuestion'),
                'category_name' => $result->category->name,
                'category_id' => $result->category_id,
                'faq_id' => $result->id,
                'confirmAlert' => view('templates._confirm_single_deletion', [
                    'model' => $result,
                    'editableTbl' => true,
                    'delMsg' => trans('messages.deleteQuestionConfirm',['id' => $result->id])])->render()
            ];
    }

    /**
     * Validate form data.
     *
     * @param $data
     * @return mixed
     */
    private function validateFormData($data)
    {
        return Validator::make($data, [
            'question_en' => 'required|string|max:500',
            'question_ru' => 'required|string|max:500',
            'answer_en' => 'required|string|max:500',
            'answer_ru' => 'required|string|max:500',
            'category' => 'required|numeric|min:1|exists:question_categories,id'
        ]);
    }

    /**
     * Delete question category.
     *
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        //get faq
        $faq = QuestionAnswerPair::find($id);
        if (!$faq)
            return ['level' => 'error', 'message' => trans('messages.notAuthorized')];

        //delete question and check result
        $result = $faq->delete();
        if (!$result)
            return ['level' => 'error', 'message' => trans('messages.deleteQuestionError')];
        else
            return ['level' => 'success', 'message' => trans('messages.deleteQuestion', ['id' => $id])];
    }
}
