<?php

namespace App\Http\Controllers\Admin\faq;

use App\Http\Controllers\Controller;
use App\Models\faq\QuestionCategory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class QuestionCategoryController extends Controller
{
    /**
     * Show questions categories grid.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.faq.question_category.index', ['categories' => QuestionCategory::all()]);
    }

    /**
     * Change question category.
     *
     * @param Request $request
     * @param $id
     * @return array
     */
    public function update(Request $request, $id)
    {
        //check data
        $data = $request->all();
        $validation = $this->validateFormData($data);
        if ($validation->fails())
            return ['level' => 'error', 'message' => $validation->errors()->first()];

        //get category
        $category = QuestionCategory::find($id);
        if (!$category)
            return ['level' => 'error', 'message' => trans('messages.notAuthorized')];

        //update category
        $result = $category->update(['name:ru' => $data['name_ru'], 'name:en' => $data['name_en']]);
        if (!$result)
            return ['level' => 'error', 'message' => trans('messages.updateQuestionCategoryError')];
        else
            return ['level' => 'success', 'message' => trans('messages.updateQuestionCategory')];
    }

    /**
     * Create question category.
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request)
    {
        //check data
        $data = $request->all();
        $validation = $this->validateFormData($data);
        if ($validation->fails())
            return ['level' => 'error', 'message' => $validation->errors()->first()];

        //create category
        $result = QuestionCategory::create(['name:ru' => $data['name_ru'], 'name:en' => $data['name_en']]);
        if (!$result)
            return ['level' => 'error', 'message' => trans('messages.createQuestionCategoryError')];
        else
            return [
                'level' => 'success',
                'message' => trans('messages.createQuestionCategory'),
                'id' => $result->id,
                'confirmAlert' => view('templates._confirm_single_deletion', [
                    'model' => $result,
                    'editableTbl' => true,
                    'delMsg' => trans('messages.deleteQuestCategoryConfirm',['id' => $result->id])])->render()
                ];
    }

    /**
     * Validate form data.
     *
     * @param $data
     * @return mixed
     */
    private function validateFormData($data)
    {
        return Validator::make($data, [
            'name_en' => 'required|string|max:255',
            'name_ru' => 'required|string|max:255'
        ]);
    }

    /**
     * Delete question category.
     *
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        //get question category
        $category = QuestionCategory::find($id);
        if (!$category)
            return ['level' => 'error', 'message' => trans('messages.notAuthorized')];

        //delete category and questions from this category
        $category->questions()->delete();
        $category->translations()->delete();
        $result = $category->delete();

        //check result
        if (!$result)
            return ['level' => 'error', 'message' => trans('messages.deleteQuestionCategoryError')];
        else
            return ['level' => 'success', 'message' => trans('messages.deleteQuestionCategory', ['id' => $id])];
    }
}
