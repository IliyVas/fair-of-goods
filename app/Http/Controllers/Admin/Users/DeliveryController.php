<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\RequestAPI\Moysklad;
use App\Mail\DeliveryStatusChanged;
use App\Models\Delivery;
use App\Models\Product\ProductStatus;
use App\Models\User;
use App\Http\Controllers\DeliveryController as BasicDeliveryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DeliveryController extends BasicDeliveryController
{
    /**
     * @var string
     */
    protected $indexPath;

    /**
     * Get index page route.
     *
     * $param $userId
     * @return string
     */
    protected function getIndexPath($userId)
    {
        return $this->indexPath = route('users.deliveries.index', [$userId]);
    }

    /**
     * Show deliveries grid.
     *
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($userId = null)
    {
        $user = User::find($userId);
        if (is_null($user))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        return view('admin.user.delivery.index', [
            'deliveries' => $user->deliveries,
            'user' => $user
        ]);
    }

    /**
     * Confirm delivery|export.
     *
     * @param $userId
     * @param $deliveryId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function confirm($userId, $deliveryId)
    {
        //get delivery
        $delivery = Delivery::find($deliveryId);
        if (is_null($delivery))
            return redirect()->back()->withErrors(trans('messages.deliveryDoesNotExistError',
                ['id' => $deliveryId]));

        if ($delivery->confirmed || $delivery->user_id != $userId)
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        //confirm delivery
        $delivery = $delivery->forceFill(['confirmed' => true]);
        $moySklad = new Moysklad;
        $errors = false;

        //change product status
        foreach ($delivery->products as $product) {
            if ($delivery->on_stock) {
                if ($product->status !== ProductStatus::ON_STOCK) {
                    $moySkladResult = $moySklad->addProduct($product);
                    if (!$moySkladResult)
                        $errors = true;
                    else {
                        $enter = $moySklad->addEnterWithProduct($moySkladResult->id);
                        if (!$enter)
                            $errors = true;
                    }

                    if (!$errors) {
                        $product->forceFill([
                            'stock_id' => $moySkladResult->id,
                            'enter_id' => $enter->id
                        ])->save();
                        $product->changeStatus(ProductStatus::ON_STOCK);
                    }
                }
            } else {
                $moySklad->removeProduct($product);
                $product->forceFill([
                    'stock_id' => null,
                    'enter_id' => null
                    ])->save();
                $product->changeStatus(ProductStatus::REGISTERED);
            }

            if (!$errors)
                $delivery->save();
        }

        //check result and get message
        if ($errors && $delivery->on_stoct)
            flash(trans('messages.confirmExportError'), 'error');
        elseif ($errors && !$delivery->on_stock)
            flash(trans('messages.confirmDeliveryError'), 'error');
        elseif(!$errors) {
            if ($delivery->on_stock)
                flash(trans('messages.confirmExport', ['id' => $deliveryId]))->success();
            else
                flash(trans('messages.confirmDelivery', ['id' => $deliveryId]))->success();

            Mail::to($delivery->user->email)->send(new DeliveryStatusChanged($delivery));
        }

        return redirect()->back();
    }

    /**
     * Show edit delivery form.
     *
     * @param $userId
     * @param $deliveryId
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($userId = null, $deliveryId = null)
    {
        $delivery = Delivery::find($deliveryId);
        if (is_null($delivery) || $delivery->user_id != $userId)
            return redirect()->back()
                ->withErrors(trans('messages.deliveryDoesNotExistError', ['id' => $deliveryId ]));

        if (!$delivery->isEditable())
            return redirect()->back()->withErrors(trans('messages.noRightsForEditDeliveryError'));

        return view('admin.user.delivery.edit', ['delivery' => $delivery]);
    }

    /**
     * Show create delivery form.
     *
     * @param Request $request
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request, $userId = null)
    {
        if ($request->selected_items)
            $selectedProducts = $request->selected_items;
        else $selectedProducts = null;

        $user = User::find($userId);
        if (is_null($user))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        return view('admin.user.delivery.create', [
            'delivery' => new Delivery(),
            'products' => $user->products,
            'selectedProducts' => $selectedProducts,
            'user' => $user
        ]);
    }
}
