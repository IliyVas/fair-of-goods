<?php

namespace App\Http\Controllers\Admin\Users;

use App\Helpers\CommonHelper;
use App\Http\RequestAPI\Moysklad;
use App\Models\Product\DiscountType;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Models\User;
use App\Http\Controllers\Product\ProductController as BasicProductController;
use Carbon\Carbon;

class ProductController extends BasicProductController
{
    /**
     * Get index page route.
     *
     * @param $ownerId
     * @return string
     */
    protected function getIndexPath($ownerId)
    {
        return $this->indexPath = route('users.products.index', [$ownerId]);
    }

    /**
     * Show user products.
     *
     * @param $userId
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($userId = null)
    {
        $user = User::find($userId);
        if (is_null($user))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        return view('admin.user.product.index', [
            'products' => $user->products,
            'user' => $user,
        ]);
    }

    /**
     * Show edit product form.
     *
     * @param $userId
     * @param $productId
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($userId = null, $productId = null)
    {
        $product = Product::find($productId);
        if (is_null($product) || $product->user_id != $userId)
            return redirect()->back()
                ->withErrors(trans('messages.productDoesNotExistError', ['id' => $productId ]));

        if ($product->status !== ProductStatus::REGISTERED)
            return redirect()->back()->withErrors(trans('messages.noRightsForEditProductError',
                ['status' => trans('basic.' .$product->status . 'ProductStatus')]));

        return view('admin.user.product.edit', ['product' => $product]);
    }

    /**
     * Show products page.
     *
     * @param $userId
     * @param $productId
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($userId = null, $productId = null)
    {
        $product = Product::find($productId);

        if (is_null($product) || $product->user_id != $userId)
            return redirect()->back()
                ->withErrors(trans('messages.productDoesNotExistError', ['id' => $productId ]));

        return view('admin.user.product.show', ['model' => $product]);
    }

    /**
     * Show add product form.
     *
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($userId = null)
    {
        $user = User::find($userId);
        if (is_null($user))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        return view('admin.user.product.create', ['user' => $user]);
    }

    /**
     * @param $userId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function download($userId)
    {
        $user = User::find($userId);
        if (is_null($user))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        $fileName = $userId . '_' . $user->first_name . '_' . $user->last_name .
            '_products_' . Carbon::today()->format('d_m_y') . '_' . time();
        $discounts = DiscountType::getDiscountTypesNames();
        $thData = array_merge(
            [
                trans('basic.name'),
                trans('basic.desc'),
                trans('basic.donate'),
                trans('basic.price'),
            ],
            $discounts
        );
        $lastColumn = range('A', 'Z')[count($thData) - 1];

        $products = $user->products;
        if (count($products) > 0) {
            $tdData = [];
            foreach ($products as $product) {
                $productDiscounts = [];
                if ($product->donate_after_fair || !$product->for_sale)
                    $donate = trans('basic.yes');
                else $donate = trans('basic.no');

                if (count($product->discounts) > 0)
                    foreach ($discounts as $discountTypeId => $discountTypeName)
                        foreach ($product->discounts as $productDiscount)
                            if ($productDiscount->type_id === $discountTypeId)
                                $productDiscounts[] = $productDiscount->percent . '%';

                $tdData[] = array_merge(
                    [
                        $product->name,
                        $product->description,
                        $donate,
                        $product->price,
                    ],
                    $productDiscounts
                );
            }

            CommonHelper::downloadInExel($fileName, $thData, $tdData, $lastColumn);
        }

        return redirect()->back();
    }

    /**
     * @param $userId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function downloadPriceList($userId)
    {
        $downloadUrls = (new Moysklad())->getPrintPriceListUrl($userId);

        if (!empty($downloadUrls))
            return view('admin.user.product.download_pricelist', ['urls' => $downloadUrls]);
        else
            return redirect()->back()->withErrors(trans('messages.pricelistError'));
    }
}
