<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FairRequest;
use App\Models\Fair\Fair;
use Carbon\Carbon;

class FairController extends Controller
{
    /**
     * Show fairs grid.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.fair.index', ['fairs' => Fair::all()]);
    }

    /**
     * Show edit fair show.
     *
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $fair = Fair::find($id);
        if (is_null($fair)) return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        $fair->start_date = date_create($fair->start_date)->format('d.m.Y');
        $fair->finish_date = date_create($fair->finish_date)->format('d.m.Y');

        return view('admin.fair.edit', ['fair' => $fair]);
    }

    /**
     * Save fair changes.
     *
     * @param FairRequest $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(FairRequest $request, $id)
    {
        $fair = Fair::find($id);

        if (is_null($fair))
            return redirect()->back()->withErrors(trans('messages.fairDoesNotExistError', ['id' => $id]));

        //parse dates
        $data = $request->all();
        $data['finish_date'] = Carbon::parse($data['finish_date'])->toDateString();
        $data['start_date'] = Carbon::parse($data['start_date'])->toDateString();

        //save changes
        $result = $fair->update($data);

        if (!$result)
            flash(trans('messages.updateFairError'), 'error');
        else
            flash(trans('messages.updateFair'))->success();

        return redirect()->back();

    }
}
