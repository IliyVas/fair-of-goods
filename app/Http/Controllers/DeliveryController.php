<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeliveryRequest;
use App\Mail\CreateDelivery;
use App\Models\Delivery;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class DeliveryController extends Controller
{
    /**
     * Product count limit.
     */
    const LIMIT = 50;

    /**
     * @var string
     */
    protected $indexPath;

    /**
     * Get index page route.
     *
     * @param $userId
     * @return string
     */
    protected function getIndexPath($userId)
    {
        return $this->indexPath = route('deliveries.index');
    }

    /**
     * Show deliveries grid.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('delivery.index')->with(['deliveries' => Auth::user()->deliveries]);
    }

    /**
     * Delete delivery.
     *
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        //get selected deliveries id
        $deliveryIds = $request->selected_items;
        if (empty($deliveryIds))
            return redirect()->back()->withErrors(trans('messages.noDeliveriesSelectedError'));

        foreach ($deliveryIds as $id) {
            $delivery = Delivery::getUserDelivery($id);
            $userId = null;

            if (!$delivery) {
                flash(trans('messages.deliveryDoesNotExistError', ['id' => $id ]), 'error');
            } elseif (!$delivery->isDeletable()) {
                $userId = $delivery->user_id;
                flash(trans('messages.noRightsForDeleteDeliveryError', ['id' => $delivery->id ]), 'error');
            } else {
                //delete delivery
                $userId = $delivery->user_id;
                $delivery->revertProductStatus();
                $delivery->products()->detach();
                $result = $delivery->delete();

                if (!$result) return redirect()->back()->withErrors(
                    trans('messages.deleteDeliveryError', ['id' => $id]));

                flash(trans('messages.deleteDelivery', ['id' => $id]))->success();
            }
        }

        return redirect($this->getIndexPath($userId));
    }

    /**
     * Show create delivery form.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if ($request->selected_items)
            $selectedProducts = $request->selected_items;
        else $selectedProducts = null;

        return view('delivery.create', [
            'delivery' => new Delivery(),
            'products' => Auth::user()->products,
            'selectedProducts' => $selectedProducts
        ]);
    }

    /**
     * Create delivery.
     *
     * @param DeliveryRequest $request
     * @param $userId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store(DeliveryRequest $request, $userId = null)
    {
        //create delivery
        if (is_null($request->courier))
            $request->courier = true;

        if (count($request->products) > self::LIMIT)
            return redirect()->back()->withErrors('messages.deliveryProductCountError');

        $delivery = (new Delivery)->store([
            'on_stock' => $request->on_stock,
            'address' => Delivery::getFullAddressString($request),
            'delivery_date' => Carbon::parse($request->delivery_date)->toDateString(),
            'delivery_time' => $request->delivery_time,
            'courier' => $request->courier,
        ], $userId);

        //check result
        if (!$delivery) {
            if ($request->on_stock)
                return redirect()->back()->withErrors(trans('messages.createExportError'));
            else return redirect()->back()->withErrors(trans('messages.createDeliveryError'));
        }

        $delivery->saveProducts($request, true);

        //check that $delivery has products
        if (count($delivery->products) <= 0) {
            $delivery->delete();
            if ($request->on_stock)
               flash(trans('messages.createExportError'), 'error');
            else flash(trans('messages.createDeliveryError'), 'error');
        } else {
            //get success message
            if (!$request->on_stock)
                flash(trans('messages.createDelivery'))->success();
            else flash(trans('messages.createExport'))->success();

            foreach (User::all() as $user)
                if ($user->isAdmin())
                    Mail::to($user->email)->send(new CreateDelivery($delivery));

        }

        return redirect()->back();
    }

    /**
     * Show edit delivery form.
     *
     * @param $id
     * @return $this|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $delivery = Auth::user()->deliveries()->find($id);
        if (!$delivery)
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        if (!$delivery->isEditable())
            return redirect()->back()->withErrors(trans('messages.noRightsForEditDeliveryError'));

        return view('delivery.edit', ['delivery' => $delivery]);
    }

    /**
     * Update delivery.
     *
     * @param DeliveryRequest $request
     * @param $deliveryId
     * @param $userId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(DeliveryRequest $request, $userId = null, $deliveryId)
    {
        $delivery = Delivery::getUserDelivery($deliveryId);
        if (!$delivery)
            return redirect()->back()->withErrors(trans('messages.deliveryDoesNotExistError',
                ['id' => $deliveryId ]));

        if (!is_null($userId) && $delivery->user_id !== (int) $userId)
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        if (!$delivery->isEditable())
            return redirect()->back()->withErrors(trans('messages.noRightsForEditDeliveryError'));

        if (count($request->products) > self::LIMIT)
            return redirect()->back()->withErrors('messages.deliveryProductCountError');

        //update delivery
        if (is_null($request->courier))
            $request->courier = true;

        $result = $delivery->update([
            'on_stock' => $request->on_stock,
            'address' => Delivery::getFullAddressString($request),
            'delivery_date' => Carbon::parse($request->delivery_date)->toDateString(),
            'delivery_time' => $request->delivery_time,
            'courier' => $request->courier,
        ]);

        //check result
        if (!$result) {
            if ($request->on_stock)
                return redirect()->back()->withErrors(trans('messages.editExportError'));
            else return redirect()->back()->withErrors(trans('messages.editDeliveryError'));
        }

        //detach old products and save new
        $delivery->revertProductStatus();
        $delivery->products()->detach();
        $delivery->saveProducts($request);

        //check that $delivery has products
        if (count($delivery->products) <= 0) {
            $delivery->delete();
            if ($request->on_stock)
                flash(trans('messages.editExportError'), 'error');
            else flash(trans('messages.editDeliveryError'), 'error');
        } else {
            //get success message
            if (!$request->on_stock)
                flash(trans('messages.editDelivery'))->success();
            else flash(trans('messages.editExport'))->success();
        }

        return redirect()->back();
    }
}
