<?php

namespace App\Http\Controllers\Product;

use App\Models\Product\DiscountType;
use App\Models\Product\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Institution\Institution;
use App\Models\Feature\Feature;
use App\Models\Category\Category;

class ProductFormController extends Controller
{

    /**
     * Get constitution and size inputs data.
     *
     * @param Request $request
     * @return bool|string
     */
    public function categoryDependentBlock(Request $request)
    {
        $category = Category::find($request->category);
        if (!$category) return false;

        //get constitution data
        $constitution = null;
        if ($category->hasFeature(Feature::CONSTITUTION)) $constitution = Feature::getFeature(Feature::CONSTITUTION);

        //get child age data
        $childAge = null;
        if ($category->hasFeature(Feature::CHILD_AGE)) $childAge = Feature::getFeature(Feature::CHILD_AGE);

        return view('product_form._category_dependent_block')->with([
            'constitution' => $constitution,
            'childAge' => $childAge,
            'size' => $category->getSizeData(),
            'childAgeOldValue' => $request->child_age,
            'constitutionOldValue' => $request->constitution,
            'sizeOldValue' => $request->size,
            'product' => Product::getUserProduct($request->product),
        ])->render();
    }

    /**
     * Get second step form block.
     *
     * @param Request $request
     * @return bool|string
     */
    public function secondStep(Request $request)
    {
        return view('product_form._second_step')->with([
            'institutions' => Institution::all(),
            'discountTypes' => DiscountType::all(),
            'forSale' => true,
            'donateAfterFairOldValue' => $request->donateAfterFair,
            'institutionIdOldValue' => $request->institution,
            'priceOldValue' => $request->price,
            'product' => Product::getUserProduct($request->product),
        ])->render();
    }

    /**
     * Get institution form block.
     *
     * @param Request $request
     * @return string
     */
    public function institution(Request $request)
    {
        return view('product_form._institution')->with([
            'institutions' => Institution::all(),
            'forSale' =>false,
            'institutionIdOldValue' => $request->institution,
            'product' => Product::getUserProduct($request->product),
        ])->render();
    }
}
