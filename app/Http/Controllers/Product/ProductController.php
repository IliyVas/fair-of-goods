<?php

namespace App\Http\Controllers\Product;

use App\Helpers\MediaHelper;
use App\Http\Controllers\Controller;
use App\Http\RequestAPI\Moysklad;
use App\Http\Requests\ProductRequest;
use App\Models\Feature\Feature;
use App\Models\Product\Product;
use App\Models\Product\ProductHistoryType;
use App\Models\Product\ProductStatus;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * @var string
     */
    protected $indexPath;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->middleware('checkProductManagementPermissions')
            ->only('show', 'edit', 'update');
    }

    /**
     * Get index page route.
     *
     * @param $ownerId
     * @return string
     */
    protected function getIndexPath($ownerId)
    {
        return $this->indexPath = route('products.index');
    }

    /**
     * Show products grid.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();

        return view('product.index')->with(['products' => $user->products, 'user' => $user]);
    }

    /**
     * Show add product form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Create product.
     *
     * @param ProductRequest $request
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(ProductRequest $request, $userId = null)
    {
        //create product
        if ($request->for_sale)
            $data = array_merge($request->all(), ['revenue_percent' => Setting::getRevenuePercent()]);
        else $data = $request->all();

        $product = (new Product)->store($data, $userId);
        if (is_null($product)) return redirect()->back()->withErrors(trans('messages.createProductError'));

        $product->saveFeatureValues($request);
        $product->saveDiscounts($request);
        $product->saveImage($request);

        flash(trans('messages.createProduct', ['name' => $product->name]))->success();
        return redirect()->back();
    }

    /**
     * Delete products.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function destroy(Request $request)
    {
        //get selected products id
        $productIds = $request->selected_items;
        if (empty($productIds))
            return redirect()->back()->withErrors(trans('messages.noProductsSelectedError'));

        $ownerId = null;
        foreach ($productIds as $id) {
            //get product
            $product = Product::getUserProduct($id);
            if (is_null($product))
                return redirect()->back()->withErrors(trans('messages.notAuthorized'));

            $ownerId = $product->user_id;
            if ($product->status !== ProductStatus::REGISTERED) {
                flash(trans('messages.noRightsForDeleteProductError', [
                    'name' => $product->name,
                    'status' => trans('basic.' . $product->status . 'ProductStatus')
                ]), 'error');
            } else {
                $name = $product->name;

                $result = $product->delete();
                if (!$result)
                    return redirect()->back()->withErrors(trans('messages.deleteProductError', ['name' => $name]));

                flash(trans('messages.deleteProduct', ['name' => $name]))->success();
            }
        }

        return redirect($this->getIndexPath($ownerId));
    }

    /**
     * Show product.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $product = Product::find($id);

        if (is_null($product))
            return redirect()->back()->withErrors(trans('messages.productDoesNotExistError', ['id' => $id]));

        return view('product.show')->with(['model' => $product]);
    }

    /**
     * Show edit product form.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::getUserProduct($id);

        if (is_null($product))
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        if ($product->status !== ProductStatus::REGISTERED)
            return redirect()->back()->withErrors(trans('messages.noRightsForEditProductError',
                ['status' => trans('basic.' .$product->status . 'ProductStatus')]));

        return view('product.edit')->with(['product' => $product]);
    }

    /**
     * Change product.
     *
     * @param ProductRequest $request
     * @param $productId
     * @param $userId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(ProductRequest $request, $userId = null, $productId)
    {
        //get product
        $product = Product::getUserProduct($productId);

        if (is_null($product) || $product->status !== ProductStatus::REGISTERED ||
        !is_null($userId) && (int) $userId !== $product->user_id)
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        //update product
        $data =$request->all();
        $priceChanged = isset($data['price']) && (int) $data['price'] !== $product->price;
        $forSaleChanged = $data['for_sale'] && !$product->for_sale;
        if ($forSaleChanged || $priceChanged)
            $result = $product->update(array_merge($data, [
                'revenue_percent' => Setting::getRevenuePercent()]));
        elseif (!$data['for_sale'])
            $result = $product->update(array_merge($data, [
                'revenue_percent' => null,
                'donate_after_fair' => false,
                'price' => 0
            ]));
        else $result = $product->update($data);

        if (!$result)
            return redirect()->back()->withErrors(trans('messages.updateProductError', ['name' => $product->name]));

        //save additional data
        $product->featureValues()->detach();
        $product->saveFeatureValues($request);

        //attach discount
        $product->discounts()->detach();
        $product->saveDiscounts($request);

        //upload image
        $product->saveImage($request);

        //add history
        $product->history()->create(['type' => ProductHistoryType::CHANGED_DATA]);

        flash(trans('messages.updateProduct', ['name' => $product->name]))->success();
        return redirect()->back();
    }

    /**
     * @param $userId
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function downloadPriceList($userId)
    {
        $downloadUrls = (new Moysklad())->getPrintPriceListUrl($userId);

        if (!empty($downloadUrls))
            return view('product.download_pricelist', ['urls' => $downloadUrls]);
        else
            return redirect()->back()->withErrors(trans('messages.pricelistError'));
    }

    /**
     * @param $userId
     * @param $productId
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function duplicate($userId, $productId)
    {
        $product = Product::getUserProduct($productId, $userId);

        if (is_null($product) || $product->status !== ProductStatus::REGISTERED)
            return redirect()->back()->withErrors(trans('messages.notAuthorized'));

        $newProduct = (new Product())->store([
            'category_id' => $product->category_id,
            'institution_id' => $product->institution_id,
            'name' => $product->name,
            'description' => $product->description,
            'status' => ProductStatus::REGISTERED,
            'for_sale' => $product->for_sale,
            'price' => $product->price,
            'donate_after_fair' => $product->donate_after_fair,
            'revenue_percent' => $product->revenue_percent,
        ], $userId);

        if (is_null($newProduct))
            return redirect()->back()->withErrors(trans('messages.error'));

        //save features
        if (!is_null($product->getFeatureValue(Feature::CONSTITUTION, 'id')))
            $newProduct->featureValues()->attach($product->getFeatureValue(Feature::CONSTITUTION, 'id'));
        if (!is_null($product->getFeatureValue(Feature::CHILD_AGE, 'id')))
            $newProduct->featureValues()->attach($product->getFeatureValue(Feature::CHILD_AGE, 'id'));
        if (!is_null($product->getFeatureValue(Feature::GENDER, 'id')))
            $newProduct->featureValues()->attach($product->getFeatureValue(Feature::GENDER, 'id'));
        if (!is_null($product->getSizeFeatureValue()))
            $newProduct->featureValues()->attach($product->getSizeFeatureValue());

        //save discounts
        if (count($product->discounts) > 0)
            foreach ($product->discounts as $discount)
                $newProduct->discounts()->attach($discount->id);

        //save image
        if($product->hasMedia(Product::GENERAL_IMAGE))
            MediaHelper::uploadImageFromUrl($newProduct, asset($product->media()->first()->getUrl()), Product::GENERAL_IMAGE);

        flash(trans('messages.duplicateProduct', ['name' => $newProduct->name]))->success();

        return redirect()->back();
    }
}
