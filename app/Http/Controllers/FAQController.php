<?php

namespace App\Http\Controllers;

use App\Models\faq\QuestionCategory;

class FAQController extends Controller
{
    /**
     * Show FAQ page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('faq.index', ['categories' => QuestionCategory::all()]);
    }
}
