<?php

namespace App\Http\Controllers;

use App\Http\Requests\Profile\ProfilePasswordRequest;
use App\Http\Requests\Profile\ProfileRequest;
use App\Mail\EmailChanged;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ProfileController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('checkProfileEditPermissions')->except(['index', 'showPhoneForm', 'savePhone']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        return view('profile.index', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('profile.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProfileRequest $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request, $id)
    {
        //get user
        $user = User::find($id);

        //update user
        $user->fill([
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'phone'      => $request->phone,
        ]);
        if ($request->has('birthday')) {
            $user->birthday = Carbon::parse($request->birthday);
        }
        $result = $user->save();

        //update image
        $hasMedia = $user->hasMedia();
        if ($request->hasFile('avatar')) {
            if ($hasMedia)
                $user->deleteMedia($user->getMedia()->first());

            $user->addMediaFromRequest('avatar')->toMediaCollection();
        }

        if (!$result)
            flash(trans('messages.errUpdatedUser'), 'error');
        else
            flash(trans('messages.updatedUser'))->success();

        return redirect()->back();
    }


    /**
     * @param ProfilePasswordRequest $request
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function passwords(ProfilePasswordRequest $request, $id)
    {
        $user = User::find($id);
        $user->password = bcrypt($request->password);
        $result = $user->save();
        if (!$result)
            flash(trans('messages.errChangedPword'), 'error');
        else
            flash(trans('messages.changedPword'))->success();

        return redirect()->back();
    }

    /**
     * Change email.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function email(Request $request, $id)
    {
        $this->validate($request, [
            'new_email' => 'required|email|unique:users,email|unique:users,new_email'
        ]);

        //save new email
        $user = User::find($id);
        $user->update(['new_email' => $request->new_email, 'token' => str_random(30)]);

        //send mail
        Mail::to($user->new_email)->send(new EmailChanged($user));
        flash(trans('messages.emailActive'))->success();

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPhoneForm()
    {
        return view('profile.phone_form');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function savePhone(Request $request)
    {
        $this->validate($request, [
            'phone'        => 'required|string|max:50',
            'isValidPhone' => 'required|accepted'
        ]);

        $user = Auth::user();
        $user->update(['phone' => $request->phone]);

        return redirect()->route('profile.index');
    }
}

