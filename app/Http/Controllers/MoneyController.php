<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Moneta\MonetaSdk;

class MoneyController extends Controller
{
    /**
     * @var mixed
     */
    private $monetaSdkAccountId;

    /**
     * @var MonetaSdk
     */
    private $monetaSDK;

    /**
     * MoneyController constructor.
     */
    public function __construct()
    {
        $this->monetaSdkAccountId = config('moneta.basic.account.id');
        $this->monetaSDK = new MonetaSdk(config_path('moneta/'));
    }

    /**
     * Show withdraw money page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->canWithdrawMoney())
            $disabled = '';
        else $disabled = 'disabled';

        return view('money.index', ['user' => $user, 'disabled' => $disabled]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function withdraw(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();
        $sum = $user->getAvailableTransactionAmount();

        if (array_key_exists('secureToken', $data) && $sum > 0) {
            $this->setMonetaSDKSettings();
            $this->monetaSDK->checkMonetaServiceConnection();

            $result = $this->monetaSDK->sdkMonetaTransfer(
                $this->monetaSdkAccountId,
                $this->monetaSDK->getSettingValue('monetasdk_account_pay_password_enrypted'),
                "279",
                $sum,
                trans('basic.withdrawMoneyDesc', [
                    'sum' => htmlspecialchars($user->getAvailableTransactionAmount())
                ]),
                ['SECURETOKEN' => $data['secureToken']]
            );

            if (isset($result->status) && isset($result->transaction)) {
                $user->transactions()->create([
                    'amount' => $sum,
                    'status' => $result->status,
                    'number' => $result->transaction,
                ]);
                flash(trans('messages.success'))->success();
            } else {
                flash(trans('messages.error'), 'error');
                Log::error(json_encode($result));
            }
        } else flash(trans('messages.error'), 'error');

        return redirect()->route('money.index');
    }

    /**
     * Set MonetsSDK settings.
     */
    private function setMonetaSDKSettings()
    {
        $this->monetaSDK->setSettingValue('monetasdk_account_id', $this->monetaSdkAccountId);
        $this->monetaSDK->setSettingValue('monetasdk_connection_type', config('moneta.basic.sdk.connection_type'));
        $this->monetaSDK->setSettingValue('monetasdk_demo_mode', config('moneta.basic.sdk.demo_mode'));
        $this->monetaSDK->setSettingValue('monetasdk_test_mode', config('moneta.basic.sdk.test_mode'));
        $this->monetaSDK->setSettingValue('monetasdk_debug_mode', config('moneta.basic.sdk.debug_mode'));
        $this->monetaSDK->setSettingValue('monetasdk_account_username', config('moneta.basic.account.username'));
        $this->monetaSDK->setSettingValue('monetasdk_account_password', config('moneta.basic.account.password'));
        $this->monetaSDK->setSettingValue('monetasdk_account_pay_password_enrypted', config('moneta.basic.account.pay_password_enrypted'));
    }

    /**
     * @param $id
     * @return array|bool|mixed|null|object
     */
    public function getOperationById($id)
    {
        $this->setMonetaSDKSettings();
        $this->monetaSDK->checkMonetaServiceConnection();
        $result = $this->monetaSDK->sdkMonetaGetOperationDetailsById((int) $id);

        if (isset($result->data->operation))
            $operation = $result->data->operation;
        else $operation = null;

        return $operation;
    }
}
