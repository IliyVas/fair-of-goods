<?php

namespace App\Http\RequestAPI;

use App\Models\Feature\Feature;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class Moysklad
{
    /**
     * @var mixed
     */
    protected $username;

    /**
     * @var mixed
     */
    protected $password;

    /**
     * @var mixed
     */
    protected $base_url;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $organization;

    /**
     * Moysklad constructor.
     */
    public function __construct()
    {
        $this->username = config('moysklad.sklad_username');
        $this->password = config('moysklad.sklad_password');
        $this->base_url = config('moysklad.base_api_sklad');
        $this->organization = [
            'meta' => [
                'href' => $this->base_url . 'entity/organization/7808951d-236f-11e7-7a34-5acf00218230',
                'metadataHref' => $this->base_url . 'entity/organization/metadata',
                'type' => 'organization',
                'mediaType' => 'application/json'
            ]
        ];

        $this->client = new Client([
            'auth' => [$this->username, $this->password],
            'allow_redirects' => false
        ]);
    }

    /**
     * Creating request for api with type GET
     *
     * @param $request
     * @param $full
     * @return mixed
     */
    public function getData($request, $full = false)
    {
        try{
            if ($full) $api = $request;
            else  $api = $this->base_url.$request;

            $result = $this->client->request('GET', $api);

            return json_decode($result->getBody());
        }catch (GuzzleException $exception){

            if($exception->getCode() == '401'){
                Log::info('Wrong login or password');
            }
            Log::error($exception->getMessage());
        }
    }

    /**
     * Send data with post.
     *
     * @param $request
     * @param array $data
     * @return mixed
     */
    public function sendData($request, array $data)
    {
        try{
            $api = $this->base_url.$request;
            $result = $this->client->post($api, [
                'json' => $data
            ]);
            return json_decode($result->getBody());
        }catch (GuzzleException $exception){

            Log::error($exception->getMessage());
        }
    }

    /**
     * Creating request for api with type DELETE.
     *
     * @param $request
     */
    public function delete($request)
    {
        try{
            $api = $this->base_url.$request;
            $this->client->delete($api);
        }catch (GuzzleException $exception){

            Log::error($exception->getMessage());
        }
    }

    /**
     * Creating request for api with type PUT.
     *
     * @param $request
     * @param $data
     * @return mixed
     */
    public function update($request, $data)
    {
        try{
            $api = $this->base_url.$request;
            $result = $this->client->put($api, [
                'json' => $data
            ]);

            return json_decode($result->getBody());
        }catch (GuzzleException $exception){

            Log::error($exception->getMessage());
        }
    }

    /**
     * Get all orders whose update time is greater than
     * or equal to the value specified in the parameter $updatedFrom.
     *
     * @param string $updatedFrom
     * @return mixed
     */
    public function getOrders($updatedFrom = null)
    {
        if (!is_null($updatedFrom))
            $query = '?updatedFrom=' . $updatedFrom;
        else $query = '';

        return $this->getData('/entity/retaildemand' . $query)->rows;
    }

    /**
     * Get ordered products array.
     *
     * @param $orderId
     * @return mixed
     */
    public function getOrderedProducts($orderId)
    {
        return $this->getData('/entity/retaildemand/' . $orderId . '/positions')->rows;
    }

    /**
     * Add product to system.
     *
     * @param $product
     * @return mixed
     */
    public function addProduct($product)
    {
        $discounts = '';
        if (count($product->discounts) > 0) {
            foreach ($product->discounts as $discount) {
                if (is_null($discount->discountType->finish_day))
                    $discounts .= 'Скидка на ' . $discount->discountType->start_day . ' и последующие дни ярмарки: '
                        . $discount->percent . '%! ';
                else
                    $discounts .= 'Скидка на ' . $discount->discountType->start_day . '-' .  $discount->discountType->finish_day .
                        ' день ярмарки: ' . $discount->percent . '%! ';
            }
        }

        $desc = '';
        if (!is_null($product->description))
            $desc = $product->description;

        if ($product->donate_after_fair)
            $flag = "Да";
        else
            $flag = "Нет";

        return $this->sendData('/entity/product', [
            'name' => $product->name,
            'description' => $desc,
            'salePrices' => [
                [
                    'value' => $product->price * 100,
                    'priceType' => 'Цена продажи',
                    'currency' => [
                        'meta' => [
                            'href' => $this->base_url . 'entity/currency/7809a22a-236f-11e7-7a34-5acf00218237',
                            'metadataHref' => $this->base_url . 'entity/currency/metadata',
                            'type' => 'currency',
                            'mediaType' => 'application/json'
                        ]
                    ]
                ],
                [
                    'value' => $product->price * 100,
                    'priceType' => 'Цена без скидки',
                    'currency' => [
                        'meta' => [
                            'href' => $this->base_url . 'entity/currency/7809a22a-236f-11e7-7a34-5acf00218237',
                            'metadataHref' => $this->base_url . 'entity/currency/metadata',
                            'type' => 'currency',
                            'mediaType' => 'application/json'
                        ]
                    ]
                ],
            ],
            'attributes' => [
                [
                    "id" => "2e327fcd-b0ce-11e7-7a34-5acf0015ed05",
                    "name" => "Скидки",
                    "value" =>  $discounts,
                ],
                [
                    "id" => "2e328232-b0ce-11e7-7a34-5acf0015ed06",
                    "name" => "Категория",
                    "value" =>  $product->category->getTranslation('ru')->name,
                ],
                [
                    "id" => "a7efdc9a-b801-11e7-7a34-5acf001dd8f1",
                    "name" => "Комплекция",
                    "value" =>  $product->getFeatureValue(Feature::CONSTITUTION, 'content', true),
                ],
                [
                    "id" => "a7efdf05-b801-11e7-7a34-5acf001dd8f3",
                    "name" => "Возраст ребенка",
                    "value" =>  $product->getFeatureValue(Feature::CHILD_AGE, 'content', true),
                ],
                [
                    "id" => "a7efdfc2-b801-11e7-7a34-5acf001dd8f4",
                    "name" => "Для кого вещь",
                    "value" =>  $product->getFeatureValue(Feature::GENDER, 'content', true),
                ],
                [
                    "id" => "a7efde2f-b801-11e7-7a34-5acf001dd8f2",
                    "name" => "Размер",
                    "value" => $product->getSizeFeatureValue('content', true),
                ],
                [
                    "id" => "f68591d0-dc1f-11e7-7a34-5acf000cf613",
                    "name" => "ID владельца",
                    "value" => $product->user_id,
                ],
                [
                    "id" => "902a103a-df4e-11e7-7a34-5acf0044d5aa",
                    "name" => "Передается на благотворительность",
                    "value" => $flag,
                ],
            ]
        ]);
    }

    /**
     * Add enter with product.
     *
     * @param $productId
     * @return mixed
     */
    public function addEnterWithProduct($productId)
    {
        $result = $this->sendData('/entity/enter', [
            'organization' =>  $this->organization,
            'store' => [
                'meta' => [
                    'href' => $this->base_url . 'entity/store/24607fc1-a438-11e7-7a69-971100012746',
                    'metadataHref' => $this->base_url . 'entity/store/metadata',
                    'type' => 'store',
                    'mediaType' => 'application/json'
                ]
            ],
            'positions' => [
                [
                    'quantity' => 1,
                    'assortment' => [
                        'meta' => [
                            'href' => $this->base_url . "/entity/product/" . $productId,
                            "metadataHref" => "https://online.moysklad.ru/api/remap/1.1/entity/product/metadata",
                            "type" => "product",
                            "mediaType" => "application/json"
                        ]
                    ]
                ]
            ]
        ]);

        return $result;
    }

    /**
     * Delete product with enter.
     *
     * @param $product
     */
    public function removeProduct($product)
    {
        $this->delete('/entity/enter/' . $product->enter_id);
        $this->delete('/entity/product/' . $product->stock_id);
    }

    /**
     * Change sale product price.
     *
     * @param $id
     * @param $price
     * @return mixed
     */
    public function changeSaleProductPrice($id, $price)
    {
        return $this->update('/entity/product/' . $id, [
            'salePrices' => [
                [
                    'value' => $price * 100,
                    'priceType' => 'Цена продажи',
                    'currency' => [
                        'meta' => [
                            'href' => $this->base_url . 'entity/currency/7809a22a-236f-11e7-7a34-5acf00218237',
                            'metadataHref' => $this->base_url . 'entity/currency/metadata',
                            'type' => 'currency',
                            'mediaType' => 'application/json'
                        ]
                    ]
                ]
            ]
        ]);
    }

    /**
     * @param null $userId
     * @return array
     */
    public function getPrintPriceListUrl($userId = null)
    {
        $priceLists = $this->addPriceLists($userId);
        $template = $this->getPriceListTemplate();
        $downloadUrls = [];

        if (!empty($priceLists) && !is_null($template)) {
            foreach ($priceLists as $priceList) {
                if (!is_null($priceList)) {
                    $api = $this->base_url . '/entity/pricelist/' . $priceList->id . '/export';
                    $data = [
                        'template' => [
                            'meta' => [
                                'href' => $template->meta->href,
                                'type' => $template->meta->type,
                                'mediaType' => 'application/json'
                            ]
                        ],
                        'extension' => 'pdf',
                        'count' => 1
                    ];

                    try{
                        $response = $this->client->post($api, [
                            'json' => $data
                        ])->getHeaders();

                        if (!empty($response) && array_key_exists('Location', $response))
                            $downloadUrls[] = array_first($response['Location']);
                    }catch (GuzzleException $exception){

                        Log::error($exception->getMessage());
                    }
                }
            }
        }

        return $downloadUrls;
    }

    /**
     * @param null $userId
     * @return mixed
     */
    public function addPriceLists($userId = null)
    {
        $priceLists = [];
        $finalData = [];
        $data = [
            'organization' => $this->organization,
            'positions' => [
                'rows' => []
            ],
            'columns' => [['name' => 'Дата']],
        ];
        $products = [];

        if (is_null($userId))
            $products = Product::whereNotNull('stock_id')
                ->where('for_sale', true)
                ->where('status', '!=', ProductStatus::SOLD_OUT)
                ->get();
        else {
            $user = User::find($userId);
            if (!is_null($user))
                $products = $user->products()
                    ->whereNotNull('stock_id')
                    ->where('for_sale', true)
                    ->where('status', '!=', ProductStatus::SOLD_OUT)
                    ->get();
        }

        if (count($products) > 0) {
            $productsCollections = $products->chunk(100);

            foreach ($productsCollections as $key => $productsCollection) {
                $finalData[$key] = $data;
                foreach ($productsCollection as $product) {
                    $finalData[$key]['positions']['rows'][] = [
                        'assortment' => [
                            'meta' => [
                                'href' => $this->base_url . '/entity/product/' . $product->stock_id,
                                'metadataHref' => $this->base_url . '/entity/product/metadata',
                                'type' => 'product',
                                'mediaType' => 'application/json'
                            ]
                        ],
                        'cells' => [
                            'column' => 'Дата',
                            'value' => Carbon::today()->format('d.m.Y')
                        ]
                    ];
                }
            }
        }

        if (!empty($finalData)) {
            foreach ($finalData as $data) {
                $priceLists[] = $this->sendData('/entity/pricelist', $data);
            }
        }

        return $priceLists;
    }

    /**
     * @return mixed|null
     */
    public function getPriceListTemplate()
    {
        $template = null;
        $templates = $this->getData('/entity/pricelist/metadata/customtemplate/')->rows;

        if (!empty($templates))
            $template = array_first($templates);

        return$template;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getProduct($id)
    {
        return $this->getData('/entity/product/' . $id);
    }

    /**
     * @param $product
     * @return mixed
     */
    public function setProductOwnerId($product)
    {
        return $this->update('/entity/product/' . $product->stock_id, [
            'attributes' => [
                [
                    "id" => "f68591d0-dc1f-11e7-7a34-5acf000cf613",
                    "name" => "ID владельца",
                    "value" => $product->user_id,
                ],
            ]
        ]);
    }

    /**
     * @param $product
     * @return mixed
     */
    public function setDonateAfterFairFlag($product)
    {
        if ($product->donate_after_fair)
            $flag = "Да";
        else
            $flag = "Нет";

        return $this->update('/entity/product/' . $product->stock_id, [
            'attributes' => [
                [
                    "id" => "902a103a-df4e-11e7-7a34-5acf0044d5aa",
                    "name" => "Передается на благотворительность",
                    "value" => $flag,
                ],
            ]
        ]);
    }

    /**
     * @param $offset
     * @param $limit
     * @return mixed
     */
    public function getProducts($offset, $limit) {
        return $this->getData('/entity/product?offset=' . $offset . '&limit=' . $limit)->rows;
    }
}
