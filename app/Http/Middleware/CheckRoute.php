<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isAdminRoute = str_contains( $request->route()->getPrefix(), 'admin');
        $user = Auth::user();

        if ($user->isAdmin() && $isAdminRoute || $user->isUser() && !$isAdminRoute)
            return $next($request);
        elseif ($user->isAdmin())
            return redirect()->route('admin.dashboard');
        elseif ($user->isUser())
            return redirect()->route('dashboard');

        return redirect()->route('login');
    }
}
