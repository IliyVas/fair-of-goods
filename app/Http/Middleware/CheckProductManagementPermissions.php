<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckProductManagementPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (!$user->isAdmin() && !$user->products->contains('id', $request->route('product'))) {
            return redirect()->route('products.index')->withErrors(trans('messages.notAuthorized'));
        }

        return $next($request);
    }
}
