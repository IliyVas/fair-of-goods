<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasPhone
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $route = \Route::current()->getName();
        $excepts = ['phone.save', 'phone.form', 'login', 'logout', 'register'];
        if (Auth::check() && !in_array($route, $excepts) && $user->isUser() && is_null($user->phone)) {
            return redirect()->route('phone.form');
        }

        return $next($request);
    }
}
