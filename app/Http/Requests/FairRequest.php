<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class FairRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name:en' => 'required|string|min:2|max:255',
            'name:ru' => 'required|string|min:2|max:255',
            'description:en' => 'required|string|min:5|max:500',
            'description:ru' => 'required|string|min:5|max:500',
            'address:en' => 'required|string|min:5|max:500',
            'address:ru' => 'required|string|min:5|max:500',
            'lat' => 'nullable|required_with:long|string|min:2|max:10',
            'long' => 'nullable|required_with:lat|string|min:2|max:10',
            'start_date' => 'required|date|after_or_equal:' . date_create()->format('d.m.Y'),
            'finish_date' => 'required|date|after:start_date',
        ];
    }
}
