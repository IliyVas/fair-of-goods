<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'       => 'required|min:3|max:191',
            'last_name'        => 'required|min:3|max:191',
            'email'            => 'required|email',
            'social'           => 'sometimes|in:on,off,yes,no,true,false,1,0',
            'active'           => 'sometimes|in:on,off,yes,no,true,false,1,0',
            'password'         => 'sometimes|required|min:6',
            'password_confirm' => 'sometimes|required|same:password',

        ];
    }
}
