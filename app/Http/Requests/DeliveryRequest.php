<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class DeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products' => 'required|exists:products,id',
            'address' => 'required_without:courier|nullable|string|max:255',
            'courier' => 'sometimes|boolean',
            'apartment' => 'nullable|numeric',
            'delivery_date' => 'required|date|after_or_equal:' . date_create()->format('d.m.Y'),
            'delivery_time' => 'required|string|min:10|max:11',
            'on_stock' => 'required|boolean'
        ];
    }
}
