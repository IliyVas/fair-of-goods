<?php

namespace App\Http\Requests\Profile;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|min:3|max:191',
            'last_name'  => 'required|string|min:3|max:191',
            'birthday'   => 'nullable|date',
            'phone'      => 'required|string|max:50',
            'avatar'     => 'image',
            'isValidPhone' => 'required|accepted'
        ];
    }
}
