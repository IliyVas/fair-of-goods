<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:2',
            'description' => 'nullable|string|max:500',
            'category_id' => 'required|numeric|min:1|exists:categories,id',
            'institution_id' => 'required_if:for_sale, false|required_if:donate_after_fair, true|numeric|exists:institutions,id',
            'constitution' => 'nullable|numeric|min:1|exists:feature_values,id',
            'child_age' => 'nullable|numeric|min:1|exists:feature_values,id',
            'size' => 'sometimes|numeric|min:1|exists:feature_values,id',
            'gender' => 'required|numeric|min:1|exists:feature_values,id',
            'image' => 'nullable|image',
            'hasImage' => 'required_without:image',
            'for_sale' => 'required|boolean',
            'donate_after_fair' => 'required_if:for_sale, true|boolean',
            'price' => 'required_if:for_sale, true|numeric|min:0',
            'discount.*' => 'sometimes|nullable|numeric|min:1|exists:discounts,id'
        ];
    }
}
