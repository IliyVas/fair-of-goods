<?php

namespace App\Helpers;

class MediaHelper
{
    /**
     * Upload image.
     *
     * @param $model
     * @param $image
     * @param string $mediaCollection
     * @return mixed
     */
    public static function uploadImage($model, $image, $mediaCollection = 'default')
    {
        $name = time() . CommonHelper::transliterate($image->getClientOriginalName());

        return $model->addMedia($image)
            ->usingName($name)->usingFileName($name)->toMediaCollection($mediaCollection);
    }

    /**
     * Upload image from url.
     *
     * @param $model
     * @param $url
     * @param string $mediaCollection
     * @return mixed
     */
    public static function uploadImageFromUrl($model, $url, $mediaCollection = 'default')
    {
        return $model->addMediaFromUrl($url)->toMediaCollection($mediaCollection);
    }
}
