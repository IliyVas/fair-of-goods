<?php

namespace App\Helpers;

use Maatwebsite\Excel\Facades\Excel;

class CommonHelper
{
    /**
     * Translates a string to transliteration.
     *
     * @param $string
     * @return string
     */
    public static function transliterate($string)
    {
        $string = strip_tags($string); //remove html-tags
        $string = trim($string);
        $string = function_exists('mb_strtolower') ? mb_strtolower($string) : strtolower($string);
        $string = strtr($string, array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm',
            'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shh', 'ъ' => '',
            'ы' => 'i', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'));
        $string = str_replace(" ", "", $string); //remove spaces

        return $string;
    }

    /**
     * Activate menu item.
     *
     * @param array $routeNames
     * @param bool $addClassAttribute
     * @return string
     */
    public static function addActiveClass(array $routeNames, $addClassAttribute = true)
    {
        if (!in_array(\Route::current()->getName(), $routeNames)) {
            return '';
        }

        if ($addClassAttribute) {
            return 'class="active"';
        } else {
            return 'active';
        }
    }

    /**
     * @param $fileName
     * @param array $thData
     * @param array $tdData
     * @param $lastColumn
     */
    public static function downloadInExel($fileName, array $thData, array $tdData, $lastColumn) {
        Excel::create($fileName, function ($excel) use ($thData, $tdData, $lastColumn) {
            $excel->sheet('List', function($sheet) use ($thData, $tdData, $lastColumn) {
                $rowNum = 1;

                //set headers
                $sheet->row($rowNum, $thData);

                //set data
                if (count($tdData) > 0) {
                    foreach ($tdData as $item) {
                        $rowNum++;
                        $sheet->row($rowNum, $item);
                    }
                }

                //set common styles
                $sheet->cells('A1:' . $lastColumn . $rowNum, function($cells) {
                    $cells->setValignment('center');
                });

                //set headers styles
                $sheet->cells('A1:' . $lastColumn . '1', function($cells) {
                    $cells->setAlignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#E0E0E0');
                });

                //set height
                $heightSettings = [];
                for ($i = 1; $i <= $rowNum; $i++) {
                    $heightSettings[$i] = 25;
                }
                $sheet->setHeight($heightSettings);

                //set borders
                for ($i = 1; $i <= $rowNum; $i++) {
                    foreach (range('A', $lastColumn) as $column) {
                        $sheet->setBorder($column . $i, 'thin');
                    }
                }
            });
        })->store('xls')->download('xls');
    }
}
