<?php

namespace App\Models\Fair;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Fair extends Model
{
    use Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = ['name', 'description', 'address', 'locale'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['lat', 'long', 'start_date', 'finish_date'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * @var model
     */
    private static $nearestFair;

    /**
     * @var model
     */
    private static $currentFair;

    /**
     * @var model
     */
    private static $latestFair;

    /**
     * Get count of days before the nearest fair.
     *
     * @return mixed|string
     */
    public static function getNearestFairDaysCount()
    {
        if (is_null(self::getNearestFair())) return 0;
        else
            return date_create(date('Y-m-d', time()))
                ->diff(date_create(self::$nearestFair->start_date))
                ->days;
    }

    /**
     * Get nearest fair.
     *
     * @return mixed|string
     */
    public static function getNearestFair()
    {
        if (is_null(self::$nearestFair))
            self::$nearestFair = self::where('start_date', '>', date('Y-m-d', time()))
                ->orderBy('start_date')->first();

        return self::$nearestFair;
    }

    /**
     * Get latest fair.
     *
     * @return Model|mixed
     */
    public static function getLatestFair()
    {
        if (is_null(self::$latestFair))
            self::$latestFair = self::where('finish_date', '<', Carbon::today()->format('Y-m-d'))
                ->orderBy('finish_date')->get()->last();

        return self::$latestFair;
    }

    /**
     * Check possibility to edit fair.
     *
     * @return bool
     */
    public function isEditable()
    {
        return $this->start_date > date('Y-m-d', time());
    }

    /**
     * Get current fair.
     *
     * @return Model|null|static
     */
    public static function getCurrentFair()
    {
        $today = Carbon::today()->format('Y-m-d');
        if (is_null(self::$currentFair))
            self::$currentFair = self::where('start_date', '<=', $today)
                ->where('finish_date', '>=', $today)->first();

        return self::$currentFair;
    }

    /**
     * Get count of the current fair days .
     *
     * @param  bool $end
     * @return mixed|string
     */
    public static function getCurrentFairDaysCount($end = true)
    {
        if (is_null(self::getCurrentFair())) $count = 0;
        else {
            if ($end) $date = self::$currentFair->finish_date;
            else $date = self::$currentFair->start_date;

            $count = date_create(Carbon::today())
                    ->diff(date_create($date))
                    ->days + (int) !($end);
        }

        return $count;
    }
}
