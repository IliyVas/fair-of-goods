<?php

namespace App\Models\Fair;

use Illuminate\Database\Eloquent\Model;

class FairTranslation extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'address'];
}
