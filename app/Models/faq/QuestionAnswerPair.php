<?php

namespace App\Models\faq;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class QuestionAnswerPair extends Model
{
    use Translatable;

    /**
     * @var string
     */
    public $translationModel = 'App\Models\faq\QuestionAnswerPairTranslation';

    /**
     * @var array
     */
    public $translatedAttributes = ['question', 'answer'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * Get question category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(QuestionCategory::class);
    }
}
