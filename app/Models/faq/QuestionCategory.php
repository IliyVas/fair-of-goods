<?php

namespace App\Models\faq;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class QuestionCategory extends Model
{
    use Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = ['name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * Get all questions from this category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(QuestionAnswerPair::class, 'category_id');
    }
}
