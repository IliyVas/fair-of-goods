<?php

namespace App\Models\faq;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswerPairTranslation extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'answer'];
}
