<?php

namespace App\Models\Product;

use App\Models\Category\Category;
use App\Models\Fair\Fair;
use App\Models\Feature\Feature;
use App\Models\Feature\FeatureValue;
use App\Models\Institution\Institution;
use App\Models\Delivery;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Helpers\MediaHelper;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class ProductStatus
{
    const REGISTERED = 'registered';
    const COURIER_EXPECTED = 'courier';
    const ON_STOCK = 'stock';
    const SALE = 'sale';
    const DONATED_TO_CHARITY = 'donated';
    const SOLD_OUT = 'sold';

    /**
     * Get all statuses names.
     *
     * @return array
     */
    public static function getAllStatuses()
    {
        return array(self::REGISTERED, self::COURIER_EXPECTED, self::ON_STOCK,
            self::SALE, self::DONATED_TO_CHARITY, self::SOLD_OUT);
    }
}

class Product extends Model implements HasMediaConversions
{
    use HasMediaTrait;

    const GENERAL_IMAGE = 'ProductImage';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'for_sale',
        'price',
        'donate_after_fair',
        'name',
        'description',
        'category_id',
        'institution_id',
        'revenue_percent',
    ];

    /**
     * Get product history.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function history()
    {
        return $this->hasMany(ProductHistory::class);
    }

    /**
     * Get all feature values of product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function featureValues()
    {
        return $this->belongsToMany(FeatureValue::class);
    }

    /**
     * Get all discounts of product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function discounts()
    {
        return $this->belongsToMany(Discount::class);
    }

    /**
     * Get product category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get institution.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function institution()
    {
        return $this->belongsTo(Institution::class);
    }

    /**
     * Get all deliveries of product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function deliveries()
    {
        return $this->belongsToMany(Delivery::class);
    }

    /**
     * Get product owner.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get sold out product data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function soldOutProduct()
    {
        return $this->hasOne(SoldOutProduct::class);
    }

    /**
     * Change status and create product history.
     *
     * @param $status
     */
    public function changeStatus($status)
    {
        $this->forceFill(['status' => $status])->save();
        $this->history()->create([
            'type' => ProductHistoryType::CHANGED_STATUS,
            'content' => $status
        ]);
    }

    /**
     * Get selected kind feature value.
     *
     * @param string $attribute
     * @param bool $moysklad
     * @param $kind
     * @return mixed
     */
    public function getFeatureValue($kind, $attribute = 'content', $moysklad = false)
    {
        $featureValue = $this->featureValues()->where('feature_id', Feature::getFeature($kind)->id)->first();

        $result = null;

        if ($featureValue && $attribute === 'content') {
            if ($moysklad) $result = $featureValue->getTranslation('ru')->content;
            else $result = $featureValue->content;
        } elseif($featureValue && $attribute === 'id') $result = $featureValue->id;

        return $result;
    }

    /**
     * Get user product.
     *
     * @param $productId
     * @param $userId
     * @return null
     */
    public static function getUserProduct($productId, $userId = null)
    {
        $product = null;
        $user = null;

        if (is_null($userId)) $user = Auth::user();
        else $user = User::find($userId);

        if (!is_null($user) && $user->isAdmin())
            $product = self::find($productId);
        elseif (!is_null($user)) $product = $user->products()->find($productId);

        return $product;
    }

    /**
     * Get product size id.
     *
     * @param string $attribute
     * @param bool $moysklad
     * @return int|null
     */
    public function getSizeFeatureValue($attribute = 'id', $moysklad = false)
    {
        $aSize = $this->getFeatureValue(Feature::ACCESSORIES_SIZE, $attribute, $moysklad);
        $cSize = $this->getFeatureValue(Feature::CLOTHES_SIZE, $attribute, $moysklad);
        $wSize = $this->getFeatureValue(Feature::WOMEN_SIZE, $attribute, $moysklad);
        $fSize = $this->getFeatureValue(Feature::FOOTWEAR_SIZE, $attribute, $moysklad);
        $size = null;

        if (!is_null($aSize)) $size = $aSize;
        elseif (!is_null($cSize)) $size = $cSize;
        elseif (!is_null($wSize)) $size = $wSize;
        elseif (!is_null($fSize)) $size = $fSize;

        return $size;
    }

    /**
     * Save discounts.
     *
     * @param $request
     */
    public function saveDiscounts($request)
    {
        if ($request->discount) {
            foreach ($request->discount as $discount) {
                if ($discount)
                    $this->discounts()->attach($discount);
            }
        }
    }

    /**
     * Save product image.
     *
     * @param $request
     * @return $this
     */
    public function saveImage($request)
    {
        if ($this->hasMedia(self::GENERAL_IMAGE) && $request->hasFile('image'))
            $this->media()->first()->delete();

        if ($request->hasFile('image')) {
            $result = MediaHelper::uploadImage($this, $request->image, self::GENERAL_IMAGE);

            if (!$result) {
                flash(trans('messages.uploadProductImgError'), 'error');
                Log::error('Image error! ' . json_encode($result));
            }
        }
    }

    /**
     * Save additional data.
     *
     * @param $request
     */
    public function saveFeatureValues($request)
    {
        //verify the validity of the transmitted value of gender and associate the value with the product
        if (Feature::getFeature(Feature::GENDER)->featureValues->contains('id', $request->gender))
            $this->featureValues()->attach($request->gender);

        //check features values associated with the selected category
        $category = Category::find($this->category_id);

        //verify the validity of the transmitted value of constitution
        $hasConstitution = $category->hasFeature(Feature::CONSTITUTION);
        $correctConstitutionValue = Feature::getFeature(Feature::CONSTITUTION)->featureValues
            ->contains('id', $request->constitution);
        //associate the constitution value with the product
        if($request->constitution && $hasConstitution && $correctConstitutionValue)
            $this->featureValues()->attach($request->constitution);

        //verify the validity of the transmitted size value of size
        $size = $category->getSizeData();
        $isCorrectValue = false;
        if (!is_null($size))
            $isCorrectValue = $size->featureValues->contains('id', $request->size);
        //associate the size value with the product
        if($request->size && $isCorrectValue)
            $this->featureValues()->attach($request->size);

        //verify the validity of the transmitted value of child age
        $hasChildAge = $category->hasFeature(Feature::CHILD_AGE);
        $correctChildAgeValue = Feature::getFeature(Feature::CHILD_AGE)->featureValues
            ->contains('id', $request->child_age);
        //associate the child age value with the product
        if($request->child_age && $hasChildAge && $correctChildAgeValue)
            $this->featureValues()->attach($request->child_age);
    }

    /**
     * Register media conversions.
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->width(70)
            ->height(70);

        $this->addMediaConversion('medium')
            ->width(130)
            ->height(130);
    }

    /**
     * Get image url.
     *
     * @param string $conversion
     * @return string
     */
    public function getImageUrl($conversion = 'medium')
    {
        if($this->hasMedia(self::GENERAL_IMAGE)) {
            return $this->media()->first()->getUrl($conversion);
        } else {
            return asset('img/no-image-' . $conversion . '.png');
        }
    }

    /**
     * Get count of products.
     *
     * @return int
     */
    public static function getNumberOfProducts()
    {
        return self::all()->count();
    }

    /**
     * Get data for show view.
     *
     * @return array
     */
    public function getShowRouteData()
    {
        return array(
            'model' => $this,
            'gender' => $this->getFeatureValue(Feature::GENDER),
            'constitution' => $this->getFeatureValue(Feature::CONSTITUTION),
            'size' => $this->getSizeFeatureValue('content'),
            'childAge' => $this->getFeatureValue(Feature::CHILD_AGE),
        );
    }

    /**
     * Check that the price og the product is available.
     * @return bool
     */
    public function isPriceAvailable()
    {
        return $this->for_sale && !is_null($this->stock_id);

    }

    /**
     * Get current product price.
     *
     * @return mixed
     */
    public function getCurrentPrice()
    {
        $price = $this->price;
        $activeDiscount = (new Discount)->getActiveDiscount($this);

        if ($this->status == ProductStatus::SOLD_OUT)
            $price = $this->soldOutProduct->price;
        elseif (!is_null($activeDiscount))
            $price = $this->price - $this->price * $activeDiscount->percent/100;

        return $price;
    }

    /**
     * Get user revenue.
     * @return float|int|null
     */
    public function getUserRevenue()
    {
        $revenue = 0;

        if (!is_null($this->revenue_percent)) {
            if ($this->status == ProductStatus::SOLD_OUT)
                $revenue = $this->soldOutProduct->price * $this->revenue_percent/100;
            else
                $revenue = $this->getCurrentPrice() * $this->revenue_percent/100;
        }

        return $revenue;
    }

    /**
     * @param $data
     * @param $userId
     * @return Model
     */
    public function store($data, $userId)
    {
        if (!is_null($userId)) $user = User::find($userId);
        else $user = Auth::user();

        return $user->products()->create($data);
    }
}
