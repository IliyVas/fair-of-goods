<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['percent'];

    /**
     * Get all products.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * Get discount type.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function discountType()
    {
        return $this->belongsTo(DiscountType::class, 'type_id');
    }

    /**
     * Get active discount.
     *
     * @param $product
     * @return null
     */
    public function getActiveDiscount($product = null)
    {
        $activeDiscountType = DiscountType::getActiveDiscountType();
        $discount = null;

        if (!is_null($activeDiscountType)) {

            if (is_null($product))
                $discount= $activeDiscountType->discounts;
            else $discount = $product->discounts()->whereTypeId($activeDiscountType->id)->first();
        }

        return $discount;
    }
}
