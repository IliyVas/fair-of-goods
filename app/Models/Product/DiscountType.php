<?php

namespace App\Models\Product;

use App\Models\Fair\Fair;
use Illuminate\Database\Eloquent\Model;

class DiscountType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['start_day', 'finish_day'];

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function discounts()
    {
        return $this->hasMany(Discount::class, 'type_id');
    }

    /**
     * Get model of active today discount type.
     *
     * @return Model|null|static
     */
    public static function getActiveDiscountType()
    {
        $dayNum = Fair::getCurrentFairDaysCount(false);

        $discountType = self::where('start_day', '<=', $dayNum)
            ->where('finish_day', '>=', $dayNum)
            ->orWhere(function ($query) use ($dayNum) {
                $query
                    ->where('start_day', '<=', $dayNum)
                    ->whereNull('finish_day');
            })
            ->first();

        return $discountType;
    }

    /**
     * @return array
     */
    public static function getDiscountTypesNames()
    {
        $discountTypes = self::all();
        $discountTypesNames = [];

        if (count($discountTypes) > 0) {
            foreach ($discountTypes as $discountType) {
                if (is_null($discountType->finish_day))
                    $discountTypesNames[$discountType->id] = trans('basic.discountDescEnd', [
                        'start_day' => $discountType->start_day
                    ]);
                else
                    $discountTypesNames[$discountType->id] = trans('basic.discountDesc', [
                        'start_day' => $discountType->start_day,
                        'finish_day' => $discountType->finish_day
                    ]);
            }
        }

        return $discountTypesNames;
    }
}
