<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class SoldOutProduct extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price',
        'order_id',
        'sale_date',
        'user_id'
    ];

    /**
     * Get product.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
