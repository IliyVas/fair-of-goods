<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductHistoryType
{
    const CHANGED_STATUS = 'status';
    const CHANGED_DATA = 'data';
}

class ProductHistory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'content',
    ];
}
