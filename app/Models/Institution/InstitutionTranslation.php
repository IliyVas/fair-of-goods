<?php

namespace App\Models\Institution;

use Illuminate\Database\Eloquent\Model;

class InstitutionTranslation extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];
}
