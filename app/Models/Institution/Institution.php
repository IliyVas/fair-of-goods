<?php

namespace App\Models\Institution;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Institution extends Model
{
    use Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = ['name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];
}
