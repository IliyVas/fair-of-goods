<?php

namespace App\Models\Feature;

use Illuminate\Database\Eloquent\Model;

class FeatureValueTranslation extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content'];
}
