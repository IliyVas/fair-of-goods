<?php

namespace App\Models\Feature;

use App\Models\Category\Category;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    const CONSTITUTION = 'constitution';
    const CLOTHES_SIZE = 'clothes_size';
    const WOMEN_SIZE = 'women_size';
    const ACCESSORIES_SIZE = 'accessories_size';
    const FOOTWEAR_SIZE = 'footwear_size';
    const GENDER = 'gender';
    const CHILD_AGE = 'child_age';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['kind'];

    /**
     * Get all values of feature.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function featureValues()
    {
        return $this->hasMany(FeatureValue::class);
    }

    /**
     * Return all categories of selected feature.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Get feature model.
     *
     * @param $kind
     * @return Model|null|static
     */
    public static function getFeature($kind)
    {
        return self::where('kind', $kind)->first();
    }

    /**
     * @return array
     */
    public static function getSizeFeatures()
    {
        return [
            self::FOOTWEAR_SIZE,
            self::CLOTHES_SIZE,
            self::WOMEN_SIZE,
            self::ACCESSORIES_SIZE,
        ];
    }
}
