<?php

namespace App\Models\Feature;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class FeatureValue extends Model
{
    use Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = ['content'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];
}
