<?php

namespace App\Models;

use App\Mail\ResetPassword;
use App\Mail\UserRegistered;
use App\Models\Fair\Fair;
use App\Models\Product\Product;
use App\Models\Product\SoldOutProduct;
use App\Models\Product\ProductStatus;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Ultraware\Roles\Models\Role;
use Illuminate\Support\Facades\Mail;
use Ultraware\Roles\Traits\HasRoleAndPermission;
use Ultraware\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class User extends Authenticatable implements HasRoleAndPermissionContract, HasMediaConversions
{
    use Notifiable, HasRoleAndPermission, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'social',
        'token',
        'active',
        'provider',
        'provider_id',
        'first_name',
        'last_name',
        'birthday',
        'new_email',
        'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'birthday',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function socialProfile()
    {
        return $this->hasOne(SocialProfile::class);
    }

    /**
     * Get all products of user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Get all deliveries of user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }

    /**
     * Get sold out products data.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getSoldOutProducts()
    {
        return $this->hasMany(SoldOutProduct::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions()
    {
        return $this->hasMany(UserTransaction::class);
    }

    /**
     * Set token.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->token = str_random(30);
        });
    }

    /**
     * Attach role to user.
     */
    public function addUserRole()
    {
        $userRole = Role::where('name', 'User')->first();
        $this->attachRole($userRole);
    }

    /**
     * Confirm email.
     */
    public function confirmEmail()
    {
        $this->active = true;
        $this->token = null;

        if ($this->new_email) {
            $this->email = $this->new_email;
            $this->new_email = null;

            flash(trans('messages.emailChanged'))->success();
        } else {
            Mail::to($this->email)->send(new UserRegistered($this));
            flash(trans('messages.emailActivated'))->success();
        }
        $this->save();
    }

    /**
     * Register media conversions.
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
              ->width(30)
              ->height(30);

        $this->addMediaConversion('medium')
              ->width(115)
              ->height(115);
    }

    /**
     * @param string $conversion
     * @return string
     */
    public function getAvatarUrl($conversion = 'medium')
    {
        if($this->hasMedia()) {
            return $this->getMedia()->first()->getUrl($conversion);
        } else {
            return asset('img/person-placeholder-' . $conversion . '.jpg');
        }
    }

    /**
     * Products count
     *
     * @return int
     */
    public function productsCount()
    {
        $products = $this->products()->get();
        return $products->count();
    }

    /* Count donated products.
     *
     * @return int
     */
    public function getNumberOfDonatedProducts()
    {
        return count($this->products()->whereStatus(ProductStatus::DONATED_TO_CHARITY)->get());
    }

    /**
     * Count sold products.
     *
     * @return int
     */
    public function getNumberOfSoldProducts()
    {
        return count($this->getSoldOutProducts);
    }

    /**
     * Get total revenue;
     *
     * @return float|int
     */
    public function getTotalRevenue()
    {
        $soldOutProducts = $this->getSoldOutProducts;
        $totalRevenue = 0;

        if (count($soldOutProducts) > 0) {
            foreach ($soldOutProducts as $soldOutProduct) {
                $totalRevenue += $soldOutProduct->price*$soldOutProduct->product->revenue_percent/100;
            }
        }

        return $totalRevenue;
    }

    /**
     * Get the available user transaction amount.
     *
     * @return float|int
     */
    public function getAvailableTransactionAmount()
    {
        $revenue = $this->getTotalRevenue();
        $previousTransactions = $this->transactions;

        if (count($previousTransactions) > 0 && $revenue !== 0)
            $availableTransactionAmount = $revenue - $previousTransactions->sum('amount');
        else $availableTransactionAmount = $revenue;

        return $availableTransactionAmount;
    }

    /**
     * Get the user transactions amount.
     *
     * @return float|int
     */
    public function getTransactionsAmount()
    {
        $transactions = $this->transactions;
        $amount = 0;

        if (count($transactions) > 0)
            $amount = $transactions->sum('amount');

        return $amount;
    }

    /**
     * Get nearest delivery.
     *
     * @return $this
     */
    public function getNearestDelivery()
    {
        $deliveries = $this->deliveries()
            ->where('delivery_date', '>=', date('Y-m-d', time()))
            ->where('confirmed', false)
            ->where('courier', true)
            ->get();

        if (!count($deliveries) < 0) return null;

        return $this->deliveries()->where('delivery_date', $deliveries->min('delivery_date'))->first();
    }

    /**
     * Get user age.
     *
     * @return int
     */
    public function getAge()
    {
        return date_create($this->birthday)->diff(date_create(date('Y-m-d', time())))->y;
    }

    /**
     * Get count of users.
     *
     * @return int
     */
    public static function getNumberOfUsers()
    {
        $users = self::all();

        if ($users->count() > 0) {
            foreach ($users as $key => $user) {
                if ($user->isAdmin())
                    unset ($users[$key]);
            }
        }

        return $users->count();
    }

    /**
     * Get sold out products on fair.
     *
     * @param $fair
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getFairSoldOutProducts($fair)
    {
        $fairStartDate = date_create($fair->start_date)->format('Y-m-d H:i:s');
        $fairFinishDate = date_create($fair->finish_date)->format('Y-m-d H:i:s');

        return $this->getSoldOutProducts()
            ->where('sale_date', '>=', $fairStartDate)
            ->where('sale_date', '<=', $fairFinishDate)
            ->get();
    }

    /**
     * Get current fair revenue;
     *
     * @param $soldOutProducts
     * @return float|int
     */
    public function getCurrentFairRevenue($soldOutProducts)
    {
        $revenue = 0;
        if (count($soldOutProducts) > 0) {
            foreach ($soldOutProducts as $soldOutProduct) {
                $revenue += $soldOutProduct->price*$soldOutProduct->product->revenue_percent/100;
            }
        }

        return $revenue;
    }

    /**
     * Get fair products.
     *
     * @param $fair
     * @return mixed
     */
    public function getFairDonatedProducts($fair)
    {
        return $this->products()
            ->whereStatus(ProductStatus::DONATED_TO_CHARITY)
            ->where('updated_at', '>=', $fair->start_date)
            ->where('updated_at', '<=', $fair->finish_date)
            ->get();
    }

    /**
     * Get not sold fair products.
     *
     * @param $fair
     * @return mixed
     */
    public function getFairNotSoldProducts($fair)
    {
        return $this->products()
            ->whereNotIn('status', [ProductStatus::DONATED_TO_CHARITY, ProductStatus::SOLD_OUT])
            ->where('created_at', '<=', $fair->finish_date)
            ->get();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * @return bool
     */
    public function canWithdrawMoney()
    {
        $currentFair = Fair::getCurrentFair();
        $amount = $this->getAvailableTransactionAmount();

        return is_null($currentFair) && $amount !== 0;
    }
}
