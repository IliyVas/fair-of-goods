<?php

namespace App\Models;

use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Delivery extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'on_stock',
        'address',
        'delivery_date',
        'delivery_time',
        'user_id',
        'courier',
    ];

    /**
     * Get all products of deliveries.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * Get delivery user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Check possibility to delete.
     *
     * @return bool
     */
    public function isDeletable()
    {
        if (Auth::user()->isUser() && $this->delivery_date < date('Y-m-d', time() + 24*60*60))
            $isDeletable = false;
        else $isDeletable = true;

        return $isDeletable;
    }

    /**
     * Revert product status after delivery deletion.
     */
    public function revertProductStatus()
    {
        $products = $this->products;

        foreach ($products as $product) {
            if ($product->status === ProductStatus::COURIER_EXPECTED) {
                if ($this->on_stock)
                    $product->changeStatus(ProductStatus::REGISTERED);
                else
                    $product->changeStatus(ProductStatus::ON_STOCK);
            }
        }
    }

    /**
     * Check possibility to edit.
     *
     * @return bool
     */
    public function isEditable()
    {
        $isAdmin = Auth::user()->isAdmin();

        if ($this->confirmed)
            $isEditable = false;
        elseif (!$isAdmin && $this->delivery_date < Carbon::tomorrow()->format('Y-m-d'))
            $isEditable = false;
        else $isEditable = true;

        return $isEditable;
    }

    /**
     * Get full address string from $request.
     *
     * @param $request
     * @return string
     */
    public static function getFullAddressString($request)
    {
        if (is_null($request->apartment)) $address = $request->address;
        else $address = $request->address . '|' . $request->apartment;

        return $address;
    }

    /**
     * Associate the products with the delivery.
     *
     * @param $request
     * @param bool $create
     */
    public function saveProducts($request, $create = false)
    {
        foreach ($request->products as $productId) {
            $product = Product::getUserProduct($productId, $this->user_id);
            $isRegisteredStatus = $product->status === ProductStatus::REGISTERED;
            $isOnStockStatus = $product->status === ProductStatus::ON_STOCK;

            if ($isRegisteredStatus && $this->on_stock || $isOnStockStatus && !$this->on_stock) {
                //change products status
                $product->changeStatus(ProductStatus::COURIER_EXPECTED);
                //associate the products with the delivery
                $this->products()->attach($product->id);
            } else if (!$isRegisteredStatus && !$isOnStockStatus) {
                if ($create)
                    flash(trans('messages.noRightsForCreateDeliveryError',
                        ['status' => trans('basic.' . $product->status . 'ProductStatus')]), 'error');
                else
                    flash(trans('messages.invalidStatusEditError',
                    ['status' => trans('basic.' . $product->status . 'ProductStatus')]), 'error');
            } else {
                flash(trans('messages.notAuthorized'), 'error');
            }
        }
    }

    /**
     * Get count of deliveries today.
     *
     * @return mixed
     */
    public static function getNumberOfDeliveriesToday()
    {
       return self::whereDeliveryDate(Carbon::today()->format('Y-m-d'))->get()->count();
    }

    /**
     * Get delivery of user.
     *
     * @param $id
     * @return mixed|static
     */
    public static function getUserDelivery($id)
    {
        if (Auth::user()->isAdmin()) $delivery = self::find($id);
        else $delivery = Auth::user()->deliveries()->find($id);

        return $delivery;
    }

    /**
     * @param $data
     * @param $userId
     * @return Model
     */
    public function store($data, $userId)
    {
        if (!is_null($userId)) $user = User::find($userId);
        else $user = Auth::user();

        return $user->deliveries()->create($data);
    }
}