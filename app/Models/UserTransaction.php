<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTransactionStatus
{
    const SUCCEED = 'SUCCEED';
    const TAKENIN_NOTSENT = 'TAKENIN_NOTSENT';
}

class UserTransaction extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['amount', 'status', 'number'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
