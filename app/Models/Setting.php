<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    const REVENUE_PERCENT = 'percent of revenue';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'value'];

    /**
     * Get user percent of revenue.
     * @return int
     */
    public static function getRevenuePercent()
    {
        return (int) self::whereName(self::REVENUE_PERCENT)->first()->value;
    }
}
