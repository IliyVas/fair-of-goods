<?php

namespace App\Models\Category;

use App\Models\Feature\Feature;
use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class Category extends Model
{
    use Translatable;

    /**
     * @var array
     */
    public $translatedAttributes = ['name'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
     protected $with = ['translations'];

    /**
     * Get all subcategories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories()
     {
         return $this->hasMany(Category::class, 'parent_id');
     }

    /**
     * Get all features for selected category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function features()
     {
         return $this->belongsToMany(Feature::class);
     }

    /**
     * Check for availability feature of the category.
     *
     * @param $kind
     * @return bool
     */
    public function hasFeature($kind)
     {
         return !is_null($this->features()->where('kind', $kind)->first());
     }

    /**
     * Get all main categories.
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getCategories()
     {
         return self::where('parent_id', null)->get();
     }

    /**
     * @return Model|null|static
     */
    public function getSizeData()
    {
        $size = null;
        $sizeTypes = Feature::getSizeFeatures();

        foreach ($sizeTypes as $sizeType)
            if ($this->hasFeature($sizeType))
                $size = Feature::getFeature($sizeType);

        return $size;
    }
}
