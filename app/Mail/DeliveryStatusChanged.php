<?php

namespace App\Mail;

use App\Models\Delivery;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DeliveryStatusChanged extends Mailable
{
    use Queueable, SerializesModels;

    public $delivery;

    /**
     * EmailChanged constructor.
     *
     * @param Delivery $delivery
     */
    public function __construct(Delivery $delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))->view('emails.delivery_status_changed', [
            'delivery' => $this->delivery
        ]);
    }
}
