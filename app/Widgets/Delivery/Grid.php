<?php

namespace App\Widgets\Delivery;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;

class Grid extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'deliveries'           => [],
        'user'                 => null,
        'routeName'            => 'deliveries.edit',
        'deleteRoute'          => '',
        'showProductRouteName' => ''
    ];

    /**
     * Grid constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        $this->config['deliveries'] = $data['deliveries'];
        $this->config['deleteRoute'] = $data['deleteRoute'];
        $this->config['user'] = Auth::user();

        if (isset($data['routeName']))
            $this->config['routeName'] = $data['routeName'];

        if (isset($data['showIssueExportBtn']) && isset($data['issueExportRoute'])) {
            $this->config['showIssueExportBtn'] = $data['showIssueExportBtn'];
            $this->config['issueExportRoute'] = $data['issueExportRoute'];
        }

        if (isset($data['showProductRouteName']))
            $this->config['showProductRouteName'] = $data['showProductRouteName'];
    }

    /**
     * Return delivery view.
     */
    public function run()
    {
        return view('widgets.delivery.grid', $this->config);
    }
}
