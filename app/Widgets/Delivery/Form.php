<?php

namespace App\Widgets\Delivery;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;

class Form extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'delivery'         => null,
        'products'         => [],
        'user'             => null,
        'selectedProducts' => null,
        'pageName'         => '',
        'route'            => '',
        'editing'          => true,
        'method'           => 'PATCH',
    ];

    /**
     * Form constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        $this->config['user'] = Auth::user();
        $this->config['delivery'] = $data['delivery'];

        if (isset($data['products']))
            $this->config['products'] = $data['products'];
        else $this->config['products'] = Auth::user()->products;

        if (isset($data['pageName']))
            $this->config['pageName'] = $data['pageName'];
        else $this->config['pageName'] = trans('basic.editDelivery');

        if (isset($data['selectedProducts']))
            $this->config['selectedProducts'] = $data['selectedProducts'];
        else $this->config['selectedProducts'] = $data['delivery']->products->pluck('id')->all();

        if (isset($data['route']))
            $this->config['route'] = $data['route'];
        else
            $this->config['route'] = route('deliveries.store');

        if (isset($data['editing']))
            $this->config['editing'] = $data['editing'];

        if (isset($data['method']))
            $this->config['method'] = $data['method'];
    }

    /**
     * Return delivery view.
     */
    public function run()
    {
        return view('widgets.delivery.form', $this->config);
    }
}
