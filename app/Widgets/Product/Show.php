<?php

namespace App\Widgets\Product;

use Arrilot\Widgets\AbstractWidget;

class Show extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'admin'       => false,
        'editRoute'   => 'products.edit',
        'deleteRoute' => 'products.destroy',
    ];

    /**
     * Show constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        if (isset($data['admin']))
        $this->config['admin'] = $data['admin'];

        if (isset($data['editRoute']))
            $this->config['editRoute'] = $data['editRoute'];

        if (isset($data['deleteRoute']))
            $this->config['deleteRoute'] = $data['deleteRoute'];

        $this->config = array_merge($this->config, $data['model']->getShowRouteData());
    }

    /**
     * Return product view.
     */
    public function run()
    {
        return view('widgets.product.show', $this->config);
    }
}
