<?php

namespace App\Widgets\Product;

use Arrilot\Widgets\AbstractWidget;

class DownloadPriceList extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'downloadUrls' => [],
    ];

    /**
     * Return product form.
     */
    public function run()
    {
        return view('widgets.product.download_pricelist', $this->config);
    }
}
