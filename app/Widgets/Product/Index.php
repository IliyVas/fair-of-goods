<?php

namespace App\Widgets\Product;

use Arrilot\Widgets\AbstractWidget;

class Index extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'products'               => [],
        'admin'                  => false,
        'editRoute'              => 'products.edit',
        'deleteRoute'            => '',
        'showRoute'              => 'products.show',
        'showCreateItemBtn'      => false,
        'createItemRoute'        => '',
        'issueExportRoute'       => '',
        'showIssueExportBtn'     => false,
        'downloadItemsRoute'     => '',
        'downloadPriceListRoute' => '',
        'duplicateRoute'         => 'products.duplicate',
    ];

    /**
     * Return products list view.
     */
    public function run()
    {
        return view('widgets.product.index', $this->config);
    }
}
