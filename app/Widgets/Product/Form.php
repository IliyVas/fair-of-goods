<?php

namespace App\Widgets\Product;

use App\Models\Category\Category;
use App\Models\Feature\Feature;
use Arrilot\Widgets\AbstractWidget;

class Form extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'product'    => null,
        'categories' => [],
        'gender'     => null,
        'pageName'   => '',
        'route'      => 'products.store',
        'method'     => 'POST',
    ];

    /**
     * Form constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        $this->config['categories'] = Category::getCategories();
        $this->config['gender'] = Feature::getFeature(Feature::GENDER);
        $this->config['pageName'] = $data['pageName'];

        if (isset($data['route']))
            $this->config['route'] = $data['route'];

        if (isset($data['product']))
            $this->config['product'] = $data['product'];

        if (isset($data['method']))
            $this->config['method'] = $data['method'];
    }

    /**
     * Return product form.
     */
    public function run()
    {
        return view('widgets.product.form', $this->config);
    }
}
