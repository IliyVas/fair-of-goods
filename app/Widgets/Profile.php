<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class Profile extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'user'  => null,
        'admin' => false,
    ];

    /**
     * Return profile view.
     */
    public function run()
    {
        return view('widgets.profile', $this->config);
    }
}
