<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Product\ProductStatus;

class CreateProductsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create table for products
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('stock_id')->nullable();
            $table->string('enter_id')->nullable();
            $table->integer('category_id')->unsigned()->index();
            $table->integer('institution_id')->unsigned()->nullable()->index();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('status')->default(ProductStatus::REGISTERED)->index();
            $table->boolean('for_sale')->default(false)->index();
            $table->integer('price')->default(0);
            $table->boolean('donate_after_fair')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('institution_id')->references('id')->on('institutions')->onDelete('cascade');
        });

        //create table for sold products
        Schema::create('sold_out_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->index();
            $table->string('order_id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('price');
            $table->dateTime('sale_date');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        //create table for discount types
        Schema::create('discount_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('start_day');
            $table->integer('finish_day')->nullable();
            $table->timestamps();
        });

        //create table for discounts
        Schema::create('discounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('percent');
            $table->integer('type_id')->unsigned();
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('discount_types')->onDelete('cascade');
        });

        //create table for associating products to discounts (Many-to-Many)
        Schema::create('discount_product', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('discount_id')->unsigned();

            $table->unique(['discount_id','product_id']);
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('discount_id')->references('id')->on('discounts')->onDelete('cascade');
        });

        //create table for associating products to feature values (Many-to-Many)
        Schema::create('feature_value_product', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('feature_value_id')->unsigned();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('feature_value_id')->references('id')->on('feature_values')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['category_id']);
            $table->dropForeign(['institution_id']);
        });

        Schema::table('sold_out_products', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::table('discounts', function (Blueprint $table) {
            $table->dropForeign(['type_id']);
        });

        Schema::table('discount_product', function (Blueprint $table) {
            $table->dropForeign(['discount_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::table('feature_value_product', function (Blueprint $table) {
            $table->dropForeign(['feature_value_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::dropIfExists('feature_value_product');
        Schema::dropIfExists('discount_product');
        Schema::dropIfExists('sold_out_products');
        Schema::dropIfExists('products');
        Schema::dropIfExists('discounts');
        Schema::dropIfExists('discount_types');
    }
}
