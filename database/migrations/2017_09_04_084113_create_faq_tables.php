<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('question_answer_pairs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->nullable()->unsigned()->index();
            $table->foreign('category_id', 'question_answer_pairs_category_id')->references('id')->on('question_categories')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('question_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_category_id')->unsigned();
            $table->string('name');
            $table->string('locale')->index();
            $table->foreign('question_category_id')->references('id')->on('question_categories')->onDelete('cascade');
        });

        Schema::create('question_answer_pair_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_answer_pair_id')->unsigned();
            $table->text('question');
            $table->text('answer');
            $table->string('locale')->index();
            $table->foreign('question_answer_pair_id', 'question_answer_pair_translations_pair_id')
                ->references('id')->on('question_answer_pairs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_answer_pairs', function (Blueprint $table) {
            $table->dropForeign('question_answer_pairs_category_id');
        });

        Schema::table('question_category_translations', function (Blueprint $table) {
            $table->dropForeign(['question_category_id']);
        });

        Schema::table('question_answer_pair_translations', function (Blueprint $table) {
            $table->dropForeign('question_answer_pair_translations_pair_id');
        });

        Schema::dropIfExists('question_answer_pair_translations');
        Schema::dropIfExists('question_answer_pairs');
        Schema::dropIfExists('question_category_translations');
        Schema::dropIfExists('question_categories');
    }
}
