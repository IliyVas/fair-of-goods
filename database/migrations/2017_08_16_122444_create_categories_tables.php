<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create table for categories
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('cascade');
        });

        //create table for category translations
        Schema::create('category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('name');
            $table->string('locale')->index();

            $table->unique(['category_id','locale']);
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

        //create table for feature
        Schema::create('features', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kind')->unique();
            $table->timestamps();
        });

        //create table for feature values
        Schema::create('feature_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feature_id')->unsigned();
            $table->timestamps();

            $table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
        });

        //create table for feature value translations
        Schema::create('feature_value_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feature_value_id')->unsigned();
            $table->string('content');
            $table->string('locale')->index();

            $table->unique(['feature_value_id','locale']);
            $table->foreign('feature_value_id')->references('id')->on('feature_values')->onDelete('cascade');
        });

        //create table for associating features to categories (Many-to-Many)
        Schema::create('category_feature', function (Blueprint $table) {
            $table->integer('feature_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->foreign('feature_id')->references('id')->on('features')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropForeign(['parent_id']);
        });

        Schema::table('category_translations', function (Blueprint $table) {
            $table->dropForeign(['category_id']);
        });

        Schema::table('feature_values', function (Blueprint $table) {
            $table->dropForeign(['feature_id']);
        });

        Schema::table('feature_value_translations', function (Blueprint $table) {
            $table->dropForeign(['feature_value_id']);
        });

        Schema::table('category_feature', function (Blueprint $table) {
            $table->dropForeign(['feature_id']);
            $table->dropForeign(['category_id']);
        });

        Schema::dropIfExists('category_translations');
        Schema::dropIfExists('category_feature');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('feature_value_translations');
        Schema::dropIfExists('feature_values');
        Schema::dropIfExists('features');
    }
}
