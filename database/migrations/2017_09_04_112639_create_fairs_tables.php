<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFairsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fairs', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start_date');
            $table->date('finish_date');
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->timestamps();
        });

        Schema::create('fair_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fair_id')->unsigned();
            $table->string('name');
            $table->string('description');
            $table->string('address');
            $table->string('locale')->index();

            $table->unique(['fair_id','locale']);
            $table->foreign('fair_id')->references('id')->on('fairs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fair_translations', function (Blueprint $table) {
            $table->dropForeign(['fair_id']);
        });

        Schema::dropIfExists('fair_translations');
        Schema::dropIfExists('fairs');
    }
}
