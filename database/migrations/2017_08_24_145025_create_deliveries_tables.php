<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //create table for deliveries
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('on_stock')->default(true);
            $table->boolean('confirmed')->default(false);
            $table->text('address')->nullable();
            $table->date('delivery_date');
            $table->string('delivery_time');
            $table->timestamps();
        });

        //create table for associating products to deliveries (Many-to-Many)
        Schema::create('delivery_product', function (Blueprint $table) {
            $table->integer('product_id')->unsigned();
            $table->integer('delivery_id')->unsigned();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('delivery_id')->references('id')->on('deliveries')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_product', function (Blueprint $table) {
            $table->dropForeign(['delivery_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::dropIfExists('delivery_product');
        Schema::dropIfExists('deliveries');
    }
}
