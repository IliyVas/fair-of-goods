<?php

use Illuminate\Database\Seeder;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Models\Category\Category;
use App\Models\Institution\Institution;
use App\Models\Feature\Feature;
use App\Models\User;
use App\Models\Setting;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::getCategories();
        $institutions = Institution::all();
        $statuses = [ProductStatus::REGISTERED, ProductStatus::COURIER_EXPECTED];
        $users = User::whereFirstName('User')->get();

        for ($i = 1; $i <= 30; $i++) {
            $user = $users->random();
            $subcategory = $categories->random()->subcategories->random();

            if ($i <= (count($statuses) - 1))
                $status = $statuses[$i];
            else $status = $statuses[rand(0, count($statuses) - 1)];

            $data = [
                'category_id' => $subcategory->id,
                'name' => 'Наименование вещи №' . $i,
                'description' => 'Описание вещи №' . $i,
                'status' => $status,
                'institution_id' => $institutions->random()->id,
            ];

            $product = $user->products()->create($data);

            //attach constitution
            if ($subcategory->hasFeature(Feature::CONSTITUTION))
                $product->featureValues()->attach(Feature::getFeature(Feature::CONSTITUTION)->featureValues
                    ->random()->id);

            //attach size
            if ($subcategory->hasFeature(Feature::CLOTHES_SIZE))
                $product->featureValues()->attach(Feature::getFeature(Feature::CLOTHES_SIZE)->featureValues
                    ->random()->id);
            elseif ($subcategory->hasFeature(Feature::ACCESSORIES_SIZE))
                $product->featureValues()->attach(Feature::getFeature(Feature::ACCESSORIES_SIZE)->featureValues
                    ->random()->id);
            elseif ($subcategory->hasFeature(Feature::WOMEN_SIZE))
                $product->featureValues()->attach(Feature::getFeature(Feature::WOMEN_SIZE)->featureValues
                    ->random()->id);
            elseif ($subcategory->hasFeature(Feature::FOOTWEAR_SIZE))
                $product->featureValues()->attach(Feature::getFeature(Feature::FOOTWEAR_SIZE)->featureValues
                    ->random()->id);

            //attach gender
            $product->featureValues()->attach(Feature::getFeature(Feature::GENDER)->featureValues
                ->random()->id);

            $product->addMediaFromUrl(base_path('resources/assets/flatlab/img/product-list/pro-thumb-' . rand(1, 3) . '.jpg'))
                ->toMediaCollection(Product::GENERAL_IMAGE);

            if ($product->status === ProductStatus::COURIER_EXPECTED)
                $product->deliveries()->create([
                    'user_id' => $product->user_id,
                    'address' => 'Республика Беларусь, г.Могилев, ул.Первомайская, 20|18',
                    'delivery_date' => date('Y-m-d', time() + (rand(3, 7)*24*60*60)),
                    'delivery_time' => '10.00-13.00'
                ]);

//            $product->forceFill([
//                'for_sale' => true,
//                'donate_after_fair' => true,
//                'revenue_percent' => Setting::getRevenuePercent(),
//                'price' => rand(555,1000),
//            ])->save();
        }
    }
}
