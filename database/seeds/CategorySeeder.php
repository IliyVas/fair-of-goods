<?php

use Illuminate\Database\Seeder;
use App\Models\Category\Category;
use App\Models\Feature\Feature;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => [
                    'name:en' => 'Children\'s clothing and footwear',
                    'name:ru' => 'Детская одежда и обувь',
                    ],
                'subcategories' => [
                    [
                        'name:en' => 'Pants',
                        'name:ru' => 'Брюки',
                        'constitution' => true,
                        'clothes_size' => true,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Outerwear',
                        'name:ru' => 'Верхняя одежда',
                        'constitution' => true,
                        'clothes_size' => true,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Knitwear',
                        'name:ru' => 'Трикотаж',
                        'constitution' => true,
                        'clothes_size' => true,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Dresses and skirts',
                        'name:ru' => 'Платья и юбки',
                        'constitution' => true,
                        'clothes_size' => true,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Footwear',
                        'name:ru' => 'Обувь',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => true,
                    ],
                    [
                        'name:en' => 'Caps, mittens, scarves',
                        'name:ru' => 'Шапки, варежки, шарфы',
                        'constitution' => true,
                        'clothes_size' => false,
                        'accessories_size' => true,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Sportswear',
                        'name:ru' => 'Спортивная одежда',
                        'constitution' => true,
                        'clothes_size' => true,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Sport shoes',
                        'name:ru' => 'Спортивная обувь',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => true,
                    ],
                    [
                        'name:en' => 'Socks, tights',
                        'name:ru' => 'Носки, колготки',
                        'constitution' => false,
                        'clothes_size' => true,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Dance, festive clothes, carnival costumes and paraphernalia',
                        'name:ru' => 'Танцевальная, праздничная одежда, карнавальные костюмы и атрибутика',
                        'constitution' => true,
                        'clothes_size' => true,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Pajamas and bathrobes',
                        'name:ru' => 'Пижамы и халаты',
                        'constitution' => true,
                        'clothes_size' => true,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Other',
                        'name:ru' => 'Другое',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                ]
            ],
            [
                'name' => [
                    'name:en' => 'Goods for children and toys',
                    'name:ru' => 'Товары для детей и игрушки',
                ],
                'subcategories' => [
                    [
                        'name:en' => 'Car seats',
                        'name:ru' => 'Автомобильные кресла',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Bicycles and scooters',
                        'name:ru' => 'Велосипеды и самокаты',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Books',
                        'name:ru' => 'Книги',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Carriages',
                        'name:ru' => 'Коляски',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Toys',
                        'name:ru' => 'Игрушки',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Goods for feeding',
                        'name:ru' => 'Товары для кормления',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Goods for bathing',
                        'name:ru' => 'Товары для купания',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Bed dress',
                        'name:ru' => 'Постельные принадлежности',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'School supplies',
                        'name:ru' => 'Школьные принадлежности',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Sporting goods',
                        'name:ru' => 'Спортивные товары',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Other',
                        'name:ru' => 'Другое',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => true,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                ]
            ],
            [
                'name' => [
                    'name:en' => 'Goods for women',
                    'name:ru' => 'Товары для женщин',
                ],
                'subcategories' => [
                    [
                        'name:en' => 'Clothes for pregnant women',
                        'name:ru' => 'Одежда для беременных',
                        'constitution' => true,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => true,
                        'footwear_size' => false,
                    ],
                    [
                        'name:en' => 'Slings',
                        'name:ru' => 'Слинги',
                        'constitution' => false,
                        'clothes_size' => false,
                        'accessories_size' => false,
                        'child_age' => false,
                        'women_size' => false,
                        'footwear_size' => false,
                    ],
                ]
            ],
        ];

        //create age of child as feature
        $childAgeData = [
            'feature' => [
                'kind' => 'child_age',
            ],
            'feature_value' => [
                [
                    'content:en' => 'At 0-3 month old',
                    'content:ru' => 'На 0-3 мес',
                ],
                [
                    'content:en' => 'At 3-6 month old',
                    'content:ru' => 'На 3-6 мес',
                ],
                [
                    'content:en' => 'At 6-9 month old',
                    'content:ru' => 'На 6-9 мес',
                ],
                [
                    'content:en' => 'At 9-12 month old',
                    'content:ru' => 'На 9-12 мес',
                ],
                [
                    'content:en' => 'At 1-1,5 years old',
                    'content:ru' => 'На 1-1,5 года',
                ],
                [
                    'content:en' => 'At 1,5-2 years old',
                    'content:ru' => 'На 1,5-2 года',
                ],
                [
                    'content:en' => 'At 3 years old',
                    'content:ru' => 'На 3 года',
                ],
                [
                    'content:en' => 'At 4 years old',
                    'content:ru' => 'На 4 года',
                ],
                [
                    'content:en' => 'At 5 years old',
                    'content:ru' => 'На 5 лет',
                ],
                [
                    'content:en' => 'At 6 years old',
                    'content:ru' => 'На 6 лет',
                ],
                [
                    'content:en' => 'At 7 years old',
                    'content:ru' => 'На 7 лет',
                ],
                [
                    'content:en' => 'At 8 years old',
                    'content:ru' => 'На 8 лет',
                ],
                [
                    'content:en' => 'At 9 years old',
                    'content:ru' => 'На 9 лет',
                ],
                [
                    'content:en' => 'At 10 years old',
                    'content:ru' => 'На 10 лет',
                ],
                [
                    'content:en' => 'At 11 years old',
                    'content:ru' => 'На 11 лет',
                ],
                [
                    'content:en' => 'At 12 years old',
                    'content:ru' => 'На 12 лет',
                ],
            ]
        ];
        $childAge = Feature::create($childAgeData['feature']);
        //add feature values
        foreach ($childAgeData['feature_value'] as $featureValue) {
            $childAge->featureValues()->create($featureValue);
        }

        //create constitution as feature
        $constitutionData = [
            'feature' => [
                'kind' => 'constitution',
            ],
            'feature_value' => [
                [
                    'content:en' => 'Slender',
                    'content:ru' => 'Стройная',
                ],
                [
                    'content:en' => 'Average',
                    'content:ru' => 'Средняя',
                ],
                [
                    'content:en' => 'Stout',
                    'content:ru' => 'Плотная',
                ],
            ]
        ];
        $constitution = Feature::create($constitutionData['feature']);
        //add feature values
        foreach ($constitutionData['feature_value'] as $featureValue) {
            $constitution->featureValues()->create($featureValue);
        }

        //create footwear size as feature
        $footwearSizeData = [
            'feature' => [
                'kind' => 'footwear_size',
            ],
            'feature_value' => []
        ];
        for ($i = 16; $i <= 35; $i++)
        {
            $footwearSizeData['feature_value'][] = ['content:ru' => $i, 'content:en' => $i];
        }
        $footwearSize = Feature::create($footwearSizeData['feature']);
        //add feature values
        foreach ($footwearSizeData['feature_value'] as $featureValue) {
            $footwearSize->featureValues()->create($featureValue);
        }

        //create clothes size as feature
        $clothesSizeData = [
            'feature' => [
                'kind' => 'clothes_size',
            ],
            'feature_value' => [
                [
                    'content:en' => 'Less than or equal to 50 cm',
                    'content:ru' => 'Меньше или равно 50 см',
                ],
                [
                    'content:en' => '50-56 cm',
                    'content:ru' => '50-56 см',
                ],
                [
                    'content:en' => '56-62 cm',
                    'content:ru' => '56-62 см',
                ],
                [
                    'content:en' => '62-68 cm',
                    'content:ru' => '62-68 см',
                ],
                [
                    'content:en' => '68-74 cm',
                    'content:ru' => '68-74 см',
                ],
                [
                    'content:en' => '74-80 cm',
                    'content:ru' => '74-80 см',
                ],
                [
                    'content:en' => '80-86 cm',
                    'content:ru' => '80-86 см',
                ],
                [
                    'content:en' => '86-92 cm',
                    'content:ru' => '86-92 см',
                ],
                [
                    'content:en' => '92-98 cm',
                    'content:ru' => '92-98 см',
                ],
                [
                    'content:en' => '98-104 cm',
                    'content:ru' => '98-104 см',
                ],
                [
                    'content:en' => '104-110 cm',
                    'content:ru' => '104-110 см',
                ],
                [
                    'content:en' => '110-116 cm',
                    'content:ru' => '110-116 см',
                ],
                [
                    'content:en' => '116-122 cm',
                    'content:ru' => '116-122 см',
                ],
                [
                    'content:en' => '122-128 cm',
                    'content:ru' => '122-128 см',
                ],
                [
                    'content:en' => '128-134 cm',
                    'content:ru' => '128-134 см',
                ],
                [
                    'content:en' => '134-140 cm',
                    'content:ru' => '134-140 см',
                ],
                [
                    'content:en' => '140-146 cm',
                    'content:ru' => '140-146 см',
                ],
                [
                    'content:en' => '146-152 cm',
                    'content:ru' => '146-152 см',
                ],
                [
                    'content:en' => 'Without size',
                    'content:ru' => 'Без размера',
                ],
            ]
        ];
        $clothesSize = Feature::create($clothesSizeData['feature']);
        //add feature values
        foreach ($clothesSizeData['feature_value'] as $featureValue) {
            $clothesSize->featureValues()->create($featureValue);
        }

        //create women size as feature
        $womenSizeData = [
            'feature' => [
                'kind' => 'women_size',
            ],
            'feature_value' => [
                [
                    'content:en' => 'XXS',
                    'content:ru' => '38',
                ],
                [
                    'content:en' => 'XS',
                    'content:ru' => '40',
                ],
                [
                    'content:en' => 'S',
                    'content:ru' => '42',
                ],
                [
                    'content:en' => 'M',
                    'content:ru' => '44',
                ],
                [
                    'content:en' => 'M',
                    'content:ru' => '46',
                ],
                [
                    'content:en' => 'L',
                    'content:ru' => '48',
                ],
                [
                    'content:en' => 'L',
                    'content:ru' => '50',
                ],
                [
                    'content:en' => 'XL',
                    'content:ru' => '52',
                ],
                [
                    'content:en' => 'XXL',
                    'content:ru' => '54',
                ],
            ]
        ];
        $womenSize = Feature::create($womenSizeData['feature']);
        //add feature values
        foreach ($womenSizeData['feature_value'] as $featureValue) {
            $womenSize->featureValues()->create($featureValue);
        }

        //create accessories size as feature
        $accessoriesSizeData = [
            'feature' => [
                'kind' => 'accessories_size',
            ],
            'feature_value' => [
                [
                    'content:en' => 'Less than or equal to 18',
                    'content:ru' => 'Mеньше или равно 18',
                ]
            ]
        ];
        for ($i = 19; $i <= 33; $i++)
        {
            $accessoriesSizeData['feature_value'][] = ['content:ru' => $i, 'content:en' => $i];
        }
        $accessoriesSize = Feature::create($accessoriesSizeData['feature']);
        //add feature values
        foreach ($accessoriesSizeData['feature_value'] as $featureValue) {
            $accessoriesSize->featureValues()->create($featureValue);
        }

        //create gender as feature
        $genderData = [
            'feature' => [
                'kind' => 'gender',
            ],
            'feature_value' => [
                [
                    'content:en' => 'Suitable for boys and girls',
                    'content:ru' => 'Подойдет и мальчикам, и девочкам',
                ],
                [
                    'content:en' => 'For girls',
                    'content:ru' => 'Для девочек',
                ],
                [
                    'content:en' => 'For boys',
                    'content:ru' => 'Для мальчиков',
                ],
                [
                    'content:en' => 'For women',
                    'content:ru' => 'Для женщин',
                ],
            ]
        ];
        $gender = Feature::create($genderData['feature']);
        //add feature values
        foreach ($genderData['feature_value'] as $featureValue) {
            $gender->featureValues()->create($featureValue);
        }

        foreach ($categories as $categoryKey => $categoryValue) {
            $category = Category::create($categoryValue['name']);

            //create subcategories
            foreach ($categoryValue['subcategories'] as $subcategory) {
                //check child age feature
                $hasChildAge = $subcategory['child_age'];
                unset($subcategory['child_age']);

                //check constitution feature
                $hasConstitution = $subcategory['constitution'];
                unset($subcategory['constitution']);

                //check clothes size feature
                $hasClothesSize = $subcategory['clothes_size'];
                unset($subcategory['clothes_size']);

                //check women size feature
                $hasWomenSize = $subcategory['women_size'];
                unset($subcategory['women_size']);

                //check accessories size feature
                $hasAccessoriesSize = $subcategory['accessories_size'];
                unset($subcategory['accessories_size']);

                //check footwear size feature
                $hasFootwearSize = $subcategory['footwear_size'];
                unset($subcategory['footwear_size']);

                $subcategory = $category->subcategories()->create($subcategory);

                //attach category to child age feature
                if ($hasChildAge)
                    $subcategory->features()->attach($childAge->id);

                //attach category to constitution feature
                if ($hasConstitution)
                    $subcategory->features()->attach($constitution->id);

                //attach category to clothes size feature
                if ($hasClothesSize)
                    $subcategory->features()->attach($clothesSize->id);

                //attach category to women size feature
                if ($hasWomenSize)
                    $subcategory->features()->attach($womenSize->id);

                //attach category to accessories size feature
                if ($hasAccessoriesSize)
                    $subcategory->features()->attach($accessoriesSize->id);

                //attach category to footwear size feature
                if ($hasFootwearSize)
                    $subcategory->features()->attach($footwearSize->id);

                //attach category to gender feature
                $subcategory->features()->attach($gender->id);
            }
        }
    }
}
