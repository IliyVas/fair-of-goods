<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SettingsSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(InstitutionSeeder::class);
        $this->call(DiscountSeeder::class);

        if (!env('APP_PROD')) {
            $this->call(ProductSeeder::class);
        }

        $this->call(FAQSeeder::class);
        $this->call(FairSeeder::class);
    }
}
