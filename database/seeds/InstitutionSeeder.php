<?php

use Illuminate\Database\Seeder;
use App\Models\Institution\Institution;

class InstitutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $institutions = [
            [
                'name:en' => 'Any',
                'name:ru' => 'Любая',
            ],
            [
                'name:en' => 'Charitable Foundation "Creation"',
                'name:ru' => 'Благотворительный фонд "Созидание"',
            ],
            [
                'name:en' => 'Committee "Civil Assistance"',
                'name:ru' => 'Комитет "Гражданское содействие"',
            ],
        ];

        foreach ($institutions as $institution) {
            Institution::create($institution);
        }
    }
}
