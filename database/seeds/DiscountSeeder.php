<?php

use Illuminate\Database\Seeder;
use App\Models\Product\DiscountType;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'start_day'  => 4,
                'finish_day' => 5,
            ],
            [
                'start_day' => 6,
            ]
        ];

       foreach ($types as $type) {
           $result = DiscountType::create($type);

           $result->discounts()->create(['percent'    => 20]);
           $result->discounts()->create(['percent'    => 50]);
       }
    }
}
