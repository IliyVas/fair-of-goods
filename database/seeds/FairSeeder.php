<?php

use Illuminate\Database\Seeder;
use App\Models\Fair\Fair;

class FairSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Fair::create([
            'name:en' => 'Fair of Goods "The Second Life of Children\'s Things"',
            'name:ru' => 'Базар добра "Вторая жизнь детских вещей"',
            'description:en' => 'We give welfare to families and a second life for children\'s things!',
            'description:ru' => 'Мы дарим благосостояние семьям и вторую жизнь детским вещам!',
            'address:en' => 'Moscow, Kronshtadtsky Boulevard, 9',
            'address:ru' => 'Москва, Кронштадтский бульвар, 9',
            'start_date' => '2017-12-12',
            'finish_date' => '2017-12-17',
        ]);

        if (!env('APP_PROD')) {
            for ($i = 1; $i <= 3; $i++) {
                Fair::create([
                    'name:en' => str_random(7),
                    'name:ru' => str_random(7),
                    'description:en' => str_random(7) . ' ' . str_random(5),
                    'description:ru' => str_random(7) . ' ' . str_random(5),
                    'address:en' => 'Moscow, Kronshtadtsky Boulevard, 9',
                    'address:ru' => 'Москва, Кронштадтский бульвар, 9',
                    'start_date' => date('Y-m-d', time() - $i*7*24*60*60),
                    'finish_date' => date('Y-m-d', time() - $i*5*24*60*60),
                ]);
            }

        }
    }
}
