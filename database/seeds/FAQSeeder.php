<?php

use Illuminate\Database\Seeder;
use App\Models\faq\QuestionCategory;

class FAQSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 5; $i++) {
            QuestionCategory::create([
                'name:en' => 'Question category №' . $i,
                'name:ru' => 'Категория вопроса №' . $i,
            ]);
        }

        for ($i = 1; $i <= 50; $i++) {
            $category = QuestionCategory::all()->random();
            $category->questions()->create([
                'question:en' => 'Question №' . $i . ' from the category ' . $category->getTranslation('en')->name . '?',
                'answer:en' => 'Answer to a question №' . $i . ' from the category ' . $category->getTranslation('en')->name . '.',
                'question:ru' => 'Вопрос №' . $i . ' из категории ' . $category->getTranslation('ru')->name . '?',
                'answer:ru' => 'Ответ на вопрос №' . $i . ' из категории ' . $category->getTranslation('ru')->name . '.',
            ]);
        }
    }
}
