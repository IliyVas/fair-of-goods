<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Ultraware\Roles\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create admin role and users
        $adminRole = Role::create(['name' => 'Admin', 'slug' => 'admin']);
        $admin = User::create([
            'active' => true,
            'email' => 'admin@yuliya-titova.com',
            'password' => bcrypt('admin'),
            'first_name' => 'Yuliya',
            'phone' => '+79067896093'
        ]);
        $admin->attachRole($adminRole);

        //create user role and users
        $userRole = Role::create(['name' => 'User', 'slug' => 'user']);
        if (!env('APP_PROD')) {
            for ($i = 1; $i <= 3; $i++) {
                $user = User::create([
                    'active' => true,
                    'email' => 'user' . $i . '@user.com',
                    'password' => bcrypt('user'),
                    'first_name' => 'User',
                    'last_name' => str_random(rand(3, 8)),
                    'phone' => '+79067896093',
                    'birthday' => date('Y-m-d', time() - 18*365*24*60*60*$i)
                ]);

                $user->attachRole($userRole);
            }
        }
    }
}
