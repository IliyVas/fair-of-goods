<?php

return [
    'sklad_username' => env('SKLAD_USERNAME',''),
    'sklad_password' => env('SKLAD_PASSWORD',''),
    'base_api_sklad' => 'https://online.moysklad.ru/api/remap/1.1'
];
