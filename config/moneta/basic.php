<?php

use Moneta\MonetaSdk;

return [

    'public_id' => env('MONETA_PUBLIC_ID'),
    'url'       => (new MonetaSdk(config_path('moneta/')))->getSettingValue('monetasdk_production_url'),
    'account'   => [
        'id'                    => env('MONETA_MNT_ID'),
        'demo_id'               => '19312235',
        'username'              => env('MONETA_USERNAME', 'demo@moneta.ru'),
        'password'              => env('MONETA_PASSWORD', 'demo@moneta.ru'),
        'pay_password_enrypted' => env('MONETA_PAY_PASSWORD')
    ],
    'sdk'       => [
        'connection_type' => 'json',
        'demo_mode'       => '0',
        'test_mode'       => '0',
        'debug_mode'      => '0',
    ],

];