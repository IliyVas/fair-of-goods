var lang = $('html').attr('lang');

var EditableTable = function () {
    return {
        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = aData[0] + '<input type="hidden" name="question_id" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" name="question:ru" class="form-control small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" name="question:en" class="form-control small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" name="answer:ru" class="form-control small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" name="answer:en" class="form-control small" value="' + aData[4] + '">';
                jqTds[5].innerHTML = getCategorySelect(jqTds[5], nRow);
                jqTds[6].innerHTML = '<a class="edit btn-success btn btn-xs" href="" title="' + save +
                    '"><i class="fa fa-floppy-o" aria-hidden="true"></i></a><a class="cancel btn btn-danger btn-xs" href="" title="' +
                    cancel + '"><i class="fa fa-times" aria-hidden="true"></i></a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                oTable.fnUpdate('<a class="edit btn-info btn btn-xs" href="" title="' + edit +
                    '"><i class="fa fa-pencil"></i></a><a id="delete_' + jqInputs[0].value +
                    '" class="delete"></a><a class="btn-danger btn btn-xs" href="#confirm' + jqInputs[0].value +
                    '" title="' + del + '" data-toggle="modal"><i class="fa fa-trash-o "></i></a>', nRow, 6, false);
                oTable.fnDraw();
            }

            function getCategorySelect(td, nRow) {
                var categoryInput = $('input', nRow)[5], categoryId;

                if (categoryInput === undefined) {
                    categoryId = '1';
                } else {
                    categoryId = categoryInput.value;
                }

                var token = $('meta[name="csrf-token"]').attr('content');

                jQuery.ajax({
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': token},
                    url: '/' + lang + '/admin/faq/questions/categories/select',
                    data: { category_id: categoryId},
                    success: function(html){
                        td.innerHTML = html;
                    }
                });
            }

            function saveData(nRow, oTable, nEditing) {
                var inputs = $('input', nRow);
                var select = $('select', nRow);
                var route, method;
                var token = $('meta[name="csrf-token"]').attr('content');

                if (inputs[0].value === '') {
                    route = '/' + lang + '/admin/faq/questions';
                    method = 'POST';
                } else {
                    route = '/' + lang + '/admin/faq/questions/' + inputs[0].value;
                    method = 'PATCH';
                }

                jQuery.ajax({
                    type: method,
                    headers: {'X-CSRF-TOKEN': token},
                    url: route,
                    data: {
                        question_ru: inputs[1].value,
                        question_en: inputs[2].value,
                        answer_ru: inputs[3].value,
                        answer_en: inputs[4].value,
                        category: select.val(),
                        _token: token
                    },
                    success: function(result){
                        showToastr(result['level'], result['message']);

                        var jqTds = $('>td', nRow);
                        if (inputs[0].value === '' && result['level'] === 'success') {
                            jqTds[0].innerHTML = result['faq_id'] + '<input type="hidden" name="question_id" value="'
                                + result['faq_id'] + '">';
                            $('#new_row').append(result['confirmAlert']).removeAttr('id');
                        }
                        jqTds[5].innerHTML = result['category_name'] + '<input type="hidden" name="category_id" value="'
                            + result['category_id'] + '">';

                        if (result['level'] === 'error') {
                            restoreRow(oTable, nEditing);
                        } else {
                            saveRow(oTable, nEditing);
                        }

                        nEditing = null;
                    },
                    error: function(){
                        restoreRow(oTable, nEditing);
                        nEditing = null;
                    }
                });
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                oTable.fnUpdate(jqInputs[5].value, nRow, 5, false);
                oTable.fnUpdate('<a class="edit btn-info btn btn-xs" href="" title="'
                    + edit + '"><i class="fa fa-pencil"></i></a>', nRow, 6, false);
                oTable.fnDraw();
            }

            function deleteData(nRow, oTable) {
                var aData = oTable.fnGetData(nRow);
                var token = $('meta[name="csrf-token"]').attr('content');

                jQuery.ajax({
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': token},
                    url: '/' + lang + '/admin/faq/questions/' + aData[0],
                    success: function(message){
                        showToastr(message['level'], message['message']);

                        if (message['level'] === 'success') {
                            oTable.fnDeleteRow(nRow);
                        }
                    }
                });
            }

            var oTable = $('#editable-sample').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, 50] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": getLangData(),
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                }
                ]
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control medium"); // modify table per page dropdown

            var nEditing = null;

            $('#editable-sample_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '', '', '',
                    '<a class="edit btn-info btn btn-xs" href="" title="' + edit +
                    '"><i class="fa fa-pencil"></i></a><a class="cancel btn btn-danger btn-xs" data-mode="new" href="" title="'
                    + cancel + '"><i class="fa fa-times" aria-hidden="true"></i></a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                nRow.setAttribute('id', 'new_row');
                editRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#editable-sample a.delete').live('click', function (e) {
                var nRow = $(this).parents('tr')[0];
                deleteData(nRow, oTable);
            });

            $('#editable-sample a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#editable-sample a.edit').live('click', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.getAttribute('title') == save) {
                    /* Editing this row and want to save it */
                    saveData(nRow, oTable, nEditing);
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }
    };
}();

$( window ).load(function() {
    EditableTable.init();
});
