var telInput = $("#phone"),
    successMsg = $("#success-msg"),
    errorMsg = $("#error-msg"),
    isValidPhoneInput = $('#isValidPhone');

var reset = function() {
    telInput.removeClass("error");
    isValidPhoneInput.val(0);
    errorMsg.addClass("hide");
    successMsg.addClass("hide");
};

if (telInput.val() !== '' && telInput.intlTelInput("isValidNumber")) {
    isValidPhoneInput.val(1);
}

// initialise plugin
telInput.intlTelInput(
    {
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "ru";
                callback(countryCode);
            });
        },
        autoPlaceholder: "aggressive",
        utilsScript: "/js/utils.js"
    }
);

// on blur: validate
telInput.blur(function() {
    reset();
    if (telInput.val().trim()) {
        if (telInput.intlTelInput("isValidNumber")) {
            successMsg.removeClass("hide");
            isValidPhoneInput.val(1);
        } else {
            telInput.addClass("error");
            errorMsg.removeClass("hide");
        }
    }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);