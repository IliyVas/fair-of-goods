var categoryBlock = $('.category_block');

categoryBlock.on('click', function() {
    var categoryId = this.getAttribute('data-category');
    categoryBlock.removeClass('active');
    $('#category_' + categoryId).addClass('active');
    $('.subcategory_block').addClass('hidden');
    $('#open_' + categoryId).removeClass('hidden');
});
