function deleteSelected(elem) {
    if (elem.hasAttribute('data-del')) {
        $('#item_' + elem.getAttribute('data-del')).attr('checked', 'checked');
    }

    document.getElementById('multi-select-form').submit();
}