function showConstitutionsAndSizeInputs() {
    var ajaxContent = $('#constitution_and_size');
    jQuery.ajax({
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/' + $('html').attr('lang') + '/product/form/category-dependent-block',
        data: {
            _token: $('input[name="_token"]').val(),
            category: $('#category').val(),
            constitution: $('#constitution_old_value').val(),
            child_age: $('#child_age_old_value').val(),
            size: $('#size_old_value').val(),
            product: $('#product_id').val()
        },
        success: function(html){
            ajaxContent.html(html);
        },
        error: function () {
            console.log('Error! Category not found. Try again.')
        }
    });
}

function showSecondStep() {
    var ajaxContent = $('#second_step');
    //get route
    var route = '/product/form/institution';
    if ($('#forSale').val() === "1") {
        route = '/product/form/second-step';
    }

    jQuery.ajax({
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: '/' + $('html').attr('lang') + route,
        data: {
            _token: $('input[name="_token"]').val(),
            donateAfterFair: $('#donate_after_fair_old_value').val(),
            institution: $('#institution_old_value').val(),
            price: $('#price_old_value').val(),
            product: $('#product_id').val()
        },
        success: function(html){
            ajaxContent.html(html);
        }
    });
}

function activateOrganisation() {
    var donate = $('#donate_after_fair').val(), institution = $('#institution');

    if(donate === "1") {
        institution.removeAttr('disabled')
    } else {
        institution.val('');
        institution.attr('disabled', 'disabled');
    }
}

//preview
function readImgURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#product_img').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$( window ).load(function() {
    $("#image").change(function(){
        $('#product_img').show();
        readImgURL(this);
    });
});