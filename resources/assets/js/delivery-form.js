//multiselect start
var select = $('#delivery_multi_select'), select2 = $('#delivery_multi_select2'), options;

options = {
    selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='" +
    select.attr('data-search') + "...'>",
    selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='" +
    select.attr('data-search') + "...'>",
    afterInit: function (ms) {
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
            .on('keydown', function (e) {
                if (e.which === 40) {
                    that.$selectableUl.focus();
                    return false;
                }
            });

        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
            .on('keydown', function (e) {
                if (e.which == 40) {
                    that.$selectionUl.focus();
                    return false;
                }
            });
    },
    afterSelect: function () {
        this.qs1.cache();
        this.qs2.cache();
    },
    afterDeselect: function () {
        this.qs1.cache();
        this.qs2.cache();
    }
};

select.multiSelect(options);
select2.multiSelect(options);

// Map block
ymaps.ready(function () {
    var osAddress = $('#os-address'),
        fsAddress = $('#fs-address'),
        osMap = new ymaps.Map("os-map", {
            center: [55.76, 37.64],
            zoom: 10
        }),
        fsMap = new ymaps.Map("fs-map", {
            center: [55.76, 37.64],
            zoom: 10
        });

    if (osAddress.val().trim() !== '') {
        getAddress(osAddress, osMap);
    } else if (fsAddress.val().trim() !== '') {
        getAddress(fsAddress, fsMap);
    }

    osMap.events.add('click', function (e) {
        var coords = e.get('coords');
        getAddressFromCoords(coords, osMap, osAddress);
    });

    fsMap.events.add('click', function (e) {
        var coords = e.get('coords');
        getAddressFromCoords(coords, fsMap, fsAddress);
    });

    function getAddressFromCoords(coords, map, addressInput) {
        ymaps.geocode(coords).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0);

            //add placemarck
            firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
            firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());
            map.geoObjects.add(firstGeoObject);
            //set address
            addressInput.val(firstGeoObject.getAddressLine());
        });
    }

    function getAddress(addressInput, map) {
        ymaps.geocode(addressInput.val(), {results: 1}).then(function (res) {
            var firstGeoObject = res.geoObjects.get(0),
                bounds = firstGeoObject.properties.get('boundedBy');

            //add placemarck
            firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
            firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());
            map.geoObjects.add(firstGeoObject);
            map.setBounds(bounds, {
                checkZoomRange: true
            });

            //set address
            addressInput.val(firstGeoObject.getAddressLine());
        });
    }
});

//address block
$( window ).load(function() {
    var osCourier = $('#os-courier'),
        fsCourier = $('#fs-courier'),
        osAddress = $('#os-address'),
        fsAddress = $('#fs-address'),
        fsApartment = $('#fs-apartment'),
        osApartment = $('#os-apartment'),
        osAddressContainer = $('#os_address_container'),
        fsAddressContainer = $('#fs_address_container');

    getAddressBlock(osCourier, osAddressContainer, osAddress, osApartment);
    getAddressBlock(fsCourier, fsAddressContainer, fsAddress, fsApartment);

    osCourier.on("click", function () {
        getAddressBlock(osCourier, osAddressContainer, osAddress, osApartment);
    });
    fsCourier.on("click", function () {
        getAddressBlock(fsCourier, fsAddressContainer, fsAddress, fsApartment);
    });
});

function showAddressBlock(container) {
    container.removeClass('hidden');
}

function hideAddressBlock(container, address, apartment) {
    address.val('');
    apartment.val('');
    container.addClass('hidden');
}

function getAddressBlock(checkbox, container, address, apartment) {
    if (checkbox.is(':checked')) {
        hideAddressBlock(container, address, apartment);
    } else {
        showAddressBlock(container);
    }
}