var lang = $('html').attr('lang');

var EditableTable = function () {
    return {
        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[0].innerHTML = aData[0] + '<input type="hidden" name="category_id" value="' + aData[0] + '">';
                jqTds[1].innerHTML = '<input type="text" name="name:ru" class="form-control small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input type="text" name="name:en" class="form-control small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<a class="edit btn-success btn btn-xs" href="" title="' + save +
                    '"><i class="fa fa-floppy-o" aria-hidden="true"></i></a><a class="cancel btn btn-danger btn-xs" href="" title="' +
                    cancel + '"><i class="fa fa-times" aria-hidden="true"></i></a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate('<a class="edit btn-info btn btn-xs" href="" title="' + edit +
                    '"><i class="fa fa-pencil"></i></a><a id="delete_' + jqInputs[0].value +
                    '" class="delete"></a><a class="btn-danger btn btn-xs" href="#confirm' + jqInputs[0].value +
                    '" title="' + del + '" data-toggle="modal"><i class="fa fa-trash-o "></i></a>', nRow, 3, false);
                oTable.fnDraw();
            }

            function saveData(nRow, oTable, nEditing) {
                var inputs = $('input', nRow);
                var route, method;
                var token = $('meta[name="csrf-token"]').attr('content');

                if (inputs[0].value === '') {
                    route = '/' + lang + '/admin/faq/questions/categories';
                    method = 'POST';
                } else {
                    route = '/' + lang + '/admin/faq/questions/categories/' + inputs[0].value;
                    method = 'PATCH';
                }

                jQuery.ajax({
                    type: method,
                    headers: {'X-CSRF-TOKEN': token},
                    url: route,
                    data: {
                        name_ru: inputs[1].value,
                        name_en: inputs[2].value,
                        _token: token
                    },
                    success: function(message){
                        showToastr(message['level'], message['message']);

                        if (inputs[0].value === '' && message['level'] === 'success') {
                            var jqTds = $('>td', nRow);
                            jqTds[0].innerHTML = message['id'] + '<input type="hidden" name="category_id" value="'
                                + message['id'] + '">';
                            $('#new_row').append(message['confirmAlert']).removeAttr('id');
                        }

                        if (message['level'] === 'error') {
                            restoreRow(oTable, nEditing);
                        } else {
                            saveRow(oTable, nEditing);
                        }

                        nEditing = null;
                    },
                    error: function(){
                        restoreRow(oTable, nEditing);
                        nEditing = null;
                    }
                });
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate('<a class="edit btn-info btn btn-xs" href="" title="'
                    + edit + '"><i class="fa fa-pencil"></i></a>', nRow, 3, false);
                oTable.fnDraw();
            }

            function deleteData(nRow, oTable) {
                var aData = oTable.fnGetData(nRow);
                var token = $('meta[name="csrf-token"]').attr('content');

                jQuery.ajax({
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': token},
                    url: '/' + lang + '/admin/faq/questions/categories/' + aData[0],
                    success: function(message){
                        showToastr(message['level'], message['message']);

                        if (message['level'] === 'success') {
                            oTable.fnDeleteRow(nRow);
                        }
                    }
                });
            }

            var oTable = $('#editable-sample').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, 50] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,
                "sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
                "sPaginationType": "bootstrap",
                "oLanguage": getLangData(),
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0]
                }
                ]
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control medium"); // modify table per page dropdown

            var nEditing = null;

            $('#editable-sample_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '',
                    '<a class="edit btn-info btn btn-xs" href="" title="' + edit +
                    '"><i class="fa fa-pencil"></i></a><a class="cancel btn btn-danger btn-xs" data-mode="new" href="" title="'
                    + cancel + '"><i class="fa fa-times" aria-hidden="true"></i></a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                nRow.setAttribute('id', 'new_row');
                editRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#editable-sample a.delete').live('click', function (e) {
                var nRow = $(this).parents('tr')[0];
                deleteData(nRow, oTable);
            });

            $('#editable-sample a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#editable-sample a.edit').live('click', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.getAttribute('title') == save) {
                    /* Editing this row and want to save it */
                    saveData(nRow, oTable, nEditing);
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }
    };
}();

$( window ).load(function() {
    EditableTable.init();
});