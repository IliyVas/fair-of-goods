function fnFormatDetails ( oTable, nTr, descCol )
{
    var aData = oTable.fnGetData( nTr );
    var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
    sOut += '<tr><td>'+aData[descCol]+'</td></tr>';
    sOut += '</table>';

    return sOut;
}

function showHiddenInfo(table, selector, infoColumn) {
    $(document).on('click', selector + ' tbody td img',function () {
        var tr = $(this).parents('tr')[0];
        if (table.fnIsOpen(tr)) {
            /* This row is already open - close it */
            this.src = "/img/details_open.png";
            table.fnClose( tr );
        } else {
            /* Open this row */
            this.src = "/img/details_close.png";
            table.fnOpen( tr, fnFormatDetails(table, tr, infoColumn), 'details' );
        }
    } );
}

function setFormControlClass(selector) {
    $(selector + '_filter input').addClass("form-control"); // modify table search input
    $(selector + '#_length select').addClass("form-control"); // modify table per page dropdown
}

$(document).ready(function() {
    /*
     * delivery table
     */
    $('#delivery-table').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0, 1, 2, 5, 6 ] }
        ],
        "aaSorting": [[3, 'asc']],
        "oLanguage": getLangData()
    });
    setFormControlClass('#delivery-table');

    /*
     * items table
     */
    var itemsTable = $('#hidden-table-info').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0, 6, 7, 8 ] }
        ],
        "aaSorting": [[1, 'asc']],
        "oLanguage": getLangData()
    });
    setFormControlClass('#hidden-table-info');
    showHiddenInfo(itemsTable, '#hidden-table-info', 9);

    /*
     * fairs table
     */
    var fairTable = $('#fair-table').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 3, 4 ] }
        ],
        "aaSorting": [[0, 'asc']],
        "oLanguage": getLangData()
    });
    setFormControlClass('#fair-table');
    showHiddenInfo(fairTable, '#fair-table', 5);

    /*
     * users table
     */
   $('#users-table').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 7 ] }
        ],
        "aaSorting": [[0, 'asc']],
        "oLanguage": getLangData()
    });
    setFormControlClass('#users-table');

    /*
     * admin items table
     */
    var adminItemsTable = $('#items-table').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0, 7, 8, 9 ] }
        ],
        "aaSorting": [[1, 'asc']],
        "oLanguage": getLangData()
    });
    setFormControlClass('#items-table');
    showHiddenInfo(adminItemsTable, '#items-table', 10);

    /*
     * admin deliveries table
     */
    $('#deliveries-table').dataTable( {
        "aoColumnDefs": [
            { "bSortable": false, "aTargets": [ 0, 1, 2, 6, 7 ] }
        ],
        "aaSorting": [[3, 'asc']],
        "oLanguage": getLangData()
    });
    setFormControlClass('#deliveries-table');
} );