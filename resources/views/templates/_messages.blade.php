{{-- errors --}}
@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <script type="text/javascript">
            showToastr('error', '{{ $error }}');
        </script>
    @endforeach
@endif

{{-- messages --}}
@if (session()->has('flash_notification'))
    @foreach (session('flash_notification', collect())->toArray() as $message)
        <script type="text/javascript">
            showToastr('{{ $message['level'] }}', '{!! $message['message'] !!}');
        </script>
    @endforeach
    {{ session()->forget('flash_notification') }}
@endif