<section class="panel">
    <header class="panel-heading">
        {{ trans('basic.deliveriesList') }}
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div class="clearfix">
                {{--To issue delivery btn--}}
                @if (isset($showIssueExportBtn) && isset($issueExportRoute))
                    <div class="btn-group">
                        <a class="default-link" href="{{ $issueExportRoute }}">
                            <button href="" class="btn green">
                                {{ trans('basic.toIssueExport') }} <i class="fa fa-plus"></i>
                            </button>
                        </a>
                    </div>
                @endif
                {{--Delete selected deliveries--}}
                <div class="btn-group pull-right">
                    <button class="btn dropdown-toggle" data-toggle="dropdown">
                        {{ trans('basic.additionally') . ' ' }}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a  data-toggle="modal" href="#confirmDeleteSelected">
                                {{ trans('basic.deleteSelectedDeliveries') }}
                            </a>
                        </li>
                    </ul>
                    @php $multiDelMsg = trans('messages.deleteSelectedDeliveriesConfirm'); @endphp
                    @include('templates._confirm_multiple_deletions')
                </div>
            </div>
            <div class="space15"></div>

            <div class="table-responsive">
                <form id="multi-select-form" method="POST" action="{{ $deleteRoute }}">
                    {{ csrf_field() }}
                    @php $deleteSelected = true; @endphp
                    <table class="table-striped table-hover display table table-bordered"
                           @if (!isset($admin)) id="delivery-table" @else id="deliveries-table" @endif>
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{ trans('basic.dateAndTime') }}</th>
                            <th>{{ trans('basic.address') }}</th>
                            @if (isset($admin))
                                <th>{{ trans('basic.owner') }}</th>
                            @endif
                            <th>{{ trans('basic.type') }}</th>
                            <th>{{ trans('basic.status') }}</th>
                            <th>{{ trans('basic.itemsList') }}</th>
                            <th>{{ trans('basic.actions') }}</th>
                        </tr>
                        </thead>

                        @if (count($deliveries) > 0)
                            <tbody>
                            @foreach($deliveries as $model)
                                <tr>
                                    <td>
                                        <input @if(!$model->isDeletable()) disabled @endif
                                        name="selected_items[]" value="{{ $model->id }}"
                                               type="checkbox" id="item_{{ $model->id }}">
                                    </td>
                                    <td>
                                        <ul>
                                            <li>
                                                <span class="bold">
                                                    {{ trans('basic.deliveryDay' . $model->on_stock) }}:
                                                </span> {{ date_create($model->delivery_date)->format('d.m.Y') }}
                                            </li>
                                            <li>
                                                <span class="bold">
                                                    {{ trans('basic.deliveryTime' . $model->on_stock) }}:
                                                </span> {{ $model->delivery_time }}
                                            </li>
                                        </ul>
                                    </td>
                                    <td>
                                        @if (!is_null($model->address))
                                            {{ $model->address }}
                                        @endif
                                    </td>
                                    @if (isset($admin))
                                        <td>
                                            <a href="{{ route('users.show', ['id' => $model->user_id]) }}" title="{{ trans('basic.view') }}">
                                                {{ $model->user->first_name . ' ' . $model->user->last_name }}
                                            </a>
                                        </td>
                                    @endif
                                    <td>
                                        @if ($model->on_stock)
                                            {{ trans('basic.exportType') }}
                                        @else
                                            {{ trans('basic.returnType') }}
                                        @endif
                                    </td>
                                    <td>
                                        @if ($model->confirmed)
                                            {{ trans('basic.confirmed') }}
                                        @else
                                            {{ trans('basic.notConfirmed') }}
                                        @endif
                                    </td>
                                    <td>
                                        <ol>
                                            @foreach($model->products as $product)
                                                <li>
                                                    @php $showRouteVars = ['product' => $product->id]; @endphp
                                                    @if ($showProductRouteName === 'users.products.show')
                                                        @php $showRouteVars = [
                                                        'user' => $product->user_id,
                                                        'product' => $product->id
                                                        ]; @endphp
                                                    @endif
                                                    <a  href="{{ route($showProductRouteName, $showRouteVars) }}"
                                                        title="{{ trans('basic.view') }}" >
                                                        {{ $product->name }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ol>
                                    </td>
                                    <td class="action-td-lg">
                                        {{--Confirm btn--}}
                                        @if ($user->isAdmin() && !$model->confirmed)
                                            <a id="confirm_delivery_{{ $model->id }}" href="{{ route('users.deliveries.confirm', [
                                                   'userId' => $model->user_id, 'deliveryId' => $model->id]) }}"
                                               class="btn btn-success btn-xs" title="{{ trans('basic.confirm') }}">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </a>
                                        @endif
                                        {{--Edit btn--}}
                                        @if ($user->isAdmin() && !isset($admin))
                                            @php $routeVars = [$model->user_id, $model->id]; @endphp
                                        @else
                                            @php $routeVars = [$model->id]; @endphp
                                        @endif
                                        @if($model->isEditable())
                                            <a href="{{ route($routeName, $routeVars) }}" class="btn btn-primary btn-xs"
                                               title="{{ trans('basic.edit') }}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        @endif
                                        {{--Deletion block--}}
                                        @if($model->isDeletable())
                                            <a href="#confirm{{ $model->id }}" class="btn btn-danger btn-xs"
                                               title="{{ trans('basic.delete')}}" data-toggle="modal">
                                                <i class="fa fa-trash-o "></i>
                                            </a>
                                            @php $delMsg = trans('messages.deleteDeliveryConfirm',['id' => htmlspecialchars($model->id)]); @endphp
                                            @include('templates._confirm_single_deletion')
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        @endif
                    </table>
                </form>
            </div>
        </div>
    </div>
</section>