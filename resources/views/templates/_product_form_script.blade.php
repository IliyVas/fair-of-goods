<script>
    function sendMultiSelectForm(route) {
        event.preventDefault();
        var form = document.getElementById('multi-select-form');
        form.setAttribute('action', route);
        form.submit();
    }
</script>