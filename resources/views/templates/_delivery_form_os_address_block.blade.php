<div class="col-xs-5 mtop20">
    <div class="form-group-last mtop20">
        <label>{{ trans('basic.address') }}</label>
        <input id="os-address" name="address" type="text" class="form-control"
               placeholder="{{ trans('basic.address') }}"
               @if ($onStockForm) value="{{ $address }}" @endif>
    </div>

    <div class="form-group-last mtop20">
        <label>{{ trans('basic.apartment') }}</label>
        <input id="os-apartment" name="apartment" type="number" class="form-control"
               placeholder="{{ trans('basic.apartment') }}"
               @if ($onStockForm) value="{{ $apartment }}" @endif>
    </div>
</div>

{{--Map--}}
<div class="col-xs-7 mtop20">
    <p class="help-block">*{{ trans('basic.mapInfo') }}</p>
    <div id="os-map" class="map-container"></div>
</div>