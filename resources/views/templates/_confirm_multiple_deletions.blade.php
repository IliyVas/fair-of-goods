<div class="modal fade" id="confirmDeleteSelected" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ trans('basic.confirmDelete') }}</h4>
            </div>
            <div class="modal-body">
                {{ $multiDelMsg  }}
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">{{ trans('basic.no') }}</button>
                <button onclick="deleteSelected(this)" class="btn btn-warning" type="button">
                    {{ trans('basic.yes') }}
                </button>
            </div>
        </div>
    </div>
</div>