<script>
    var edit = '{{ trans("basic.edit") }}',
        save = '{{ trans("basic.save") }}',
        del = '{{ trans("basic.delete") }}',
        cancel = '{{ trans("basic.cancel") }}';

    function getLangData() {
        return  {
            "oPaginate": {
                "sPrevious": '{{ trans("pagination.prevBtn") }}',
                "sNext": '{{ trans("pagination.nextBtn") }}'
            },
            "sEmptyTable": '{{ trans("basic.emptyTable") }}',
            "sLoadingRecords": '{{ trans("basic.loadingRecords") }}',
            "sSearch": '{{ trans("basic.search") }}',
            "sLengthMenu": '{{ trans("basic.perPage", ["item" => $item]) }}',
            "sInfoEmpty": '{{ trans("basic.infoEmpty", ["item" => $item]) }}',
            "sInfo": '{{ trans("basic.tableInfo", ["item" => $item]) }}'
        };
    }
</script>