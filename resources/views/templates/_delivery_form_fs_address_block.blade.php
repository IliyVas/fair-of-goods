<div class="col-xs-5 mtop20">
    {{--Adress--}}
    <div class="form-group-last mtop20">
        <label>{{ trans('basic.address') }}</label>
        <input id="fs-address" name="address" type="text" class="form-control"
               placeholder="{{ trans('basic.address') }}"
               @if (!$onStockForm) value="{{ $address }}" @endif>
    </div>

    {{--Apartment--}}
    <div class="form-group-last mtop20">
        <label>{{ trans('basic.apartment') }}</label>
        <input id="fs-apartment" name="apartment" type="number" class="form-control"
               placeholder="{{ trans('basic.apartment') }}"
               @if (!$onStockForm) value="{{ $apartment }}" @endif>
    </div>
</div>

{{--Map--}}
<div class="col-xs-7 mtop20">
    <p class="help-block">*{{ trans('basic.mapInfo') }}</p>
    <div id="fs-map" class="map-container"></div>
</div>