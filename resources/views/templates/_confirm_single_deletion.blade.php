<div class="modal fade" id="confirm{{ $model->id }}" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{ trans('basic.confirmDelete') }}</h4>
            </div>
            <div class="modal-body">{{ $delMsg }}</div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">{{ trans('basic.no') }}</button>
                @if (isset($editableTbl))
                    <button data-dismiss="modal"  class="btn btn-warning"
                            onclick="$('#delete_{{ $model->id }}').trigger('click');">
                        {{ trans('basic.yes') }}
                    </button>
                @elseif (!isset($deleteSelected))
                    <button onclick="document.getElementById('delete_{{ $model->id }}').submit();"
                            class="btn btn-warning" type="button">
                        {{ trans('basic.yes') }}
                    </button>
                @else
                    <button onclick="deleteSelected(this)" data-del="{{ $model->id }}" class="btn btn-warning" type="button">
                        {{ trans('basic.yes') }}
                    </button>
                @endif
            </div>
        </div>
    </div>
</div>