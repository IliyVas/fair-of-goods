@if (!is_null($fair))
    <section class="panel">
        <div class="fair-description-heading">
            @if (!is_null($cFair))
                {{ trans('basic.currentFair') }}
            @else
                {{ trans('basic.nearestFair') }}
            @endif
        </div>
        <div class="panel-body">
            <h3>{{ $fair->name }}</h3>
            <p>
                <span class="bold">{{ trans('basic.datesOfThe') }}: </span>
                {{ date_create($fair->start_date)->format('d.m.Y') .
                 ' - ' . date_create($fair->finish_date)->format('d.m.Y') }}
            </p>
            <p>
                <span class="bold">{{ trans('basic.address') }}:</span>
                {{ $fair->address }}
            </p>
            <div id="map" class="map-container"></div>
        </div>
    </section>
@endif