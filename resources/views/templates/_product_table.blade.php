<section class="panel">
    <header class="panel-heading">
        {{ trans('basic.itemsList') }}
    </header>
    <div class="panel-body">
        <div class="adv-table">
            <div class="clearfix">
                {{--Add item btn--}}
                @if (isset($showCreateItemBtn) && $showCreateItemBtn)
                    <div class="btn-group">
                        <a class="default-link" href="{{ $createItemRoute }}">
                            <button href="" class="btn btn-success">
                                {{ trans('basic.addItem') }} <i class="fa fa-plus"></i>
                            </button>
                        </a>
                    </div>
                @endif
                @if ($admin && isset($downloadItemsRoute))
                    <div class="btn-group">
                        <a @if(!count($products) > 0) disabled @endif class="default-link" href="{{ $downloadItemsRoute }}">
                            <button @if(!count($products) > 0) disabled @endif href="" class="btn btn-warning">
                                {{ trans('basic.downloadProducts') }} <i class="fa fa-download"></i>
                            </button>
                        </a>
                    </div>
                @endif
                @if (isset($downloadPriceListRoute) && !is_null($downloadPriceListRoute))
                    <div class="btn-group">
                        <a @if(!count($products) > 0) disabled @endif class="default-link" href="{{ $downloadPriceListRoute }}">
                            <button @if(!count($products) > 0) disabled @endif href="" class="btn btn-info">
                                {{ trans('basic.downloadPriceList') }} <i class="fa fa-download"></i>
                            </button>
                        </a>
                    </div>
                @endif
                <div class="btn-group pull-right">
                    <button class="btn dropdown-toggle" data-toggle="dropdown">
                        {{ trans('basic.additionally') . ' ' }}
                        <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right dropdown__width">
                        @if (isset($showIssueExportBtn) && $showIssueExportBtn)
                            <li>
                                <a href="#" onclick="sendMultiSelectForm('{{ $issueExportRoute }}');">
                                    {{ trans('basic.toIssueExportOfItems') }}
                                </a>
                            </li>
                        @endif
                        @if($admin)
                            <li>
                                <a href="#" onclick="sendMultiSelectForm('{{route('admin.products.confirm') }}');">
                                    {{ trans('basic.confirmSelectedProducts') }}
                                </a>
                            </li>
                        @endif
                        <li>
                            <a data-toggle="modal" href="#confirmDeleteSelected">
                                {{ trans('basic.deleteSelectedProducts') }}
                            </a>
                        </li>
                    </ul>
                    @php $multiDelMsg = trans('messages.deleteSelectedProductsConfirm'); @endphp
                    @include('templates._confirm_multiple_deletions')
                </div>
            </div>
            <div class="space15"></div>

            <div class="table-responsive" id="addedScrollTable">
                <form id="multi-select-form" method="POST" action="{{ $deleteRoute }}">
                    {{ csrf_field() }}
                    @php $deleteSelected = true; @endphp
                    <table class="table-striped table-hover display table table-bordered"
                           @if(!$admin) id="hidden-table-info" @else id="items-table" @endif>
                        <thead>
                        <tr>
                            <th></th>
                            <th>{{ trans('basic.name') }}</th>
                            @if ($admin)
                                <th>{{ trans('basic.owner') }}</th>
                            @endif
                            <th>{{ trans('basic.category') }}</th>
                            <th>{{ trans('basic.status') }}</th>
                            <th>{{ trans('basic.shortPrice') }}</th>
                            <th>
                                @if (Auth::user()->isUser())
                                    {{ trans('basic.revenuePercent') }}*
                                @else
                                    {{ trans('basic.revenuePercentAdm') }}*
                                @endif
                            </th>
                            <th id="desc_btn">{{ trans('basic.btnShowDescription') }}</th>
                            <th>{{ trans('basic.image') }}</th>
                            <th>{{ trans('basic.actions') }}</th>
                            <th class="hidden"></th>
                        </tr>
                        </thead>

                        @if (count($products) > 0)
                            <tbody>
                            @foreach($products as $model)
                                @if ($admin && !isset($simple))
                                    @php $routeVars = [$model->user_id, $model->id]; @endphp
                                @else
                                    @php $routeVars = [$model->id]; @endphp
                                @endif
                                <tr>
                                    <td>
                                        @if ($model->status !== ProductStatus::REGISTERED &&
                                         $model->status !== ProductStatus::ON_STOCK)
                                            @php $disabled = 'disabled'; @endphp
                                        @else  @php $disabled = ''; @endphp @endif
                                        <input {{ $disabled }} name="selected_items[]" value="{{ $model->id }}" type="checkbox"
                                               id="item_{{ $model->id }}">
                                    </td>
                                    <td>
                                        <a href="{{ route($showRoute, $routeVars) }}" title="{{ trans('basic.view') }}">
                                            {{ $model->name }}
                                        </a>
                                    </td>
                                    @if($admin)
                                        <td>
                                            <a href="{{ route('users.show', ['id' => $model->user_id]) }}" title="{{ trans('basic.view') }}">
                                                {{ $model->user->first_name . ' ' . $model->user->last_name}}
                                            </a>
                                        </td>
                                    @endif
                                    <td>{{ $model->category->name }}</td>
                                    <td>
                                        {{ trans('basic.' . $model->status . 'ProductStatus') }}
                                    </td>
                                    <td>{{ $model->price . ' ' . trans('basic.rub') }}</td>
                                    <td>{{ $model->getUserRevenue() . ' ' . trans('basic.rub') }}</td>
                                    <td class="desc_btn center">
                                        <img class="img-btn" src="{{ asset('img/details_open.png') }}">
                                    </td>
                                    <td align="center">
                                        <img src="{{ $model->getImageUrl('thumb') }}">
                                    </td>
                                    <td class="action-td-lg">
                                        {{--View btn block--}}
                                        <a href="{{ route($showRoute, $routeVars) }}"
                                           class="btn btn-warning btn-xs m-bot15" title="{{ trans('basic.view') }}">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                        @if ($model->status === ProductStatus::REGISTERED)
                                            {{--Edit btn block--}}
                                            <a href="{{ route($editRoute, $routeVars) }}" class="btn  m-bot15 btn-primary btn-xs"
                                               title="{{ trans('basic.edit', ['name' => htmlspecialchars($model->name)]) }}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            {{--Duplicate btn block--}}
                                            <a href="{{ route($duplicateRoute, ['user' => $model->user_id, 'product' => $model->id]) }}"
                                               class="btn  m-bot15 btn-success btn-xs"
                                               title="{{ trans('basic.duplicate', ['name' => htmlspecialchars($model->name)]) }}">
                                                <i class="fa fa-copy"></i>
                                            </a>
                                            {{--Deletion block--}}
                                            <a href="#confirm{{ $model->id }}" class="btn btn-danger btn-xs m-bot15"
                                               title="{{ trans('basic.delete')}}" data-toggle="modal">
                                                <i class="fa fa-trash-o "></i>
                                            </a>
                                            @php $delMsg = trans('messages.deleteProductConfirm',['name' => htmlspecialchars($model->name)]); @endphp
                                            @include('templates._confirm_single_deletion')
                                        @endif

                                        {{--Confirm btn--}}
                                        @php $confirm = $admin && $model->status === ProductStatus::ON_STOCK; @endphp
                                        @if ($confirm && !$model->for_sale ||$confirm && $model->donate_after_fair)
                                            <a href="{{ route('admin.products.confirm', [
                                            'selected_items' => [$model->id]]) }}"
                                               class="btn btn-success btn-xs m-bot15"
                                               title="{{ trans('basic.confirmProduct') }}">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td class="hidden"> {{ $model->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            </tfoot>
                        @endif
                    </table>
                </form>
            </div>
            *{{ trans('basic.revenueDesc') }}
        </div>
    </div>
</section>