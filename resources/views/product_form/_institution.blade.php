{{--Institutions block--}}
@if (isset($institutions) && count($institutions) > 0)
    <div class="form-group">
        {!! Form::label('institution_id', trans('basic.institution'), ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            <select @if($forSale)disabled="disabled" @endif id="institution" name="institution_id" class="form-control m-bot15">
                <option value=""></option>
                @if (isset($product) && is_null($institutionIdOldValue))
                    @php $selectedInstitution = $product->institution_id; @endphp
                @else
                    @php $selectedInstitution = $institutionIdOldValue; @endphp
                @endif
                @foreach($institutions as $institution)
                    <option @if($selectedInstitution == $institution->id) selected @endif
                    value="{{ $institution->id }}">
                        {{ $institution->name }}</option>
                @endforeach
            </select>
            <p class="help-block">{{ trans('basic.institutionInputDesc') }}</p>
        </div>
    </div>
@endif