{{--Constitution block--}}
@if (isset($constitution) && !is_null($constitution) && count($constitution->featureValues) > 0)
    <div class="form-group">
        {!! Form::label('constitution', trans('basic.constitution'), ['class' => 'control-label col-lg-2']) !!}
        <div class="col-lg-10">
            <select name="constitution" class="form-control m-bot15">
                <option value=""></option>
                @if (is_null($constitutionOldValue) && isset($product))
                    @php $selectedConstitution = $product->getFeatureValue(Feature::CONSTITUTION, 'id'); @endphp
                @else
                    @php $selectedConstitution = $constitutionOldValue; @endphp
                @endif
                @foreach($constitution->featureValues as $constitutionValue)
                    <option @if($selectedConstitution == $constitutionValue->id) selected @endif
                    value="{{ $constitutionValue->id }}">
                        {{ $constitutionValue->content }}</option>
                @endforeach
            </select>
            <p class="help-block">{{ trans('basic.constitutionInputDesc') }}</p>
        </div>
    </div>
@endif

{{--Size block--}}
@if (isset($size) && !is_null($size) && count($size->featureValues) > 0)
    <div class="form-group">
         <span class="col-lg-2">
             {!! Form::label('size', trans('basic.size'), ['class' => 'control-label']) !!}
             <span class="text-danger">*</span>
        </span>
        <div class="col-lg-10">
            <select name="size" class="form-control m-bot15">
                @if (is_null($sizeOldValue) && isset($product))
                    @php $selectedSize = $product->getSizeFeatureValue(); @endphp
                @else @php $selectedSize = $sizeOldValue; @endphp @endif
                @foreach($size->featureValues as $sizeValue)
                    <option @if($selectedSize == $sizeValue->id) selected @endif
                    value="{{ $sizeValue->id }}">
                        {{ $sizeValue->content }}</option>
                @endforeach
            </select>
            <p class="help-block">{{ trans('basic.sizeInputDesc') }}</p>
        </div>
    </div>
@endif

{{--Child age block--}}
@if (isset($childAge) && !is_null($childAge) && count($childAge->featureValues) > 0)
    <div class="form-group">
        {!! Form::label('child_age', trans('basic.childAge'), ['class' => 'control-label col-lg-2']) !!}
        <div class="col-lg-10">
            <select name="child_age" class="form-control m-bot15">
                @if (is_null($childAgeOldValue) && isset($product))
                    @php $selectedChildAge = $product->getFeatureValue(Feature::CHILD_AGE, 'id'); @endphp
                @else
                    @php $selectedChildAge = $childAgeOldValue; @endphp
                @endif
                    <option value=""></option>
                @foreach($childAge->featureValues as $childAgeValue)
                    <option @if($selectedChildAge == $childAgeValue->id) selected @endif
                    value="{{ $childAgeValue->id }}">
                        {{ $childAgeValue->content }}</option>
                @endforeach
            </select>
            <p class="help-block">{{ trans('basic.childAgeInputDesc') }}</p>
        </div>
    </div>
@endif
