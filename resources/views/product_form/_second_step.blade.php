{{--Price block--}}
<div class="form-group">
    <span class="col-lg-2">
        {!! Form::label('price', trans('basic.price'), ['class' => 'control-label']) !!}
        <span class="text-danger">*</span>
    </span>
    <div class="col-lg-10">
        @if (isset($product) && is_null($priceOldValue)) @php $price = $product->price; @endphp
        @else @php $price = $priceOldValue; @endphp @endif
        <input type="number" min="0" value="{{ $price }}" class="form-control"
               placeholder="{{ trans('basic.price') }}" name="price">
        <p class="help-block">{{ trans('basic.priceInputDesc') }}</p>
    </div>
</div>

{{--Discount block--}}
@if(isset($discountTypes) && count($discountTypes) > 0)
    @foreach($discountTypes as $discountType)
        {{--Check that type has discounts--}}
        @if (count($discountType->discounts) > 0)
            <div class="form-group">
                {{--Select type translation--}}
                @if (is_null($discountType->finish_day))
                    @php $trans = trans('basic.discountDescEnd', ['start_day' => htmlspecialchars($discountType->start_day)]); @endphp
                @else
                    @php
                        $trans = trans('basic.discountDesc', [
                        'start_day' => htmlspecialchars($discountType->start_day),
                        'finish_day' => htmlspecialchars($discountType->finish_day)]);
                    @endphp
                @endif
                {{--Discount label--}}
                {!! Form::label('discount[' . $discountType->id . ']', $trans, ['class' => 'col-lg-2 control-label']) !!}

                {{--Discounts values--}}
                <div class="col-lg-10">
                    {{--Get selected discounts--}}
                    @if (isset($product) && count($product->discounts) > 0)
                        @php $selectedDiscounts = $product->discounts->pluck('id')->all(); @endphp
                    @else @php $selectedDiscounts = []; @endphp @endif

                    @foreach($discountType->discounts as $discount)
                        <label class="radio-inline-product-form">
                            <input @if (in_array($discount->id, $selectedDiscounts)) checked @endif
                            type="radio" name="discount[{{ $discountType->id }}]" value="{{ $discount->id }}">
                            {{ $discount->percent }}%
                        </label>
                    @endforeach
                    <label class="radio-inline-product-form">
                        <input type="radio" name="discount[{{ $discountType->id }}]" value=""
                               @if (empty($selectedDiscounts)) checked @endif>
                        {{ trans('basic.no') }}
                    </label>
                    <p class="help-block">{{ trans('basic.discountInputDesc') }}</p>
                </div>
            </div>
        @endif
    @endforeach
@endif

{{--Donate after fair block--}}
<script>activateOrganisation();</script>
<div class="form-group">
    {!! Form::label('donate_after_fair', trans('basic.donateAfterFair'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
        @if (isset($product) && is_null($donateAfterFairOldValue))
            @php $donateAfterFair = $product->donate_after_fair; @endphp
        @else
            @php $donateAfterFair = $donateAfterFairOldValue; @endphp
        @endif
        <select onchange="activateOrganisation()" id="donate_after_fair" name="donate_after_fair" class="form-control m-bot15">
            <option @if($donateAfterFair == true) selected @endif value="1">
                {{ trans('basic.yes') }}</option>
            <option @if($donateAfterFair == false) selected @endif value="0">
                {{ trans('basic.no') }}</option>
        </select>
        <p class="help-block">{{ trans('basic.donateAfterFairInputDesc') }}</p>
    </div>
</div>

@include('product_form._institution')