<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <!-- CSS -->
    <link href="{{ mix('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ mix('css/dashboard.css') }}" rel="stylesheet">
    @yield('additional_styles')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ mix('js/html5shiv.js') }}"></script>
    <script src="{{ mix('js/respond.min.js') }}"></script>
    <![endif]-->

    <!-- Token -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body>
<section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
        <div class="sidebar-toggle-box">
            <i class="fa fa-bars"></i>
        </div>
        <!--logo-->
        @include('layouts._logo')

        <!--  notification -->
        {{--<div class="nav notify-row" id="top_menu">--}}
            {{--<ul class="nav top-menu">--}}
                {{--<li id="header_notification_bar" class="dropdown">--}}
                    {{--<a data-toggle="dropdown" class="dropdown-toggle" href="#">--}}
                        {{--<i class="fa fa-bell-o"></i>--}}
                        {{--<span class="badge bg-warning">2</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu extended notification">--}}
                        {{--<div class="notify-arrow notify-arrow-yellow"></div>--}}
                        {{--<li>--}}
                            {{--<p class="yellow">--}}
                                {{--{{ trans('basic.notificationsMessage', ['count' => htmlspecialchars(2)]) }}--}}
                            {{--</p>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<span class="label label-danger"><i class="fa fa-bell"></i></span>--}}
                                {{--Server #10 not respoding.--}}
                                {{--<span class="small italic">--}}
                                    {{--{{ trans('basic.hours', ['number' => htmlspecialchars(1)]) }}--}}
                                {{--</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<span class="label label-danger"><i class="fa fa-bell"></i></span>--}}
                                {{--Server #10 not respoding.--}}
                                {{--<span class="small italic">--}}
                                    {{--{{ trans('basic.hours', ['number' => htmlspecialchars(3)]) }}--}}
                                {{--</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">{{ trans('basic.seeAllNotifications') }}</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}

        <!--  tob right bar -->
        <div class="top-nav ">
            <ul class="nav pull-right top-menu">
                <!--  lang block -->
                @include('layouts._langs')

                <!-- user block-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" src="{{ Auth::user()->getAvatarUrl('thumb') }}">
                        <span class="username hidden-xs">

                                {{ Auth::user()->first_name }}

                        </span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href="{{ route('profile.index') }}">
                                <i class=" fa fa-suitcase"></i>{{ trans('auth.profile') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-key"></i>{{ trans('auth.logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header><!--header end-->

    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <ul class="sidebar-menu" id="nav-accordion">
                {{-- Main page --}}
                <li>
                    <a href="{{ route('dashboard') }}" {!! CommonHelper::addActiveClass(['dashboard']) !!}>
                        <i class="fa fa-dashboard" aria-hidden="true"></i>
                        <span>{{ trans('basic.dashboard') }}</span>
                    </a>
                </li>

                {{--Items block--}}
                <li class="sub-menu">
                    <a href="javascript:;" {!! CommonHelper::addActiveClass(['products.create', 'products.index']) !!}>
                        <i class="fa fa-shopping-cart"></i>
                        <span>{{ trans('basic.usersItems') }}</span>
                    </a>
                    <ul class="sub">
                        <li {!! CommonHelper::addActiveClass(['products.create']) !!}>
                            <a  href="{{ route('products.create') }}">
                                {{ trans('basic.addItem') }}
                            </a>
                        </li>
                        <li {!! CommonHelper::addActiveClass(['products.index']) !!}>
                            <a  href="{{ route('products.index') }}">{{ trans('basic.itemsList') }}</a>
                        </li>
                    </ul>
                </li>

                {{--Deliveries block--}}
                <li class="sub-menu">
                    <a href="javascript:;" {!! CommonHelper::addActiveClass(['deliveries.create', 'deliveries.index']) !!}>
                        <i class="fa fa-truck"></i>
                        <span>{{ trans('basic.courier') }}</span>
                    </a>
                    <ul class="sub">
                        <li {!! CommonHelper::addActiveClass(['deliveries.create']) !!}>
                            <a  href="{{ route('deliveries.create') }}">{{ trans('basic.toIssueExport') }}</a>
                        </li>
                        <li {!! CommonHelper::addActiveClass(['deliveries.index']) !!}>
                            <a  href="{{ route('deliveries.index') }}">{{ trans('basic.deliveriesList') }}</a>
                        </li>
                    </ul>
                </li>

                {{--Withdraw money block--}}
                <li>
                    <a href="{{ route('money.index') }}"  {!! CommonHelper::addActiveClass(['money.withdraw']) !!}>
                        <i class="fa fa-money" aria-hidden="true"></i>
                        <span>{{ trans('basic.withdrawMoney') }}</span>
                    </a>
                </li>

                {{--FAQ block--}}
                <li>
                    <a href="{{ route('faq.index') }}"  {!! CommonHelper::addActiveClass(['faq.index']) !!}>
                        <i class="fa fa-question-circle" aria-hidden="true"></i>
                        <span>{{ trans('basic.faq') }}</span>
                    </a>
                </li>
            </ul>
        </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            @yield('content')
        </section>
    </section>
    <!--main content end-->

    <!--footer start-->
    <footer class="site-footer">
        <div class="text-center">
            2017 &copy; Базар Добра.
            <a href="#" class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>
    </footer><!--footer end-->
</section>

<!-- Scripts -->
<script src="{{ mix('js/jquery.js') }}"></script>
<script class="include" type="text/javascript" src="{{ mix('js/jquery.dcjqaccordion.2.7.js') }}"></script>
<script src="{{ mix('js/bootstrap.min.js') }}"></script>
<script src="{{ mix('js/dashboard.js') }}"></script>
@yield('additional_scripts')

@include('templates._messages')
</body>
</html>
