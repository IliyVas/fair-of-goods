<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- CSS -->
    <link href="{{ mix('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ mix('css/dashboard.css') }}" rel="stylesheet">
    @yield('additional_styles')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ mix('js/html5shiv.js') }}"></script>
    <script src="{{ mix('js/respond.min.js') }}"></script>
    <![endif]-->

    <!-- Token -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body>
<section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
        <div class="sidebar-toggle-box">
            <i class="fa fa-bars"></i>
        </div>
        <!--logo-->
        @include('layouts._logo')

        <!--  notification -->
        {{--<div class="nav notify-row" id="top_menu">--}}
            {{--<ul class="nav top-menu">--}}
                {{--<li id="header_notification_bar" class="dropdown">--}}
                    {{--<a data-toggle="dropdown" class="dropdown-toggle" href="#">--}}
                        {{--<i class="fa fa-bell-o"></i>--}}
                        {{--<span class="badge bg-warning">2</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu extended notification">--}}
                        {{--<div class="notify-arrow notify-arrow-yellow"></div>--}}
                        {{--<li>--}}
                            {{--<p class="yellow">--}}
                                {{--{{ trans('basic.notificationsMessage', ['count' => htmlspecialchars(2)]) }}--}}
                            {{--</p>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<span class="label label-danger"><i class="fa fa-bell"></i></span>--}}
                                {{--Server #10 not respoding.--}}
                                {{--<span class="small italic">--}}
                                    {{--{{ trans('basic.hours', ['number' => htmlspecialchars(1)]) }}--}}
                                {{--</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">--}}
                                {{--<span class="label label-danger"><i class="fa fa-bell"></i></span>--}}
                                {{--Server #10 not respoding.--}}
                                {{--<span class="small italic">--}}
                                    {{--{{ trans('basic.hours', ['number' => htmlspecialchars(3)]) }}--}}
                                {{--</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="#">{{ trans('basic.seeAllNotifications') }}</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}

        <!--  tob right bar -->
        <div class="top-nav ">
            <ul class="nav pull-right top-menu">
                <!--  lang block -->
                @include('layouts._langs')

                <!-- user block-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" src="{{ Auth::user()->getAvatarUrl('thumb') }}">
                        <span class="username hidden-xs">

                                {{ Auth::user()->first_name }}

                        </span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li>
                            <a href="{{ route('profile.index') }}">
                                <i class=" fa fa-suitcase"></i>{{ trans('auth.profile') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-key"></i>{{ trans('auth.logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header><!--header end-->

    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <ul class="sidebar-menu" id="nav-accordion">
                {{-- Main page --}}
                <li>
                    <a href="{{ route('admin.dashboard') }}" {!! CommonHelper::addActiveClass(['admin.dashboard']) !!}>
                        <i class="fa fa-dashboard" aria-hidden="true"></i>
                        <span>{{ trans('basic.dashboard') }}</span>
                    </a>
                </li>
                {{--Products list--}}
                <li>
                    <a href="{{ route('admin.products.index') }}"{!! CommonHelper::addActiveClass(['admin.products.index']) !!}>
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <span>{{ trans('basic.itemsList') }}</span>
                    </a>
                </li>
                {{--Deliveries list--}}
                <li>
                    <a href="{{ route('admin.deliveries.index') }}"{!! CommonHelper::addActiveClass(['admin.deliveries.index', 'admin.deliveries.filter']) !!}>
                        <i class="fa fa-truck"></i>
                        <span>{{ trans('basic.deliveriesList') }}</span>
                    </a>
                </li>
                {{--Users list--}}
                <li class="sub-menu">
                    <a href="javascript:;" {!! CommonHelper::addActiveClass(['users.index', 'users.filter']) !!}>
                        <i class="fa fa-users"></i>
                        <span>{{ trans('basic.userIndex') }}</span>
                    </a>
                    <ul class="sub">
                        <li {!! CommonHelper::addActiveClass(['users.index', 'users.filter']) !!}>
                            <a  href="{{ route('users.index') }}">
                                {{ trans('basic.usersList') }}
                            </a>
                        </li>
                    </ul>
                </li>
                {{--Fairs list--}}
                <li>
                    <a href="{{ route('fairs.index') }}"{!! CommonHelper::addActiveClass(['fairs.index']) !!}>
                        <i class="fa fa-list" aria-hidden="true"></i>
                        <span>{{ trans('basic.fairsList') }}</span>
                    </a>
                </li>
                {{--Settings block--}}
                <li class="sub-menu">
                    <a href="javascript:;" {!! CommonHelper::addActiveClass([
                    'questions.categories.index',
                     'questions.index',
                     'settings.index'
                     ]) !!}>
                        <i class="fa fa-cog"></i>
                        <span>{{ trans('basic.settings') }}</span>
                    </a>
                    <ul class="sub">
                        <li {!! CommonHelper::addActiveClass(['settings.index']) !!}>
                            <a href="{{ route('settings.index') }}">
                                <span>{{ trans('basic.genSettings') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:;" {!! CommonHelper::addActiveClass(['questions.categories.index', 'questions.index']) !!}>
                                <span>{{ trans('basic.faq') }}</span>
                            </a>
                            <ul class="sub">
                                <li {!! CommonHelper::addActiveClass(['questions.categories.index']) !!}>
                                    <a  href="{{ route('questions.categories.index') }}">{{ trans('basic.categories') }}</a>
                                </li>
                                <li {!! CommonHelper::addActiveClass(['questions.index']) !!}>
                                    <a  href="{{ route('questions.index') }}">{{ trans('basic.questions') }}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </aside>
    <!--sidebar end-->

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper site-min-height">
            @yield('content')
        </section>
    </section>
    <!--main content end-->

    <!--footer start-->
    <footer class="site-footer">
        <div class="text-center">
            2017 &copy; Базар Добра.
            <a href="#" class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>
    </footer><!--footer end-->
</section>

<!-- Scripts -->
<script src="{{ mix('js/jquery.js') }}"></script>
<script class="include" type="text/javascript" src="{{ mix('js/jquery.dcjqaccordion.2.7.js') }}"></script>
<script src="{{ mix('js/bootstrap.min.js') }}"></script>
<script src="{{ mix('js/dashboard.js') }}"></script>
@yield('additional_scripts')

@include('templates._messages')
</body>
</html>
