<li class="dropdown">
    @if (LaravelLocalization::getCurrentLocale() === config('app.locale'))
        <a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#">
            <div class="lang-container">
                <img src="{{ asset('img/flags/' . config('app.locale') . '.png') }}" alt="">
                <span class="username hidden-xs">{{ LaravelLocalization::getCurrentLocaleNative() }}</span>
                <b class="caret"></b>
            </div>
        </a>
    @else
    @endif
    <ul class="dropdown-menu">
        @foreach(LaravelLocalization::getSupportedLocales() as $locale => $lang)
            @if ($locale !== config('app.locale'))
                <li>
                    <a href="{{ LaravelLocalization::getLocalizedURL($locale, null, [], true) }}" rel="alternate" hreflang="{{ $locale }}">
                        <img src="{{ asset('img/flags/' . $locale . '.png') }}" alt="">
                        {{ $lang['native'] }}
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
</li>