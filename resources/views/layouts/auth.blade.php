<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <!-- CSS -->
    <link href="{{ mix('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ mix('css/dashboard.css') }}" rel="stylesheet">
    @yield('additional_styles')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="{{ mix('js/html5shiv.js') }}"></script>
    <script src="{{ mix('js/respond.min.js') }}"></script>
    <![endif]-->

    <!-- Token -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>

<body class="login-body">
    <div class="container">
        <!--  tob right bar -->
        <div class="top-nav ">
            <ul class="nav pull-right top-menu">
                <!--  lang block -->
                @include('layouts._langs')
            </ul>
        </div>

        @yield("form")
    </div>
    <!-- Scripts -->
    <script src="{{ mix('js/jquery.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ mix('js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ mix('js/bootstrap.min.js') }}"></script>
    <script src="{{ mix('js/dashboard.js') }}"></script>
    @yield('additional_scripts')

    @include('templates._messages')
</body>
</html>