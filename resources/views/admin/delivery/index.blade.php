@extends('layouts.admin_template')

{{--Page settings--}}
@section('title'){{ trans('basic.deliveriesList') }}@endsection
@section('additional_styles')
    <link href="{{ mix('css/grid.css') }}" rel="stylesheet">
@endsection
@section('additional_scripts')
    @php $item =  trans('basic.deliveries'); @endphp
    @include('templates._dynamic_table_trans_script')
    <script type="text/javascript" language="javascript"  src="{{ mix('js/jquery.dataTables.js') }}"></script>
    <script src="{{ mix('js/grid.js') }}"></script>
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script>
        var dateInputs = $('.default-date-picker');
        $('#for_today').on('change', function(){
            if($(this).is(':checked')){
                dateInputs.val(null);
                dateInputs.attr('disabled', 'disabled');
            } else {
                dateInputs.removeAttr('disabled');
            }
        });
    </script>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <section class="panel">
                <header class="panel-heading">
                    {{ trans('basic.filter') }}
                </header>
                <div class="panel-body">
                    <form method="post" action="{{ route('admin.deliveries.filter') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>{{ trans('basic.deliveryDate') }}</label>
                            <div class="row">
                                @if (isset($filter['date_from']) && !is_null($filter['date_from']))
                                    @php $dateFrom = $filter['date_from']; @endphp
                                @else  @php $dateFrom = old('date_from'); @endphp @endif
                                <div class="col-xs-6">
                                    <input type="text" class="form-control default-date-picker"
                                           value="{{ $dateFrom }}" placeholder="{{ trans('basic.from')}}" name="date_from">
                                </div>
                                @if (isset($filter['date_to']) && !is_null($filter['date_to']))
                                    @php $dateTo = $filter['date_to']; @endphp
                                @else  @php $dateTo = old('date_to'); @endphp @endif
                                <div class="col-xs-6">
                                    <input type="text" class="form-control default-date-picker"
                                           value="{{ $dateTo }}" placeholder="{{ trans('basic.to')}}" name="date_to">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>{{ trans('basic.status') }}</label>
                            <p>
                                <input @if(isset($filter['status']) && $filter['status'] == true) checked @endif
                                type="radio" name="status" value="1"> {{ trans('basic.confirmed') }} <br>
                                <input  @if(isset($filter['status']) && $filter['status'] == false) checked @endif
                                type="radio" name="status" value="0"> {{ trans('basic.notConfirmed') }} <br>
                            </p>
                        </div>
                        <div class="form-group">
                            <label>{{ trans('basic.type') }}</label>
                            <p>
                                <input  @if(isset($filter['type']) && $filter['type'] == true) checked @endif
                                type="radio" name="type" value="1"> {{ trans('basic.exportType') }} <br>
                                <input  @if(isset($filter['type']) && $filter['type'] == false) checked @endif
                                type="radio" name="type" value="0"> {{ trans('basic.returnType') }} <br>
                            </p>
                        </div>
                        <div class="form-group border-top">
                            <p class="mtop20">
                                <input id="for_today" @if(isset($filter['today'])) checked @endif type="checkbox" name="today" value="1"> {{ trans('basic.showAllTodayDeliveries') }} <br>
                            </p>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">
                            {{ trans('basic.filterBtn') }}
                        </button>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-9">
            @include('templates._delivery_table')
        </div>
    </div>
@endsection