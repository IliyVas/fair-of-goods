@extends('layouts.admin_template')

@widget('Delivery\Form', [
'delivery' => $delivery,
'products' => $delivery->user->products,
'route'    => route('admin.deliveries.update', [$delivery->user_id, $delivery->id]),
 ])