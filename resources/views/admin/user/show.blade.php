@extends('layouts.admin_template')

@section('title')
    {{ $user->first_name . ' ' . $user->last_name }}
@endsection

@section('content')
    @widget('Profile', ['user' => $user, 'admin' => true])
@endsection