@extends('layouts.admin_template')

@widget('Delivery\Grid', [
'deliveries'           => $deliveries,
'routeName'            => 'users.deliveries.edit',
'showIssueExportBtn'   => true,
'issueExportRoute'     => route('users.deliveries.create', ['user' => $user->id]),
'deleteRoute'          => route('users.deliveries.destroy', ['user' =>  $user->id]),
'showProductRouteName' => 'users.products.show',
 ])