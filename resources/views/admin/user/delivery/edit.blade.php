@extends('layouts.admin_template')

@widget('Delivery\Form', [
'delivery' => $delivery,
'products' => $delivery->user->products,
'route'    => route('users.deliveries.update', [$delivery->user_id, $delivery->id]),
 ])