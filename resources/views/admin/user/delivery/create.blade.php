@extends('layouts.admin_template')

@widget('Delivery\Form', ['delivery'        => $delivery,
                         'pageName'         => trans('basic.toIssueExport'),
                         'selectedProducts' => $selectedProducts,
                         'editing'          => false,
                         'method'           => 'POST',
                         'products'         => $products,
                         'route'            => route('users.deliveries.store', [$user->id])
                         ])