@extends('layouts.admin_template')

{{--Page settings--}}
@section('title')
    {{ trans('basic.addUser') }}
@endsection
@section('additional_styles')
    <link href="{{ mix('css/grid.css') }}" rel="stylesheet">
@endsection

{{--Activate menu item--}}
@section('userIndex') active @endsection

{{--Main page content--}}
@section('content')
    <section class="panel">
        <header class="panel-heading">
            {{ trans('basic.addUser') }}
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div class="space15"></div>

                <div class="panel-body">
                    <form action="{{ route('users.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>{{ trans('basic.firstName')  }}</label>
                                    <input type="text" class="form-control" value="{{ old('first_name') }}" name="first_name">
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('first_name') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>{{ trans('basic.lastName')  }}</label>
                                    <input type="text" class="form-control" value="{{ old('last_name') }}" name="last_name">
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('last_name') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>{{ trans('basic.email')  }}</label>
                                    <input type="text" class="form-control" value="{{ old('email') }}" name="email">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('email') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div>
                                        <label class="clearfix">{{ trans('basic.userSocial') }}</label>
                                    </div>
                                    <label class="label_check c_on" for="checkbox-01">
                                        <input name="social" id="checkbox-01" type="checkbox">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div>
                                        <label class="clearfix">{{ trans('basic.userActive') }}</label>
                                    </div>
                                    <label class="label_check c_on" for="checkbox-02">
                                        <input name="active" id="checkbox-02" type="checkbox">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>{{ trans('basic.password')  }}</label>
                                    <input type="password" class="form-control"  name="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('password') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>{{ trans('basic.passwordConfirm')  }}</label>
                                    <input type="password" class="form-control"  name="password_confirm">
                                    @if ($errors->has('password_confirm'))
                                        <span class="help-block">
                                            <strong>
                                                <small>{{ $errors->first('password_confirm') }}</small>
                                            </strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-info" type="submit">
                                    {{ trans('basic.addUser') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('additional_scripts')
    <script type="text/javascript" language="javascript" src="{{ mix('js/jquery.dataTables.js') }}"></script>
    <script src="{{ mix('js/grid.js') }}"></script>
@endsection
