@extends('layouts.admin_template')

{{--Page settings--}}
@section('title')
    {{ trans('basic.usersList') }}
@endsection
@section('additional_styles')
    <link href="{{ mix('css/grid.css') }}" rel="stylesheet">
@endsection
@section('additional_scripts')
    @php $item =  trans('basic.users'); @endphp
    @include('templates._dynamic_table_trans_script')
    <script type="text/javascript" language="javascript"  src="{{ mix('js/jquery.dataTables.js') }}"></script>
    <script src="{{ mix('js/grid.js') }}"></script>
@endsection

{{--Main page content--}}
@section('content')
    <section class="panel col-md-3">
        <header class="panel-heading">
            {{ trans('basic.filter') }}
        </header>
        <div class="panel-body">
            <form method="post" action="{{ route('users.filter') }}">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>{{ trans('basic.firstName') }}</label>
                    <input type="text" class="form-control" value="{{ $filter['name'] or old('name') }}" name="name">
                </div>
                <div class="form-group">
                    <label>{{ trans('basic.userAge') }}</label>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="number" class="form-control" placeholder="{{ trans('basic.from')}}"
                                   value="{{ $filter['age_from'] or old('age_from') }}" name="age_from" min="0" max="150">
                        </div>
                        <div class="col-xs-6">
                            <input type="number" class="form-control" placeholder="{{ trans('basic.to')}}"  min="0" max="150"
                                   value="{{ $filter['age_to'] or old('age_to') }}" name="age_to">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>{{ trans('basic.userProducts') }}</label>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="number" class="form-control" placeholder="{{ trans('basic.from')}}" min="0"
                                   value="{{ $filter['products_from'] or old('products_from') }}" name="products_from">
                        </div>
                        <div class="col-xs-6">
                            <input type="number" class="form-control" placeholder="{{ trans('basic.to')}}" min="0"
                                   value="{{ $filter['products_to'] or old('products_to') }}" name="products_to">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>{{ trans('basic.countsDonate') }}</label>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="number" class="form-control" placeholder="{{ trans('basic.from')}}" min="0"
                                   value="{{ $filter['donate_from'] or old('donate_from') }}" name="donate_from">
                        </div>
                        <div class="col-xs-6">
                            <input type="number" class="form-control" placeholder="{{ trans('basic.to')}}" min="0"
                                   value="{{ $filter['donate_to'] or old('donate_to') }}" name="donate_to">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>{{ trans('basic.sum') }}</label>
                    <div class="row">
                        <div class="col-xs-6">
                            <input type="number" class="form-control" placeholder="{{ trans('basic.from')}}" min="0"
                                   value="{{ $filter['sum_from'] or old('sum_from') }}" name="sum_from">
                        </div>
                        <div class="col-xs-6">
                            <input type="number" class="form-control" placeholder="{{ trans('basic.to')}}" min="0"
                                   value="{{ $filter['sum_to'] or old('sum_to') }}" name="sum_to">
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">
                    {{ trans('basic.filterBtn') }}
                </button>
            </form>
        </div>
    </section>
    <div class="col-md-9">
        <section class="panel">
            <header class="panel-heading text-right">
                <span class="pull-left">
                {{ trans('basic.usersList') }}
            </span>
                <a href="{{ route('users.download') }}" type="button" id="loading-btn" class="btn btn-warning btn-xs">
                    <i class="fa fa-download"></i> {{ trans('basic.btnDownloadData') }}
                </a>
            </header>
            <div class="panel-body">
                <div class="adv-table">
                    <div class="table-responsive">
                        <table class="table-striped table-hover display table table-bordered" id="users-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ trans('basic.firstName') }}</th>
                                <th>{{ trans('basic.userAge') }}</th>
                                <th>{{ trans('auth.email') }}</th>
                                <th>{{ trans('basic.userProducts') }}</th>
                                <th>{{ trans('basic.countsDonate') }}</th>
                                <th>{{ trans('basic.sum') }}</th>
                                <th class="text-center">{{ trans('basic.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $revenue = 0; $transaction = 0; @endphp
                            @foreach($users as $model)
                                @if ($model->isUser())
                                    @php
                                        $revenue += $model->getTotalRevenue();
                                         $transaction += $model->getTransactionsAmount();
                                    @endphp
                                    <tr>
                                        <td>{{ $model->id }}</td>
                                        <td>
                                            <a href="{{ route('users.show', ['id' => $model->id]) }}" title="{{ trans('basic.view') }}">
                                                {{ $model->first_name . ' ' . $model->last_name }}
                                            </a>
                                        </td>
                                        @php $age = $model->getAge(); @endphp
                                        <td>@if ($age == 0) &mdash; @else {{ $age }} @endif</td>
                                        <td>{{ $model->email }}</td>
                                        <td>{{ $model->countProducts }}</td>
                                        <td>{{ $model->countDonateProducts }}</td>
                                        <td class="action-td-lg">
                                            {{ trans('basic.total') . ': ' }}
                                            <strong>{{ $model->getTotalRevenue() }}</strong>
                                            {{ ' ' . trans('basic.rub') }}
                                            <br>
                                            {{ trans('basic.transferred') . ': ' }}
                                            <strong>{{ $model->getTransactionsAmount() }}</strong>
                                            {{ ' ' . trans('basic.rub') }}
                                        </td>
                                        <td class="action-td-lg">
                                            {{--View items btn block--}}
                                            <a href="{{ route('users.products.index', [$model->id]) }}"
                                               class="btn btn-warning btn-xs" title="{{ trans('basic.viewProducts')}}">
                                                <i class="fa fa-shopping-cart"></i>
                                            </a>
                                            {{--Money is given in cash btn block--}}
                                            @if ($model->canWithdrawMoney())
                                                <a href="{{ route('users.confirmWithdrawMoney', [$model->id]) }}"
                                                   class="btn btn-info btn-xs" title="{{ trans('basic.moneyInCash')}}">
                                                    <i class="fa fa-money"></i>
                                                </a>
                                            @endif
                                            {{--View deliveries btn block--}}
                                            <a href="{{ route('users.deliveries.index', [$model->id]) }}"
                                               class="btn btn-success btn-xs" title="{{ trans('basic.viewExports')}}">
                                                <i class="fa fa-truck"></i>
                                            </a>
                                            {{--Delete btn block--}}
                                            <a id="del-{{ $model->id }}" href="#confirm{{ $model->id }}" class="btn btn-danger btn-xs"
                                               title="{{ trans('basic.delete')}}" data-toggle="modal">
                                                <i class="fa fa-trash-o "></i>
                                            </a>
                                            @php $delMsg = trans('messages.deleteUserConfirm',['id' => htmlspecialchars($model->id)]); @endphp
                                            @include('templates._confirm_single_deletion')
                                            <form id="delete_{{ $model->id }}" method="post" class="hidden"
                                                  action="{{ route('users.destroy', [$model->id]) }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ trans('basic.toTransfer', ['sum' => htmlspecialchars($revenue - $transaction)]) }}
                </div>
            </div>
        </section>
    </div>
@endsection
