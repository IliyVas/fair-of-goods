@extends('layouts.admin_template')
@section('title'){{ trans('basic.addItem') }}@endsection

@widget('Product\Form', [
'pageName' => trans('basic.createProductTitle'),
'route'    => ['users.products.store', $user->id],
])
