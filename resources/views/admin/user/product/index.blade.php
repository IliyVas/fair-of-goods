@extends('layouts.admin_template')

@widget('Product\Index', [
'products'               => $products,
'deleteRoute'            => route('users.products.destroy', ['userId' => $user->id]),
'editRoute'              => 'users.products.edit',
'showRoute'              => 'users.products.show',
'admin'                  => true,
'showIssueExportBtn'     => true,
'issueExportRoute'       => route('users.deliveries.create', ['user' => $user->id]),
'showCreateItemBtn'      => true,
'createItemRoute'        => route('users.products.create', ['user' => $user->id]),
'downloadItemsRoute'     => route('users.products.download', ['user' => $user->id]),
'downloadPriceListRoute' => route('users.pricelist.download', ['user' => $user->id]),
'duplicateRoute'         => 'users.products.duplicate',
])