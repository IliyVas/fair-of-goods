@extends('layouts.admin_template')

@widget('Product\Show', [
'model'        => $model,
'editRoute'    => 'users.products.edit',
'deleteRoute'  => 'users.products.destroy',
'admin'        => true,
])