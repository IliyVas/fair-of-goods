@extends('layouts.admin_template')

{{--Page settings--}}
@section('title')
    {{ trans('basic.userEdit') }}
@endsection
@section('additional_styles')
    <link href="{{ mix('css/grid.css') }}" rel="stylesheet">
@endsection

{{--Activate menu item--}}
@section('userIndex') active @endsection

{{--Main page content--}}
@section('content')
    <section class="panel">
        <header class="panel-heading">
            {{ trans('basic.userEdit') }} {{ $user->email }}
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div class="space15"></div>

                <div class="panel-body">
                    {!! Form::model($user, ['route' => ['users.update', $user->id]]) !!}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('first_name', trans('basic.firstName')) !!}
                                    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('last_name', trans('basic.lastName')) !!}
                                    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('email', trans('basic.email')) !!}
                                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div>
                                        {!! Form::label('social', trans('basic.userSocial'), ['class' => 'clearfix']) !!}
                                    </div>
                                    <label class="label_check">
                                        {!! Form::checkbox('social') !!}
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div>
                                        {!! Form::label('active', trans('basic.userActive'), ['class' => 'clearfix']) !!}
                                    </div>
                                    <label class="label_check">
                                        {!! Form::checkbox('active') !!}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-info" type="submit">
                                    {{ trans('basic.save') }}
                                </button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection
@section('additional_scripts')
    <script type="text/javascript" language="javascript" src="{{ mix('js/jquery.dataTables.js') }}"></script>
    <script src="{{ mix('js/grid.js') }}"></script>
@endsection
