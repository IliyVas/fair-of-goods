@extends('layouts.admin_template')

{{--Page settings--}}
@section('title'){{ trans('basic.questionCategories') }}@endsection
@section('additional_styles')
    <link href="{{ mix('css/grid.css') }}" rel="stylesheet">
@endsection
@section('additional_scripts')
    @php $item =  trans('basic.categoriesTbl'); @endphp
    @include('templates._dynamic_table_trans_script')
    <script type="text/javascript" language="javascript"  src="{{ mix('js/jquery.dataTables.js') }}"></script>
    <script src="{{ mix('js/grid.js') }}"></script>
    <script type="text/javascript" src="{{ mix('js/question-category-table.js') }}"></script>
@endsection

@section('content')
    <section class="panel">
        <header class="panel-heading">
            {{ trans('basic.questionCategories') }}
        </header>
        <div class="panel-body">
            <div class="adv-table editable-table ">
                <div class="clearfix">
                    <div class="btn-group">
                        <button id="editable-sample_new" class="btn green">
                            {{ trans('basic.addNewCategory') }} <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="space15"></div>

                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('basic.categoryNameRu') }}</th>
                            <th>{{ trans('basic.categoryNameEn') }}</th>
                            <th>{{ trans('basic.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($categories) > 0)
                            @foreach($categories as $model)
                                <tr class="">
                                    <td>{{ $model->id }}</td>
                                    <td>{{ $model->getTranslation('ru')->name }}</td>
                                    <td>{{ $model->getTranslation('en')->name }}</td>
                                    <td class="action-td">
                                        <a class="edit btn-info btn btn-xs" href="javascript:;" title="{{ trans('basic.edit') }}">
                                            <i class="fa fa-pencil"></i>
                                        </a><!--
                                        {{--Deletion block--}}
                                        --><a id="delete_{{ $model->id }}" class="delete"></a><!--
                                        --><a href="#confirm{{ $model->id }}" class="btn btn-danger btn-xs"
                                           title="{{ trans('basic.delete')}}" data-toggle="modal">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </td>
                                </tr>
                                @php $editableTbl = true; $delMsg = trans('messages.deleteQuestCategoryConfirm',['id' => htmlspecialchars($model->id)]); @endphp
                                @include('templates._confirm_single_deletion')
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection