@if (count($categories) > 0)
    <select class="form-control small" name="category_id">
        @foreach($categories as $category)
            <option value="{{ $category->id }}" @if($category->id == $selected) selected @endif>
                {{ $category->name }}
            </option>
        @endforeach
    </select>
@endif