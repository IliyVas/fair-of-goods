@extends('layouts.admin_template')

{{--Page settings--}}
@section('title'){{ $fair->name }}@endsection
@section('additional_styles')
    <link href="{{ mix('css/form.css') }}" rel="stylesheet">
@endsection
@section('additional_scripts')
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/form.js') }}"></script>
@endsection

{{--Main page content--}}
@section('content')
    <section class="panel">
        <header class="panel-heading">
            {{ trans('basic.editFair', ['name' => htmlspecialchars($fair->name)]) }}
        </header>
        <div class="panel-body">
            {!! Form::model($fair, ['route' => ['fairs.update', $fair->id], 'role' => 'form', 'class' => 'form-horizontal']) !!}
            {{ method_field('PATCH') }}
            {{ csrf_field() }}

            {{--Name block--}}
            <div class="form-group">
                {!! Form::label('name', trans('basic.fairName'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            {!! Form::text('name:ru', null, ['class' => 'form-control']) !!}
                            <p class="help-block">{{ trans('basic.fairNameRuInputDesc') }}</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            {!! Form::text('name:en', null, ['class' => 'form-control']) !!}
                            <p class="help-block">{{ trans('basic.fairNameEnInputDesc') }}</p>
                        </div>
                    </div>
                </div>
            </div>

            {{--Description block--}}
            <div class="form-group">
                {!! Form::label('description', trans('basic.desc'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            {!! Form::textarea('description:ru', null, ['class' => 'form-control']) !!}
                            <p class="help-block">{{ trans('basic.fairDescRuInputDesc') }}</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            {!! Form::textarea('description:en', null, ['class' => 'form-control']) !!}
                            <p class="help-block">{{ trans('basic.fairDescEnInputDesc') }}</p>
                        </div>
                    </div>
                </div>
            </div>

            {{--Address block--}}
            <div class="form-group">
                {!! Form::label('address', trans('basic.address'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            {!! Form::textarea('address:ru', null, ['class' => 'form-control', 'rows' => '4']) !!}
                            <p class="help-block">{{ trans('basic.fairAddressRuInputDesc') }}</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            {!! Form::textarea('address:en', null, ['class' => 'form-control', 'rows' => '4']) !!}
                            <p class="help-block">{{ trans('basic.fairAddressEnInputDesc') }}</p>
                        </div>
                    </div>
                </div>
            </div>

            {{--Dates block--}}
            <div class="form-group">
                {!! Form::label('address', trans('basic.datesOfThe'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            {!! Form::text('start_date', null, ['class' => 'form-control default-date-picker']) !!}
                            <p class="help-block">{{ trans('basic.fairStartDateInputDesc') }}</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            {!! Form::text('finish_date', null, ['class' => 'form-control default-date-picker']) !!}
                            <p class="help-block">{{ trans('basic.fairFinishDateInputDesc') }}</p>
                        </div>
                    </div>
                </div>
            </div>

            {{--lat && long block--}}
            <div class="form-group">
                {!! Form::label('lat-long', trans('basic.latLong'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            {!! Form::text('lat', null, ['class' => 'form-control']) !!}
                            <p class="help-block">{{ trans('basic.latInputDesc') }}</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            {!! Form::text('long', null, ['class' => 'form-control']) !!}
                            <p class="help-block">{{ trans('basic.longInputDesc') }}</p>
                        </div>
                    </div>
                </div>
            </div>

            {{--Save changes btn--}}
            <div class="form-group">
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-success pull-right">
                        {{ trans('basic.btnSaveChanges') }}
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@endsection