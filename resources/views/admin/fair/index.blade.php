@extends('layouts.admin_template')

{{--Page settings--}}
@section('title'){{ trans('basic.fairsList') }}@endsection
@section('additional_styles')
    <link href="{{ mix('css/grid.css') }}" rel="stylesheet">
@endsection
@section('additional_scripts')
    @php $item =  trans('basic.fairs'); @endphp
    @include('templates._dynamic_table_trans_script')
    <script type="text/javascript" language="javascript"  src="{{ mix('js/jquery.dataTables.js') }}"></script>
    <script src="{{ mix('js/grid.js') }}"></script>
@endsection

{{--Main page content--}}
@section('content')
    <section class="panel">
        <header class="panel-heading">
            {{ trans('basic.fairsList') }}
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div class="table-responsive">
                    <table class="table-striped table-hover display table table-bordered" id="fair-table">
                        <thead>
                        <tr>
                            <th>{{ trans('basic.name') }}</th>
                            <th>{{ trans('basic.address') }}</th>
                            <th>{{ trans('basic.datesOfThe') }}</th>
                            <th id="desc_btn">{{ trans('basic.btnShowDescription') }}</th>
                            <th>{{ trans('basic.actions') }}</th>
                            <th class="hidden"></th>
                        </tr>
                        </thead>

                        @if (count($fairs) > 0)
                            <tbody>
                            @foreach($fairs as $fair)
                                <tr>
                                    <td>{{ $fair->name }}</td>
                                    <td>{{ $fair->address }}</td>
                                    <td>
                                        {{ date_create($fair->start_date)->format('d.m.Y') . ' - ' .
                                        date_create($fair->finish_date)->format('d.m.Y') }}
                                    </td>
                                    <td class="desc_btn center">
                                        <img class="img-btn" src="{{ asset('img/details_open.png') }}">
                                    </td>
                                    <td class="action-td">
                                        @if ($fair->isEditable())
                                            {{--Edit btn block--}}
                                            <a href="{{ route('fairs.edit', [$fair->id]) }}" class="btn btn-primary btn-xs"
                                               title="{{ trans('basic.edit') }}">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td class="hidden"> {{ $fair->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection