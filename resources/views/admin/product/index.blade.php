@extends('layouts.admin_template')

{{--Page settings--}}
@section('title'){{ trans('basic.itemsList') }}@endsection
@section('additional_styles')
    <link href="{{ mix('css/grid.css') }}" rel="stylesheet">
@endsection
@section('additional_scripts')
    @php $item =  trans('basic.items'); @endphp
    @include('templates._dynamic_table_trans_script')
    <script type="text/javascript" language="javascript"  src="{{ mix('js/jquery.dataTables.js') }}"></script>
    <script src="{{ mix('js/grid.js') }}"></script>
    <script src="{{ mix('js/products_list.js') }}"></script>
    @include('templates._product_form_script')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            <section class="panel">
                <header class="panel-heading">{{ trans('basic.category') }}</header>
                <div class="panel-body">
                    <ul class="nav prod-cat">
                        @foreach($categories as $category)
                            <li>
                                @php $categoryFilter = isset($filteredByCategory) && !is_null($filteredByCategory) && $filteredByCategory->parent_id == $category->id; @endphp
                                <a href="javascript:;" id="category_{{ $category->id }}"
                                   class="category_block @if($categoryFilter) active @endif"
                                   data-category="{{ $category->id }}">
                                    <i class="fa fa-angle-right"></i> {{ $category->name }}
                                </a>
                                @if (count($category->subcategories) > 0)
                                    <ul id="open_{{ $category->id }}" class="subcategory_block @if(!$categoryFilter) hidden @endif">
                                        @foreach($category->subcategories as $subcategory)
                                            <li>
                                                <a data-subcategory="{{ $subcategory->id }}"
                                                   class="@if($categoryFilter && $filteredByCategory->id == $subcategory->id) active @endif"
                                                   href="{{ route('admin.products.filter.category', [$subcategory->id]) }}">
                                                    {{ $subcategory->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    {{ trans('basic.filter') }}
                </header>
                <div class="panel-body">
                    <form method="post" action="{{ route('admin.products.filter.status') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            @foreach($statuses as $status)
                                <input @if(isset($filteredByStatuses) && in_array($status, $filteredByStatuses)) checked @endif
                               type="checkbox" name="statuses[]" value="{{ $status }}">
                                {{ trans('basic.' . $status . 'ProductStatus') }}<br>
                            @endforeach
                        </div>
                        <button class="btn btn-primary pull-right" type="submit">{{ trans('basic.filterBtn') }}</button>
                    </form>
                </div>
            </section>
        </div>
        <div class="col-md-9">
            @include('templates._product_table')
        </div>
    </div>
@endsection