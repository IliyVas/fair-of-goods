@extends('layouts.admin_template')
@section('title'){{ $product->name }}@endsection

@widget('Product\Form', [
'pageName' => trans('basic.editProduct', ['name' => htmlspecialchars($product->name)]),
'route'    => ['admin.products.update', $product->user_id, $product->id],
'method'   => 'PATCH',
'product'  => $product
])