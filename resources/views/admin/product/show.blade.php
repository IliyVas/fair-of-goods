@extends('layouts.admin_template')

@widget('Product\Show', [
'model'        => $model,
'editRoute'    => 'admin.products.edit',
'deleteRoute'  => 'admin.products.destroy',
])