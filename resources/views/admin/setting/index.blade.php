@extends('layouts.admin_template')

{{--Page settings--}}
@section('title')
    {{ trans('basic.genSettingsFull') }}
@endsection

{{--Main page content--}}
@section('content')
    <section class="panel">
        <header class="panel-heading">
            {{ trans('basic.userRevenueSetting') }}
            <a href="{{ route('settings.revenuePercent.update') }}" class="btn btn-warning btn-xs pull-right">
                <i class="fa fa-refresh"></i> {{ trans('basic.btnUpdateRevenuePercent') }}
            </a>
        </header>
        <div class="panel-body">
            <form method="POST" class="form-horizontal tasi-form" action="{{ route('settings.update', [Settings::REVENUE_PERCENT]) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                    <label class="col-md-4 col-sm-6 col-xs-8 control-label">
                        {{ trans('basic.revenuePercentAdm') }}:
                    </label>
                    <div class="col-sm-2 col-xs-4 input-group m-bot15">
                        <input min="1" max="100" required type="number" name="value"
                               class="form-control" value="{{ Settings::getRevenuePercent() }}">
                        <span class="input-group-addon">%</span>
                    </div>
                    <div class="mtop20 col-xs-12 text-right">
                        <button class="btn btn-success" type="submit">
                            {{ trans('basic.save') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
