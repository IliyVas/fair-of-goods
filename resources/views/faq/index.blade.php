@extends('layouts.user_template')

{{--Page settings--}}
@section('title') {{ trans('basic.faq') }} @endsection

{{--Main page content--}}
@section('content')
    @if (count($categories) > 0)
        <div class="row">
            <div class="col-md-9">
                <div class="tab-content">
                    @foreach($categories as $category)
                        <div class="tab-pane @if ($category->id == 1) active @endif" id="tab_{{ $category->id }}">
                            <div class="panel-group" id="accordion{{ $category->id }}">
                                @if (count($category->questions) > 0)
                                    @php $i = 0; @endphp
                                    @foreach($category->questions as $item)
                                        @php $i++; @endphp
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a href="#accordion{{ $category->id }}_{{ $item->id }}"
                                                       data-parent="#accordion{{ $category->id }}" data-toggle="collapse"
                                                       class="accordion-toggle">
                                                        {{ $i . '. ' . $item->question }}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse collapse" id="accordion{{ $category->id }}_{{ $item->id }}">
                                                <div class="panel-body">
                                                    {{ $item->answer }}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-3">
                <ul class="vertical-menu">
                    @foreach($categories as $category)
                        <li @if ($category->id == 1) class="active" @endif>
                            <a href="#tab_{{ $category->id }}" data-toggle="tab">
                                <i class="fa fa-bullhorn"></i>
                                {{ $category->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @else
        {{ trans('basic.infoEmpty', ['item' => htmlspecialchars(trans('basic.questionsTbl'))]) }}
    @endif
@endsection