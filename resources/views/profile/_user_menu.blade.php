<aside class="profile-nav col-lg-3 col-sm-2 col-xs-12">
    <section class="panel">
        <div class="user-heading round">
            <a href="#">
                <img src="{{ $user->getAvatarUrl() }}">
            </a>
            <h1>{{ $user->first_name . ' ' . $user->last_name }}</h1>
            <p>{{ $user->email }}</p>
        </div>
        <ul class="nav nav-pills nav-stacked">
        @if (!isset($admin) || !$admin)
            <ul class="nav nav-pills nav-stacked">
                <li {!! CommonHelper::addActiveClass(['profile.index']) !!}>
                    <a href="{{ route('profile.index') }}">
                        <i class="fa fa-user"></i> {{ trans('auth.profile') }}
                    </a>
                </li>
                <li {!! CommonHelper::addActiveClass(['profile.edit']) !!} >
                    <a href="{{ route('profile.edit', ['id' => $user->id]) }}">
                        <i class="fa fa-edit"></i> {{ trans('basic.editProfile') }}
                    </a>
                </li>
            @else
                {{--View items btn block--}}
                <li>
                    <a href="{{ route('users.products.index', [$user->id]) }}">
                        <i class="fa fa-shopping-cart"></i> {{ trans('basic.viewProducts')}}
                    </a>
                </li>
                {{--View deliveries btn block--}}
                <li>
                    <a href="{{ route('users.deliveries.index', [$user->id]) }}">
                        <i class="fa fa-truck"></i> {{ trans('basic.viewExports')}}
                    </a>
                </li>
            @endif
        </ul>
    </section>
</aside>