@if (Auth::user()->isAdmin()) @php $template = 'admin_template'; @endphp
@else @php $template = 'user_template'; @endphp @endif
@extends('layouts.' . $template)

@section('title', trans('auth.regPhone'))

@section('additional_styles')
    <link rel="stylesheet" href="{{ mix('css/phone.css') }}">
@endsection

@section('content')
    <form class="form-signin" method="POST" action="{{ route('phone.save') }}">
        {{ csrf_field() }}
        <h2 class="form-signin-heading">{{trans('auth.phone')}}</h2>
        <div class="login-wrap">
            <span id="success-msg" class="hide text-success">{{ trans('messages.validPhone') }}</span>
            <span id="error-msg" class="hide text-danger">{{ trans('messages.invalidPhone') }}</span>
            <input value="{{ old('phone') }}" type="text" name="phone" id="phone" class="form-control input-lg bg-success" required>
            <input type="hidden" name="isValidPhone" id="isValidPhone" value="0">

            <button class="btn btn-lg btn-login btn-block" type="submit">{{trans('basic.save')}}</button>
        </div>
    </form>
@endsection

@section('additional_scripts')
    <script src="{{ mix('js/intlTelInput.js') }}"></script>
    <script src="{{ mix('js/intlTelInput_ini.js') }}"></script>
@endsection
