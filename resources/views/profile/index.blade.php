@if (Auth::user()->isAdmin()) @php $template = 'admin_template'; @endphp
@else @php $template = 'user_template'; @endphp @endif
@extends('layouts.' . $template)

@section('title')
    {{ trans('auth.profile') }}
@endsection

@section('content')
    @widget('Profile', ['user' => $user])
@endsection
