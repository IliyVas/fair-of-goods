@if (Auth::user()->isAdmin()) @php $template = 'admin_template'; @endphp
@else @php $template = 'user_template'; @endphp @endif
@extends('layouts.' . $template)
@section('title')
    {{ trans('basic.editProfile')  }}
@endsection
@section('additional_styles')
    <link rel="stylesheet" href="{{ mix('css/phone.css') }}">
@endsection

@section('content')
    <div class="row">

        @include('profile._user_menu')

        <aside class="profile-info col-lg-9 col-sm-10 col-xs-12">
            <section class="panel">
                <div class="bio-graph-heading">
                    {{ trans('basic.editProfile')  }}
                </div>

                <div class="panel-body bio-graph-info">
                    <h1>{{ trans('basic.profileTitle')  }}</h1>
                    <form action="{{route('profile.update', ['id' => $user->id])}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label class="col-lg-4 control-label">{{ trans('basic.firstName')  }}</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" value="{{ $user->first_name }}" name="first_name">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('first_name') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">{{ trans('basic.lastName')  }}</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" value="{{ $user->last_name }}" name="last_name">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('last_name') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">{{ trans('auth.regPhone')  }}</label>
                            <div class="col-lg-8">
                                <span id="success-msg" class="hide text-success">{{ trans('messages.validPhone') }}</span>
                                <span id="error-msg" class="hide text-danger">{{ trans('messages.invalidPhone') }}</span>
                                <input type="text" class="form-control" value="{{ $user->phone }}" name="phone" required id="phone">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('phone') }}</small>
                                        </strong>
                                    </span>
                                @endif
                                <input type="hidden" name="isValidPhone" id="isValidPhone" value="0">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">{{ trans('basic.birthday')  }}</label>
                            <div class="col-lg-8">
                                <input class="form-control default-date-picker" type="text"
                                       value="@if (!is_null($user->birthday)){{ date_create($user->birthday)->format('d.m.Y') }} @endif" name="birthday" />
                                @if ($errors->has('birthday'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('birthday') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label">{{ trans('basic.changeAvatar')  }}</label>
                            <div class="col-lg-8">
                                <input type="file" class="form-control" name="avatar">
                                @if ($errors->has('avatar'))
                                    <span class="help-block">
                                        <strong>
                                            <small>{{ $errors->first('avatar') }}</small>
                                        </strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-right">
                                <button type="submit" class="btn btn-success">{{ trans('basic.save') }}</button>
                                <a href="{{ route('profile.index') }}" class="btn btn-default">{{ trans('basic.cancel') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </section>

            {{--Password block--}}
            @if(!$user->social)
                <section>
                    <div class="panel panel-primary">
                        <div class="panel-heading">{{ trans('basic.titlePassword') }}</div>
                        <div class="panel-body">
                            <form action="{{ route('profile.pass', ['id' => $user->id]) }}" class="form-horizontal" method="post">
                                {{ csrf_field() }}
                                @if(!is_null($user->password))
                                    <div class="form-group">
                                        <label class="col-lg-4 control-label">{{ trans('basic.currentPass')  }}</label>
                                        <div class="col-lg-8">
                                            <input type="password" class="form-control" name="current_password">
                                            @if ($errors->has('current_password'))
                                                <span class="help-block">
                                                    <strong>
                                                        <small>{{ $errors->first('current_password') }}</small>
                                                    </strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">{{ trans('basic.newPass')  }}</label>
                                    <div class="col-lg-8">
                                        <input type="password" class="form-control" name="password">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('password') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">{{ trans('basic.reNewPass')  }}</label>
                                    <div class="col-lg-8">
                                        <input type="password" class="form-control" name="password_confirm">
                                        @if ($errors->has('password_confirm'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('password_confirm') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12 text-right">
                                        <button type="submit" class="btn btn-info">{{ trans('basic.save') }}</button>
                                        <a href="{{ route('profile.index') }}" class="btn btn-default">{{ trans('basic.cancel') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            @endif

            {{--Email block--}}
            @if(!$user->social)
                <section>
                    <div class="panel panel-primary">
                        <div class="panel-heading">{{ trans('basic.titleEmail') }}</div>
                        <div class="panel-body">
                            <form action="{{ route('profile.email', ['id' => $user->id]) }}" class="form-horizontal" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">{{ trans('basic.newEmail')  }}</label>
                                    <div class="col-lg-8">
                                        <input type="email" class="form-control" name="new_email">
                                        @if ($errors->has('new_email'))
                                            <span class="help-block">
                                                <strong>
                                                    <small>{{ $errors->first('new_email') }}</small>
                                                </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12 text-right">
                                        <button type="submit" class="btn btn-info">{{ trans('basic.change') }}</button>
                                        <a href="{{ route('profile.index') }}" class="btn btn-default">{{ trans('basic.cancel') }}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            @endif
        </aside>
    </div>
@endsection

@section('additional_scripts')
    <script src="{{ mix('js/intlTelInput.js') }}"></script>
    <script src="{{ mix('js/intlTelInput_ini.js') }}"></script>
    <script src="{{ mix('js/datepicker.js') }}"></script>
@endsection
