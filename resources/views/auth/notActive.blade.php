@extends('layouts.user_template')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('basic.notActiveTitle') }}</div>
                <div class="panel-body">
                    <p>{{ trans('basic.notActiveText') }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
