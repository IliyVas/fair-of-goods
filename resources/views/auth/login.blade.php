@extends('layouts.auth')

@section('title', trans('auth.loginTitle'))

@section('form')
    <form class="form-signin" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <h2 class="form-signin-heading">{{trans('auth.loginTitle')}}</h2>
        <div class="login-wrap">
            <input type="text" name="email" id="email"
                   class="form-control{{ $errors->has('email') ? ' has-error' : '' }}"
                   placeholder="{{trans('auth.email')}}"
                   value="{{ old('email') }}"
                   required autofocus>

            <input type="password" name="password" id="password"
                   class="form-control{{ $errors->has('password') ? ' has-error' : '' }}"
                   placeholder="{{trans('auth.password')}}"
                   required>
            <label class="checkbox">

            {{--<input type="checkbox" name="remember-me" {{ old('remember') ? 'checked' : '' }}> {{trans('auth.remember')}}--}}

            <span class="form-group pull-right">
                <a data-toggle="modal" href="#myModal"> {{trans('auth.forgot')}}</a>
                {{-- <a href="{{ route('password.request') }}">
                    {{trans('auth.forgot')}}
                </a> --}}
            </span>
            </label>
            <button type="submit" class="btn btn-lg btn-login btn-block">
                {{trans('auth.login')}}
            </button>
            <p>{{trans('auth.socialLogin')}}</p>
            <div class="social-link text-right">
                <a href="{{ url('/auth/google') }}">
                    <img src="{{ asset('img/google.png') }}">
                </a>
                <a href="{{ url('/auth/vkontakte') }}">
                    <img src="{{ asset('img/vk.png') }}">
                </a>
                {{--<a href="{{ url('/auth/facebook') }}">--}}
                    {{--<img src="{{ asset('img/facebook.png') }}">--}}
                {{--</a>--}}
                {{--<a href="{{ url('/auth/instagram') }}">--}}
                    {{--<img src="{{ asset('img/instagram.png') }}">--}}
                {{--</a>--}}
                <a href="{{ url('/auth/odnoklassniki') }}">
                    <img src="{{ asset('img/odnoklassniki.png') }}">
                </a>
            </div>
            <div class="registration">
                {{trans('auth.noAccount')}}
                <a class="" href="{{ route('register') }}">
                    {{trans('auth.createAccount')}}
                </a>
            </div>

        </div>
    </form>

    <!-- Modal -->
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">{{trans('auth.resetPassTitle')}}</h4>
                    </div>

                    <div class="modal-body">
                        <p>{{ trans('auth.resetInviteMsg') }}</p>
                        <input type="text" name="email"
                        placeholder="{{trans('auth.emailResetEmail')}}" autocomplete="off"
                        required
                        class="form-control placeholder-no-fix {{ $errors->has('email') ? ' has-error' : '' }}"
                        value="{{ old('email') }}">
                    </div>

                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">{{ trans('basic.cancel') }}</button>
                        <button class="btn btn-success" type="submit">{{trans('auth.emailResetSend')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal -->
@endsection