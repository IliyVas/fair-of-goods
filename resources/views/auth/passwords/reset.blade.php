@extends('layouts.auth')

@section('title', trans('auth.resetPassTitle'))

@section('form')
    <form class="form-signin" method="POST" action="{{ route('password.reset') }}">
        {{ csrf_field() }}

        <h2 class="form-signin-heading">{{trans('auth.resetPassTitle')}}</h2>
        <div class="login-wrap">
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group">
                <input type="email" name="email" id="email"
                       class="form-control{{ $errors->has('email') ? ' has-error' : '' }}"
                       placeholder="{{ trans('auth.resetPassEmail') }}"
                       value="{{ $email or old('email') }}"
                       required autofocus>
            </div>
            <div class="form-group">
                <input type="password" name="password" id="password"
                       class="form-control{{ $errors->has('password') ? ' has-error' : '' }}"
                       placeholder="{{trans('auth.resetPassPass')}}"
                       required>
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" id="password-confirm"
                       class="form-control{{ $errors->has('password_confirmation') ? ' has-error' : '' }}"
                       placeholder="{{trans('auth.resetPassConfirm')}}"
                       required>
            </div>
            <button type="submit" class="btn btn-lg btn-login btn-block">
                {{trans('auth.resetPassReset')}}
            </button>
            <div class="registration text-right">
                <a href="{{ route('register') }}">
                    {{trans('auth.createAccount')}}
                </a> |
                <a href="{{ route('login') }}">
                    {{trans('auth.login')}}
                </a>
            </div>
        </div>
    </form>
@endsection
