@extends('layouts.auth')

@section('title', trans('auth.registerTitle'))

@section('additional_styles')
    <link rel="stylesheet" href="{{ mix('css/phone.css') }}">
@endsection

@section('form')
    <form class="form-signin" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
        <h2 class="form-signin-heading">{{trans('auth.registerTitle')}}</h2>
        <div class="login-wrap">
            <p>{{trans('auth.regInputData')}}</p>
            <input type="text" name="first_name"
                   autofocus required
                   value="{{ old('first_name') }}"
                   class="form-control{{ $errors->has('first_name') ? ' has-error' : '' }}"
                   placeholder="{{trans('auth.regFstName')}}">

            <input type="text" name="last_name"
                   required
                   value="{{ old('last_name') }}"
                   class="form-control{{ $errors->has('last_name') ? ' has-error' : '' }}"
                   placeholder="{{trans('auth.regLstName')}}">

            <input type="text" name="email"
                   required
                   value="{{ old('email') }}"
                   class="form-control{{ $errors->has('email') ? ' has-error' : '' }}"
                   placeholder="{{trans('auth.regEmail')}}">

            <div class="mtop20">
                <span id="success-msg" class="hide text-success">{{ trans('messages.validPhone') }}</span>
                <span id="error-msg" class="hide text-danger">{{ trans('messages.invalidPhone') }}</span>
            </div>
            <input value="{{ old('phone') }}"
                   type="text"
                   name="phone"
                   id="phone"
                   class="form-control"
                   required>
            <input type="hidden" name="isValidPhone" id="isValidPhone" value="0">

            <input type="password" name="password"
                   required
                   class="form-control{{ $errors->has('password') ? ' has-error' : '' }}"
                   placeholder="{{trans('auth.regPass')}}">

            <input type="password" name="password_confirmation"
                   required
                   class="form-control"
                   placeholder="{{trans('auth.regConfirm')}}">

            <label class="checkbox text-left">
                <input type="checkbox" name="terms"> {!! trans('auth.regAgreement') !!}
            </label>
            <button class="btn btn-lg btn-login btn-block" type="submit">{{trans('auth.regRegBtn')}}</button>

            <div class="registration">
                {{trans('auth.regAlreadyReg')}}
                <a class="" href="{{ route('login') }}">
                    {{trans('auth.login')}}
                </a>
            </div>
        </div>
    </form>
@endsection

@section('additional_scripts')
    <script src="{{ mix('js/intlTelInput.js') }}"></script>
    <script src="{{ mix('js/intlTelInput_ini.js') }}"></script>
@endsection
