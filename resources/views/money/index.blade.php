@extends('layouts.user_template')
@section('title'){{ trans('basic.withdrawMoney') }}@endsection

@section('content')
    <section class="panel">
        <header class="panel-heading">
            {{ trans('basic.withdrawMoney') }}
        </header>
        <div class="panel-body">
            <div class="adv-table">
                <div class="space15"></div>

                <div class="panel-body">
                    {{ trans('basic.withdrawMoneyDesc', ['sum' => htmlspecialchars($user->getAvailableTransactionAmount())]) }} <br>

                    <form action="{{ config('moneta.basic.url') }}/secureCardData.htm" method="GET">
                        <input type="hidden" name="publicId" value="{{ config('moneta.basic.public_id') }}"/>
                        <input type="hidden" name="MNT_ID" value="{{ config('moneta.basic.account.id') }}"/>
                        <input type="hidden" name="redirectUrl" value="{{ route('money.withdraw') }}"/>
                        <input type="hidden" name="secure[PAYEECARDNUMBER]" value="required"/>
                        <input type="hidden" name="secure[CARDEXPIRATION]" value="required"/>
                        <input type="hidden" name="formTarget" value="_top"/>

                        <button {{ $disabled }} type="submit" class="mtop20 btn btn-warning pull-right">
                            {{ trans('basic.withdrawMoney') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection