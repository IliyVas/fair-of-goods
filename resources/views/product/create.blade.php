@extends('layouts.user_template')
@section('title'){{ trans('basic.addItem') }}@endsection

@widget('Product\Form', ['pageName' => trans('basic.createProductTitle')])
