@extends('layouts.user_template')

@widget('Product\Index', [
'products'               => $products,
'editRoute'              => 'products.edit',
'deleteRoute'            => route('products.destroy'),
'showRoute'              => 'products.show',
'showIssueExportBtn'     => true,
'issueExportRoute'       => route('deliveries.create'),
'showCreateItemBtn'      => true,
'createItemRoute'        => route('products.create'),
'downloadPriceListRoute' => route('pricelist.download', ['user' => $user->id])
])