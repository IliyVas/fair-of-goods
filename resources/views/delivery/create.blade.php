@extends('layouts.user_template')

@widget('Delivery\Form', ['delivery'        => $delivery,
                         'pageName'         => trans('basic.toIssueExport'),
                         'selectedProducts' => $selectedProducts,
                         'editing'          => false,
                         'method'           => 'POST',
                         ])
