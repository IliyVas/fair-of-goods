<select name="delivery_time" type="text" class="form-control">
    <option value="7.00-10.00" @if($time === '7.00-10.00') selected @endif>
        7.00-10.00
    </option>
    <option value="10.00-13.00" @if($time === '10.00-13.00') selected @endif>
        10.00-13.00
    </option>
    <option value="13.00-16.00" @if($time === '13.00-16.00') selected @endif>
        13.00-16.00
    </option>
    <option value="16.00-19.00" @if($time === '16.00-19.00') selected @endif>
        16.00-19.00
    </option>
    <option value="19.00-22.00" @if($time === '19.00-22.00') selected @endif>
        19.00-22.00
    </option>
</select>