@extends('layouts.user_template')

@widget('Delivery\Grid', [
'deliveries'           => $deliveries,
'deleteRoute'          => route('deliveries.destroy'),
'showIssueExportBtn'   => true,
'issueExportRoute'     => route('deliveries.create'),
'showProductRouteName' => 'products.show',
])