@extends('layouts.user_template')

@widget('Delivery\Form', [
'delivery' => $delivery,
'route'    => route('deliveries.update', [$delivery->user_id, $delivery->id])
])