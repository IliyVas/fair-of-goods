@extends('layouts.email')

@section('content')
    <tr>
        <td>
            <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560" bgcolor="F1F2F7">
                <tr>
                    <td>
                        <table class="mainContent" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
                            <tbody><tr><td height="20"></td></tr>
                            <tr>
                                <td>
                                    <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0" width="360">
                                        <tbody><tr>
                                            <td  style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                                {{trans('email.titleDeliveryStatus')}}

                                            </td>
                                        </tr>
                                        <tr><td height="15"></td></tr>
                                        <tr>
                                            <td  style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                                {{ trans('email.deliveryStatusText', ['date' => htmlspecialchars(date_create($delivery->delivery_date)->format('d.m.Y'))]) }}
                                            </td>
                                        </tr>
                                        <tr><td height="15"></td></tr>
                                        <tr>
                                            <td style="color: #a4a4a4; line-height: 25px; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">
                                                <ul>
                                                    @foreach($delivery->products as $product)
                                                        <li>{{ $product->name }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>

                                    <table align="left" border="0" cellpadding="0" cellspacing="0">
                                        <tbody><tr><td height="30" width="30"></td></tr>
                                        </tbody>
                                    </table>

                                    <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0">
                                        <tbody><tr><td height="6"></td></tr>
                                        <tr>
                                            <td><a href="" style="width: 128px; display: block;"><img  style="display: block;" src="{{ $message->embed(resource_path('assets/flatlab/img/email-img/image1.png')) }}" alt="image4" class="section-img" height="auto" width="128"></a></td>
                                        </tr>
                                        <tr><td height="10"></td></tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr><td height="20"></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@endsection