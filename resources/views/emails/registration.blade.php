@extends('layouts.email')

@section('content')
    <tr>
        <td>
            <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560" bgcolor="F1F2F7">
                <tr>
                    <td>
                        <table class="mainContent" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
                            <tbody><tr><td height="20"></td></tr>
                            <tr>
                                <td>
                                    <table class="section-item" align="left" border="0" cellpadding="0" cellspacing="0" width="360">
                                        <tbody>
                                        <tr>
                                            <td  style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;">

                                                {{trans('email.subjectActivation')}} {{$user->first_name}}!

                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr><td height="20"></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@endsection