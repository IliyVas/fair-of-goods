{{--Page settings--}}
@section('title'){{  $pageName }}@endsection
@section('additional_styles')
    <link href="{{ mix('css/form.css') }}" rel="stylesheet">
    <link href="{{ mix('css/form.additional.css') }}" rel="stylesheet">

    {{--Map--}}
    <script src="https://api-maps.yandex.ru/2.1/?lang={{ trans('basic.locale') }}" type="text/javascript"></script>
@endsection
@section('additional_scripts')
    <script src="{{ mix('js/form.js') }}"></script>
    <script src="{{ mix('js/datepicker.js') }}"></script>
    <script src="{{ mix('js/form.additional.js') }}"></script>
    <script src="{{ mix('js/delivery-form.js') }}"></script>

    @if(!is_null(old('products'))) @php $selectedProducts = old('products'); @endphp @endif
    @if(!is_null($selectedProducts) && !empty($selectedProducts))
        @foreach($selectedProducts as $item)
            <script>
                $('#{{ $item }}-selectable').trigger('click');
            </script>
        @endforeach
    @endif
@endsection

{{--Main page content--}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                {{--Page name--}}
                <header class="panel-heading">
                    {{ $pageName }}
                </header>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                {{--Tabs block--}}
                @php $onStockForm = (is_null(old('on_stock')) && !$editing || old('on_stock') == "1" || $delivery->on_stock && is_null(old('on_stock'))); @endphp
                {{--Get active tab--}}
                @if($onStockForm) @php $fsActive = ''; $osActive = 'active'; @endphp
                @else @php $osActive = ''; $fsActive = 'active'; @endphp @endif
                {{--Adress--}}
                @if (is_null(old('address')))
                    @php $address = stristr($delivery->address, '|', true); @endphp
                    @if (!$address) @php $address = $delivery->address; @endphp @endif
                @else
                    @php $address = old('address'); @endphp
                @endif
                {{--Apartment--}}
                @if (is_null(old('apartment')))
                    @php $apartment = substr(stristr($delivery->address, '|'), 1); @endphp
                @else
                    @php $apartment = old('apartment'); @endphp
                @endif
                <header class="panel-heading tab-bg-dark-navy-blue">
                    <ul class="nav nav-tabs">
                        <li class="{{ $osActive }}">
                            <a data-toggle="tab" href="#in_stock">{{ trans('basic.onStock') }}</a>
                        </li>
                        <li class="{{ $fsActive }}">
                            <a data-toggle="tab" href="#from_stock">{{ trans('basic.fromStock') }}</a>
                        </li>
                    </ul>
                </header>
                {{--Page content--}}
                <div class="panel-body">
                    <div class="tab-content">
                        @if (count($products) > 0)
                            {{--On stock form--}}
                            <div id="in_stock" class="tab-pane {{ $osActive }}">
                                <form method="post" class="form-horizontal tasi-form"
                                      action="{{ $route }}">
                                    {{ method_field($method) }}
                                    {{ csrf_field() }}
                                    {{--Select product block--}}
                                    <div class="form-group-last">
                                        <select data-search="{{ trans('basic.search') }}" name="products[]"
                                                class="multi-select" multiple="" id="delivery_multi_select" >
                                            @foreach($products as $product)
                                                @php
                                                    $isRegistered = $product->status === ProductStatus::REGISTERED;
                                                    $isOnStock = $product->status === ProductStatus::ON_STOCK;
                                                @endphp

                                                @if ($isRegistered || !$isOnStock && $delivery->on_stock
                                                 && !is_null($selectedProducts) && in_array($product->id, $selectedProducts))
                                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                        {{--Select btns--}}
                                        <div class="mtop20">
                                            <span onclick="$('#delivery_multi_select').multiSelect('select_all');"
                                                  class="btn btn-primary"> {{ trans('basic.addAll') }}
                                            </span>
                                            <span onclick="$('#delivery_multi_select').multiSelect('deselect_all');"
                                                  class="btn btn-warning pull-right"> {{ trans('basic.removeAll') }}
                                            </span>
                                        </div>
                                    </div>

                                    {{--Courier--}}
                                    @if (empty(old()))
                                        @php $courier = $delivery->courier; @endphp
                                    @else
                                        @php $courier = old('courier'); @endphp
                                    @endif
                                    <div class="form-group-last mtop20">
                                        <input id="os-courier" name="courier" type="checkbox" value="0" @if (!$courier) checked @endif>
                                        <label>{{ trans('basic.self-export') }}</label>
                                    </div>
                                    <div class="row mtop20 hidden" id="os_address_container">
                                        @include('templates._delivery_form_os_address_block')
                                    </div>

                                    <div class="row mtop20">
                                        {{--Delivery date--}}
                                        @if (is_null(old('delivery_date')))
                                            @php $date = date_create($delivery->delivery_date)->format('d.m.Y'); @endphp
                                        @else
                                            @php $date = old('delivery_date'); @endphp
                                        @endif
                                        <div class="col-xs-6">
                                            <div class="form-group-last">
                                                <label>{{ trans('basic.deliveryDay1') }}</label>
                                                <input name="delivery_date" type="text" class="form-control default-date-picker"
                                                       id="os-delivery-date" value="{{ $date }}">
                                            </div>
                                        </div>

                                        <input type="hidden" value="1" name="on_stock">

                                        {{--Delivery time--}}
                                        @if (is_null(old('delivery_time')))
                                            @php $time = $delivery->delivery_time; @endphp
                                        @else
                                            @php $time = old('delivery_time'); @endphp
                                        @endif
                                        <div class="col-xs-6">
                                            <div class="form-group-last">
                                                <label>{{ trans('basic.deliveryTime1') }}</label>
                                                @include('delivery._time_select')
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group-last mtop20">
                                        <button id="os-btn" type="submit" class="btn btn-success pull-right">
                                            @if ($editing) {{ trans('basic.btnSaveChanges') }}
                                            @else {{ trans('basic.issue') }} @endif
                                        </button>
                                    </div>
                                </form>
                            </div>

                            {{--From stock form--}}
                            <div id="from_stock" class="tab-pane {{ $fsActive }}">
                                <form method="post" class="form-horizontal tasi-form"
                                      action="{{ $route }}">
                                    {{ method_field($method) }}
                                    {{ csrf_field() }}
                                    {{--Select product block--}}
                                    <div class="form-group-last">
                                        <select data-search="{{ trans('basic.search') }}" name="products[]"
                                                class="multi-select" multiple="" id="delivery_multi_select2" >
                                            @foreach($products as $product)
                                                @php
                                                    $isRegistered = $product->status === ProductStatus::REGISTERED;
                                                    $isOnStock = $product->status === ProductStatus::ON_STOCK;
                                                @endphp

                                                @if ($isOnStock || !$isRegistered && !$delivery->on_stock
                                                 && !is_null($selectedProducts) && in_array($product->id, $selectedProducts))
                                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>

                                        {{--Select btns--}}
                                        <div class="mtop20">
                                              <span onclick="$('#delivery_multi_select2').multiSelect('select_all');"
                                                    class="btn btn-primary"> {{ trans('basic.addAll') }}
                                            </span>
                                            <span onclick="$('#delivery_multi_select2').multiSelect('deselect_all');"
                                                  class="btn btn-warning pull-right"> {{ trans('basic.removeAll') }}
                                            </span>
                                        </div>
                                    </div>

                                    {{--Courier--}}
                                    <div class="form-group-last mtop20">
                                        <input id="fs-courier" name="courier" type="checkbox" value="0" @if (!$courier) checked @endif>
                                        <label>{{ trans('basic.self-export') }}</label>
                                    </div>
                                    <div class="row mtop20 hidden" id="fs_address_container">
                                        @include('templates._delivery_form_fs_address_block')
                                    </div>

                                    <div class="row mtop20">
                                        {{--Delivery date--}}
                                        <div class="col-xs-6">
                                            <div class="form-group-last">
                                                <label>{{ trans('basic.deliveryDay0') }}</label>
                                                <input name="delivery_date" type="text" class="form-control default-date-picker"
                                                       id="fs-delivery-date" value="{{ $date }}">
                                            </div>
                                        </div>

                                        <input type="hidden" value="0" name="on_stock">

                                        {{--Delivery time--}}
                                        <div class="col-xs-6">
                                            <div class="form-group-last">
                                                <label>{{ trans('basic.deliveryTime0') }}</label>
                                                @include('delivery._time_select')
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group-last mtop20">
                                        <button id="fs-btn" type="submit" class="btn btn-success pull-right">
                                            @if ($editing) {{ trans('basic.btnSaveChanges') }}
                                            @else {{ trans('basic.issue') }} @endif
                                        </button>
                                    </div>
                                </form>
                            </div>
                        @else
                            {{ trans('messages.haveNotProducts') }}
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection