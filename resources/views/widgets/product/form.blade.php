@section('additional_styles')
    <link href="{{ mix('css/form.css') }}" rel="stylesheet">
@endsection
@section('additional_scripts')
    <script src="{{ mix('js/form.js') }}"></script>
    <script>
        //step wizard
        $(function() {
            $('#default').stepy({
                backLabel: '{{ trans('basic.btnBack') }}',
                block: true,
                nextLabel: '{{ trans('basic.btnNext') }}',
                titleClick: true,
                titleTarget: '.stepy-tab'
            });
        });

        //custom form elements
        window.onload = function() {
            showConstitutionsAndSizeInputs(); //Constitution and size inputs
            showSecondStep(); //Second step block
        };
    </script>
    <script>
        var imageInput = $('#image'), hasImageInput = $('#has_image');
        imageInput.on('change', function () {
            if (imageInput.val() === '') {
                hasImageInput.val('1');
            } else {
                hasImageInput.val(null);
            }
        });
    </script>
@endsection

{{--Main page content--}}
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                {{--Page name--}}
                <header class="panel-heading">
                    {{ $pageName }}
                </header>
                {{--Page content--}}
                <div class="panel-body">
                    <div class="stepy-tab">
                        <ul id="default-titles" class="stepy-titles clearfix">
                            <li id="default-title-0" class="current-step">
                                <div>{{ trans('basic.step', ['number' => htmlspecialchars(1)]) }}</div>
                            </li>
                            <li id="default-title-1" class="">
                                <div>{{ trans('basic.step', ['number' => htmlspecialchars(2)]) }}</div>
                            </li>
                        </ul>
                    </div>

                    {!! Form::open(['route' => $route, 'files' => true, 'id' => 'default', 'class' => 'form-horizontal']) !!}
                    {{ method_field($method) }}
                    <fieldset title="{{ trans('basic.step', ['number' => htmlspecialchars(1)]) }}" class="step"
                              id="default-step-0">
                        <legend> </legend>
                        @php $name = null; $desc = null; @endphp
                        @if (isset($product))
                            @php $name = $product->name; $desc = $product->description; @endphp
                        @endif

                        {{--Name block--}}
                        <div class="form-group">
                            <span class="col-lg-2">
                                {!! Form::label('name', trans('basic.name'), ['class' => 'control-label']) !!}
                                <span class="text-danger">*</span>
                            </span>
                            <div class="col-lg-10">
                                {!! Form::text('name', $name, ['class' => 'form-control',
                                 'placeholder' =>  trans('basic.name')]) !!}
                                <p class="help-block">{{ trans('basic.nameInputDesc') }}</p>
                            </div>
                        </div>

                        {{--Description block--}}
                        <div class="form-group">
                            {!! Form::label('description', trans('basic.desc'), ['class' => 'control-label col-lg-2']) !!}
                            </span>
                            <div class="col-lg-10">
                                {!! Form::textarea('description', $desc, ['class' => 'form-control', 'rows' => 5,
                                 'placeholder' => trans('basic.desc')]) !!}
                                <p class="help-block">{{ trans('basic.descInputDesc') }}</p>
                            </div>
                        </div>

                        {{--Categories block--}}
                        @if (isset($categories) && count($categories) > 0)
                            <div class="form-group">
                                <span class="col-lg-2">
                                    {!! Form::label('category_id', trans('basic.category'), ['class' => 'control-label']) !!}
                                    <span class="text-danger">*</span>
                                </span>
                                <div class="col-lg-10">
                                    <select onchange="showConstitutionsAndSizeInputs()" id="category" name="category_id" class="form-control m-bot15">
                                        @if (isset($product) && is_null(old('category_id')))
                                            @php $selectedCategory = $product->category_id; @endphp
                                        @else
                                            @php $selectedCategory = old('category_id'); @endphp
                                        @endif

                                        @foreach($categories as $category)
                                            <optgroup label="{{ $category->name }}">
                                                @if (count($category->subcategories) > 0)
                                                    @foreach($category->subcategories as $subcategory)
                                                        <option @if($selectedCategory == $subcategory->id) selected @endif
                                                        value="{{ $subcategory->id }}">
                                                            {{ $subcategory->name }}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </optgroup>
                                        @endforeach
                                    </select>
                                    <p class="help-block">{{ trans('basic.categoryInputDesc') }}</p>
                                </div>
                            </div>
                        @endif

                        {{--Constitution and size block--}}
                        <input type="hidden" id="constitution_old_value" value="{{ old('constitution') }}">
                        <input type="hidden" id="child_age_old_value" value="{{ old('child_age') }}">
                        <input type="hidden" id="size_old_value" value="{{ old('size') }}">
                        <section id="constitution_and_size"></section>

                        {{--Gender block--}}
                        @if (isset($gender) && !is_null($gender) && count($gender->featureValues) > 0)
                            <div class="form-group">
                                <span class="col-lg-2">
                                    {!! Form::label('gender', trans('basic.gender'), ['class' => 'control-label']) !!}
                                    <span class="text-danger">*</span>
                                </span>
                                <div class="col-lg-10">
                                    <select name="gender" class="form-control m-bot15">
                                        @if (is_null(old('gender')) && isset($product))
                                            @php $selectedGender = $product->getFeatureValue(Feature::GENDER, 'id'); @endphp
                                        @else
                                            @php $selectedGender = old('gender'); @endphp
                                        @endif
                                        @foreach($gender->featureValues as $genderValue)
                                            <option @if($selectedGender == $genderValue->id) selected @endif
                                            value="{{ $genderValue->id }}">
                                                {{ $genderValue->content }}</option>
                                        @endforeach
                                    </select>
                                    <p class="help-block">{{ trans('basic.genderInputDesc') }}</p>
                                </div>
                            </div>
                        @endif

                        {{--Image block--}}
                        <div class="form-group">
                            {!! Form::label('image', trans('basic.image'), ['class' => 'col-lg-2 control-label']) !!}
                            <div class="col-lg-10">
                                {!! Form::file('image', ['class' => 'form-control', 'id' => 'image']) !!}
                                @php $hasImage = '1'; @endphp
                                @if (!is_null(old('hasImage')))
                                    @php $hasImage = old('hasImage'); @endphp
                                @endif
                                {!! Form::hidden('hasImage', $hasImage, ['id' => 'has_image']) !!}
                                <p class="help-block">{{ trans('basic.imageInputDesc') }}</p>
                            </div>
                            @if (isset($product)) @php $productImageUrl = $product->getImageUrl(); @endphp
                            @elseif(is_null(old('image'))) @php $productImageUrl = asset('img/no-image-medium.png'); @endphp
                            @else @php $productImageUrl = asset('img/no-image-medium.png'); @endphp @endif
                            <div class="col-lg-offset-2 col-lg-10">
                                <img id="product_img" class="small-img" src="{{ $productImageUrl }}">
                            </div>
                        </div>

                        {{--For sale block--}}
                        <div class="form-group">
                            <span class="col-lg-2">
                                {!! Form::label('forSale', trans('basic.forSale'), ['class' => 'control-label']) !!}
                                <span class="text-danger">*</span>
                            </span>
                            <div class="col-lg-10">
                                @if (is_null(old('for_sale')) && isset($product))
                                    @php $forSale = $product->for_sale; @endphp
                                @else @php $forSale = old('for_sale'); @endphp @endif
                                <select onchange="showSecondStep()" id="forSale" name="for_sale" class="form-control m-bot15">
                                    <option @if($forSale == true) selected @endif value="1">
                                        {{ trans('basic.forSaleAnswerDefault') }}</option>
                                    <option @if($forSale == false) selected @endif value="0">
                                        {{ trans('basic.forSaleAnswer') }}</option>
                                </select>
                                <p class="help-block">{{ trans('basic.forSaleInputDesc') }}</p>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset title="{{ trans('basic.step', ['number' => htmlspecialchars(2)]) }}" class="step" id="default-step-1" >
                        <legend> </legend>
                        {{--Second step block--}}
                        <input type="hidden" id="donate_after_fair_old_value" value="{{ old('donate_after_fair') }}">
                        <input type="hidden" id="institution_old_value" value="{{ old('institution_id') }}">
                        <input type="hidden" id="price_old_value" value="{{ old('price') }}">
                        <input type="hidden" id="product_id" value="{{ $product->id or null }}">
                        <section id="second_step"></section>
                    </fieldset>

                    @if(isset($product)) @php $btn = trans('basic.btnSaveChanges'); @endphp
                    @else @php $btn = trans('basic.btnAddProduct'); @endphp @endif
                    <input type="submit" class="finish btn btn-danger" value="{{ $btn }}"/>
                    {!! Form::close() !!}
                </div>
            </section>
        </div>
    </div>
@endsection
