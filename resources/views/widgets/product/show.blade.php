{{--Page settings--}}
@section('title'){{ $model->name or config('app.name')}}@endsection

{{--Main page content--}}
@section('content')
    <section class="panel">
        <header class="panel-heading text-right">
            <span class="pull-left">
                {{ trans('basic.productPageTitle') }}
            </span>
            {{--<button type="button" id="loading-btn" class="btn btn-warning btn-xs">--}}
                {{--<i class="fa fa-refresh"></i> {{ trans('basic.btnUpdateStatus') }}--}}
            {{--</button>--}}
        </header>
    </section>
    <div class="row">
        <div class="col-md-8">
            <section class="panel">
                <div class="bio-graph-heading project-heading">
                    <strong>{{ $model->name }}</strong>
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row p-details">
                        {{--Category block--}}
                        <div class="bio-row">
                            <p>
                                <span class="bold">{{ trans('basic.category') }}:</span>
                                {{ $model->category->name }}
                            </p>
                        </div>
                        {{--Gender block--}}
                        <div class="bio-row">
                            <p>
                                <span class="bold">{{ trans('basic.gender') }}:</span>
                                {{ $gender or '&mdash;' }}
                            </p>
                        </div>
                        {{--Size block--}}
                        @if(!is_null($size))
                            <div class="bio-row">
                                <p>
                                    <span class="bold">{{ trans('basic.size') }}:</span>
                                    {{ $size }}
                                </p>
                            </div>
                        @endif
                        {{--Child age block--}}
                        @if(!is_null($childAge))
                            <div class="bio-row">
                                <p>
                                    <span class="bold">{{ trans('basic.childAge') }}:</span>
                                    {{ $childAge }}
                                </p>
                            </div>
                        @endif
                        {{--Constitution block--}}
                        @if (!is_null($constitution))
                            <div class="bio-row">
                                <p>
                                    <span class="bold">{{ trans('basic.constitution') }}:</span>
                                    {{ $constitution }}
                                </p>
                            </div>
                        @endif
                        {{--For sale block--}}
                        <div class="bio-row">
                            <p>
                                <span class="bold">{{ trans('basic.shortForSale') }}:</span>
                                @if($model->for_sale){{ trans('basic.yes') }}@else{{ trans('basic.no') }}@endif
                            </p>
                        </div>
                        {{--Revenue block--}}
                        @if (!is_null($model->revenue_percent))
                            <div class="bio-row">
                                <p>
                                    <span class="bold">{{ trans('basic.revenuePercent') }}:</span>
                                    {{ $model->getUserRevenue() . ' ' . trans('basic.rub') }}
                                </p>
                            </div>
                        @endif
                        {{--Donate block--}}
                        <div class="bio-row">
                            <p>
                                <span class="bold">{{ trans('basic.donate') }}:</span>
                                @if(!$model->for_sale) {{ trans('basic.yes') }}
                                @elseif ($model->donate_after_fair) {{ trans('basic.ifNotSold') }}
                                @else{{ trans('basic.no') }}
                                @endif
                            </p>
                        </div>
                        {{--Price block--}}
                        <div class="bio-row">
                            <p>
                                <span class="bold">{{ trans('basic.shortPrice') }}:</span>
                                @if($model->price){{ $model->price . ' ' . trans('basic.rub')}}@else &mdash; @endif
                            </p>
                        </div>
                        {{--Discount block--}}
                        @if (count($model->discounts) > 0)
                            @foreach($model->discounts as $discount)
                                <div class="bio-row">
                                    @php $discountType = $discount->discountType; @endphp
                                    @if (is_null($discountType->finish_day))
                                        @php $trans = trans('basic.discountDescEnd', ['start_day' => htmlspecialchars($discountType->start_day)]); @endphp
                                    @else
                                        @php
                                            $trans = trans('basic.discountDesc', [
                                            'start_day' => htmlspecialchars($discountType->start_day),
                                            'finish_day' => htmlspecialchars($discountType->finish_day)]);
                                        @endphp
                                    @endif
                                    <p>
                                        <span class="bold">{{ $trans }}:</span>
                                        {{ $discount->percent }}%
                                    </p>
                                </div>
                            @endforeach
                        @endif
                        {{--Institution block--}}
                        <div class="bio-row">
                            <p>
                                <span class="bold">{{ trans('basic.shortInstitution') }}:</span>
                                {{ $model->institution->name or '&mdash;' }}
                            </p>
                        </div>
                        {{--Image block--}}
                        <div class="bio-row">
                            <p>
                                <span class="bold">{{ trans('basic.image') }}:</span>
                                <img src="{{ $model->getImageUrl('thumb') }}">
                            </p>
                        </div>
                        {{--Description block--}}
                        <div class="bio-row">
                            <p>
                                <span class="bold">{{ trans('basic.desc') }}:</span>
                                {{ $model->description or '&mdash;' }}
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        {{--Status block--}}
                        <dl class="dl-horizontal mtop20 p-progress form-group">
                            <div class="col-sm-2">
                                <span class="bold">{{ trans('basic.status') }}:</span>
                            </div>
                            <div class="col-sm-10">
                                <div class="progress progress-striped active ">
                                    <div class="progress-bar status-{{ $model->for_sale }}-{{ $model->status }}"></div>
                                </div>
                                <small>{{ trans('basic.' . $model->status . 'ProductStatus') }}</small>
                            </div>
                        </dl>

                        {{--Buttons block--}}
                        @if ($admin)
                            @php $routeVars = [$model->user_id, $model->id]; @endphp
                        @else
                            @php $routeVars = [$model->id]; @endphp
                        @endif
                        <div class="col-xs-12 btn-product-page">
                            @if ($model->status === ProductStatus::REGISTERED)
                                <a href="{{ route($editRoute, $routeVars) }}" type="button"
                                   class="btn btn-success">
                                    <i class="fa fa-pencil"></i> {{ trans('basic.change') }}
                                </a>
                            @endif
                            {{--<button type="button" class="btn btn-primary">--}}
                                {{--<i class="fa fa fa-download"></i> {{ trans('basic.downloadLabel') }}--}}
                            {{--</button>--}}
                            @if ($model->status === ProductStatus::REGISTERED)
                                {{--Delete product block--}}
                                <a href="#confirm{{ $model->id }}" class="btn btn-danger pull-right"
                                   data-toggle="modal"  title="{{ trans('basic.delete')}}">
                                    <i class="fa fa-trash-o"></i> {{ trans('basic.delete')}}
                                </a>
                                @php $delMsg = trans('messages.deleteProductConfirm',['name' => htmlspecialchars($model->name)]); @endphp
                                @include('templates._confirm_single_deletion')
                                <form id="delete_{{ $model->id }}" method="post"
                                      action="{{ route($deleteRoute, ['userId' => $model->user_id]) }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="selected_items[]" value="{{ $model->id }}">
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        </div>

        {{--Last changes block--}}
        <div class="col-md-4">
            <section class="panel">
                <header class="panel-heading">
                    {{ trans('basic.lastChanges') }}
                </header>
                <div class="panel-body">
                    <ul>
                        <li class="m-bot15">
                            <i class="fa fa-circle text-success" aria-hidden="true"></i>
                            <i>
                                {{ date_create($model->created_at)->format('d.m.Y') .
                                ' - ' . trans('basic.created') }}
                            </i>
                        </li>
                        @if (count($model->history) > 0)
                            @foreach($model->history as $change)
                                @if($change->type == ProductHistoryType::CHANGED_DATA)
                                    <li class="m-bot15">
                                        <i class="fa fa-circle text-success" aria-hidden="true"></i>
                                        <i>
                                            {{ date_create($change->created_at)->format('d.m.Y') .
                                            ' - ' . trans('basic.changed') }}
                                        </i>
                                    </li>
                                @elseif($change->type == ProductHistoryType::CHANGED_STATUS)
                                    <li class="m-bot15">
                                        <i class="fa fa-circle text-success" aria-hidden="true"></i>
                                        <i>
                                            {{ date_create($change->created_at)->format('d.m.Y') .
                                            ' - ' . trans('basic.changedStatus',
                                             ['status' => htmlspecialchars(trans('basic.' . $change->content . 'ProductStatus'))]) }}
                                        </i>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                        @if (!is_null($model->soldOutProduct))
                            <li class="m-bot15">
                                <i class="fa fa-circle text-success" aria-hidden="true"></i>
                                <i>
                                    {{ date_create($model->soldOutProduct->sale_date)->format('d.m.Y') .
                                    ' - ' . trans('basic.sold') }}
                                </i>
                            </li>
                        @endif
                    </ul>
                </div>
            </section>
        </div>
    </div>
@endsection