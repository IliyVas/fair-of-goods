{{--Page settings--}}
@section('title'){{ trans('basic.itemsList') }}@endsection
@section('additional_styles')
    <link href="{{ mix('css/grid.css') }}" rel="stylesheet">
@endsection
@section('additional_scripts')
    @php $item =  trans('basic.items'); @endphp
    @include('templates._dynamic_table_trans_script')
    <script type="text/javascript" language="javascript"  src="{{ mix('js/jquery.dataTables.js') }}"></script>
    <script src="{{ mix('js/grid.js') }}"></script>
    @include('templates._product_form_script')
@endsection

{{--Main page content--}}
@section('content')
    @include('templates._product_table')
@endsection
