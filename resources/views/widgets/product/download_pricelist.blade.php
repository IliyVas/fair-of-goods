{{--Page settings--}}
@section('title'){{ trans('basic.downloadPriceList') }}@endsection

{{--Main page content--}}
@section('content')
    <section class="panel">
        <header class="panel-heading text-left">
            <span>
                {{ trans('basic.downloadPriceList') }}
            </span>
        </header>
    </section>
    <section class="panel">
        <div class="panel-body">
            @if(!empty($downloadUrls))
                <ul>
                    @foreach($downloadUrls as $key => $downloadUrl)
                        <li>
                            <a href="{{ $downloadUrl }}">
                                {{ trans('basic.downloadPart', ['num' => htmlspecialchars($key + 1)]) }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </section>
@endsection
