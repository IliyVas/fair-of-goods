<div class="row">

    @include('profile._user_menu')

    <aside class="profile-info col-lg-9 col-sm-10 col-xs-12">
        <section class="panel">
            <div class="bio-graph-heading">
                {{ trans('basic.infoProfile')  }}
            </div>
            <div class="panel-body bio-graph-info">
                <h1>{{ trans('basic.profileTitle')  }}</h1>
                <div class="row">
                    <div class="bio-row">
                        <p>
                            <span>{{ trans('basic.firstName')  }}: </span>{{ $user->first_name or '&mdash;' }}
                        </p>
                    </div>
                    <div class="bio-row">
                        <p>
                            <span>{{ trans('basic.email')  }}: </span>{{ $user->email or '&mdash;' }}
                        </p>
                    </div>
                    <div class="bio-row">
                        <p>
                            <span>{{ trans('basic.lastName')  }}: </span>{{ $user->last_name or '&mdash;' }}
                        </p>
                    </div>
                    <div class="bio-row">
                        <p>
                            <span>{{ trans('auth.regPhone')  }}: </span>{{ $user->phone or '&mdash;' }}
                        </p>
                    </div>
                    <div class="bio-row">
                        <p>
                            <span>{{ trans('basic.birthday')  }}: </span>
                            @if(!is_null($user->birthday)){{ date_create($user->birthday)->format('d.m.Y') }} @else &mdash; @endif
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </aside>
</div>