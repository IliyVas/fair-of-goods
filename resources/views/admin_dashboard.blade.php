@extends('layouts.admin_template')

{{--Page settings--}}
@section('title'){{ trans('basic.dashboard') }}@endsection

{{--Main page content--}}
@section('content')
    <section class="panel">
        <div class="revenue-head">
                <span>
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
            <h3>{{ trans('basic.today') }}</h3>
            <span class="rev-combo pull-right">
                    {{ Carbon::today()->format('j') . ' ' . trans('basic.month' .
                     Carbon::today()->format('M'))}}
                </span>
        </div>
        <div class="panel-body">
            <!--state overview start-->
            <div class="row state-overview mbot30">
                <div class="col-lg-3 col-sm-6">
                    <section class="panel">
                        <div class="symbol terques">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                        </div>
                        <div class="value">
                            <h1 class="count">
                                {{ Fair::getNearestFairDaysCount() }}
                            </h1>
                            <p>{{ trans('basic.beforeFairDays') }}</p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <section class="panel">
                        <div class="symbol red">
                            <i class="fa fa-users" aria-hidden="true"></i>
                        </div>
                        <div class="value">
                            <h1 class=" count2">
                                {{ User::getNumberOfUsers() }}
                            </h1>
                            <p>{{ trans('basic.allUsers') }}</p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <section class="panel">
                        <div class="symbol yellow">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        <div class="value">
                            <h1 class=" count3">
                                {{ Product::getNumberOfProducts() }}
                            </h1>
                            <p>{{ trans('basic.allItems') }}</p>
                        </div>
                    </section>
                </div>
                <div class="mbot30 col-lg-3 col-sm-6">
                    <section class="panel">
                        <div class="symbol blue">
                            <i class="fa fa-truck" aria-hidden="true"></i>
                        </div>
                        <div class="value">
                            <h1 class=" count4">
                                {{ Delivery::getNumberOfDeliveriesToday() }}
                            </h1>
                            <p>{{ trans('basic.todayExports') }}</p>
                        </div>
                    </section>
                </div>
            </div>
            <!--state overview end-->
            <div class="project-heading text-center mtop20">
                <strong>{{ trans('basic.helloAdmin', ['name' => htmlspecialchars(Auth::user()->first_name)]) }}</strong>
            </div>
        </div>
    </section>
@endsection