@extends('layouts.user_template')

{{--Page settings--}}
@section('title'){{ trans('basic.dashboard') }}@endsection
@section('additional_styles')
    {{--Map--}}
    <script src="https://api-maps.yandex.ru/2.1/?lang={{ trans('basic.locale') }}" type="text/javascript"></script>
@endsection
@section('additional_scripts')
    @if (!is_null($cFair)) @php $fair = $cFair; @endphp @else @php $fair = $nFair; @endphp @endif
    @if (!is_null($fair))
        <script>
            ymaps.ready(function () {
                var map = new ymaps.Map("map", {
                    center: [55.76, 37.64],
                    zoom: 10
                });

                var mapData;
                if ('{{ $fair->lat }}' !== '' && '{{ $fair->long }}' !== '') {
                    mapData = [{{ $fair->lat }}, {{ $fair->long }}];
                } else {
                    mapData = '{{ $fair->address }}';
                }

                ymaps.geocode(mapData, {results: 1}).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0),
                        bounds = firstGeoObject.properties.get('boundedBy');

                    //add placemarck
                    firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                    firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());
                    map.geoObjects.add(firstGeoObject);
                    map.setBounds(bounds, {
                        checkZoomRange: true
                    });
                });
            });
        </script>
    @endif
@endsection

{{--Main page content--}}
@section('content')
    @if (!is_null($cFair))
        @php
            $soldOutProducts = Auth::user()->getFairSoldOutProducts($cFair);
            $donatedProducts = Auth::user()->getFairDonatedProducts($cFair);
        @endphp
    @else @php $soldOutProducts = []; $donatedProducts = []; @endphp @endif

    <!--state overview start-->
    <div class="row state-overview">
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol terques">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                </div>
                @if (!is_null($cFair))
                    <div class="value">
                        <h1 class="count">
                            {{ Fair::getCurrentFairDaysCount() }}
                        </h1>
                        <p>{{ trans('basic.untilFairEndDays') }}</p>
                    </div>
                @else
                    <div class="value">
                        <h1 class="count">
                            {{ Fair::getNearestFairDaysCount() }}
                        </h1>
                        <p>{{ trans('basic.beforeFairDays') }}</p>
                    </div>
                @endif
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol yellow">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="value">
                    @if (!is_null($cFair))
                        <h1 class=" count3">
                            {{ count($soldOutProducts) }}
                        </h1>
                    @else
                        <h1 class=" count3">
                            {{ Auth::user()->getNumberOfSoldProducts() }}
                        </h1>
                    @endif
                    <p>{{ trans('basic.soldItems') }}</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol red">
                    <i class="fa fa-smile-o" aria-hidden="true"></i>
                </div>
                <div class="value">
                    @if (!is_null($cFair))
                        <h1 class=" count2">
                            {{ count($donatedProducts) }}
                        </h1>
                    @else
                        <h1 class=" count2">
                            {{ Auth::user()->getNumberOfDonatedProducts() }}
                        </h1>
                    @endif
                    <p>{{ trans('basic.donatedItems') }}</p>
                </div>
            </section>
        </div>
        <div class="col-lg-3 col-sm-6">
            <section class="panel">
                <div class="symbol blue">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>
                @if (!is_null($cFair))
                    <div class="value">
                        <h1 class=" count4">
                            {{ Auth::user()->getCurrentFairRevenue($soldOutProducts) }}
                        </h1>
                        <p>{{ trans('basic.revenue') }}</p>
                    </div>
                @else
                    <div class="value">
                        <h1 class=" count4">
                            {{ Auth::user()->getTotalRevenue() }}
                        </h1>
                        <p>{{ trans('basic.totalRevenue') }}</p>
                    </div>
                @endif
            </section>
        </div>
    </div>
    <!--state overview end-->

    @if (is_null($cFair))
        <div class="row">
            <!--nearest fair block-->
            <div class="col-md-8">
                @include('templates._fair_block')
            </div>

            <!--nearest delivery block-->
            <div class="col-md-4">
                <section class="panel">
                    <div class="revenue-head">
                    <span>
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                    </span>
                        <h3>{{ trans('basic.today') }}</h3>
                        <span class="rev-combo pull-right">
                                 {{ date('j', time()) . ' ' . trans('basic.month' . date('M', time()))}}
                    </span>
                    </div>
                    @php $nearestDelivery = Auth::user()->getNearestDelivery(); @endphp
                    <div class="panel-body">
                        @if (is_null($nearestDelivery))
                            <h4>{{ trans('basic.noNearestCourier') }}</h4>
                        @else
                            <h4>{{ trans('basic.nearestCourier') }}</h4>
                            <h3 class="bold">{{ date_create($nearestDelivery->delivery_date)->format('j') .
                         ' ' . trans('basic.month' . date_create($nearestDelivery->delivery_date)->format('M')) }}
                            </h3>
                            <table class="m-bot15 table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ trans('basic.itemsList') }}</th>
                                </tr>
                                </thead>
                                @php $i = 0; @endphp
                                @foreach($nearestDelivery->products as $product)
                                    @php $i++; @endphp
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td>
                                            <a href="{{ route('products.show', ['id' => $product->id]) }}"
                                               title="{{ trans('basic.view') }}">
                                                {{ $product->name }}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </section>
            </div>
            <!--nearest delivery block end-->
        </div>
    @else
        <section class="panel">
            <div class="revenue-head">
                    <span>
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                    </span>
                <h3>{{ trans('basic.today') }}</h3>
                <span class="rev-combo pull-right">
                    {{ Carbon::today()->format('j') . ' ' .
                     trans('basic.month' . Carbon::today()->format('M'))}}
                </span>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <table class="m-bot15 table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>{{ trans('basic.name') }}</th>
                            <th>{{ trans('basic.saleDate') }}</th>
                            <th>{{ trans('basic.price') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($soldOutProducts) > 0)
                            @foreach($soldOutProducts as $soldOutProduct)
                                <tr>
                                    <td>{{ $soldOutProduct->product->name }}</td>
                                    <td>{{ $soldOutProduct->sale_date }}</td>
                                    <td>{{ $soldOutProduct->price }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="m-bot15 table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ trans('basic.donateItemsList') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($donatedProducts) > 0)
                            @php $i = 0; @endphp
                            @foreach($donatedProducts as $donatedProduct)
                                @php $i++; @endphp
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $donatedProduct->name }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </section>

        <!--current fair block-->
        @include('templates._fair_block')
    @endif

    @if (!is_null($lFair))
        <div class="row">
            <div class="col-md-8">
                <section class="panel">
                    <div class="fair-description-heading">
                        {{ trans('basic.latestFair') . ': ' . $lFair->name . ' (' . date_create($lFair->start_date)->format('d.m.Y') .
                         ' - ' . date_create($lFair->finish_date)->format('d.m.Y') . ')'}}
                    </div>
                    <div class="panel-body">
                        <table class="m-bot15 table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('basic.donateItemsList') }}</th>
                                <th>{{ trans('basic.soldItemsList') }}</th>
                                <th>{{ trans('basic.notSoldItemsList') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    @php $donatedItems = Auth::user()->getFairDonatedProducts($lFair); @endphp
                                    @if (count($donatedItems) > 0)
                                        <ul>
                                            @foreach($donatedItems as $key => $donatedItem)
                                                <li>{{ $key+1 . '. ' . $donatedItem->name }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </td>
                                <td>
                                    @php $soldItems = Auth::user()->getFairSoldOutProducts($lFair); @endphp
                                    @if (count($soldItems) > 0)
                                        <ul>
                                            @foreach($soldItems as $key => $soldItem)
                                                <li>{{ $key+1 . '. ' . $soldItem->product->name }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </td>
                                <td>
                                    @php $notSoldItems = Auth::user()->getFairNotSoldProducts($lFair); @endphp
                                    @if (count($notSoldItems) > 0)
                                        <ul>
                                            @foreach($notSoldItems as $key => $notSoldItem)
                                                <li>{{ $key+1 . '. ' . $notSoldItem->name }}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </div>
    @endif
@endsection