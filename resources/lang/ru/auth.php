<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Эти учетные данные не соответствуют нашим записям.',
    'throttle' => 'Слишком много попыток входа в систему. Повторите попытку через :seconds сек.',
    'logout'   => 'Выйти',
    'profile'  => 'Профиль',

    'email'      => 'Адрес электронной почты',
    'password'   => 'Пароль',
    'remember'   => 'Запомнить меня',
    'login'      => 'Войти',
    'forgot'     => 'Забыли пароль?',
    'loginTitle' => 'Вход',
    'socialLogin'=> 'или выполните вход через социальныую сеть',
    'noAccount'  => 'Ещё нет аккаунта?',
    'createAccount' => 'Создать аккаунт',

    'registerTitle' => 'Регистрация',
    'regFstName'    => 'Имя',
    'regLstName'    => 'Фамилия',
    'regEmail'      => 'E-Mail',
    'regPhone'      => 'Телефон',
    'regPass'       => 'Пароль',
    'regConfirm'    => 'Подтвердите пароль',
    'regRegBtn'     => 'Зарегистрироваться',
    'regInputData'  => 'Введите ваши данные ниже',
    'regAgreement'  => 'Я согласен с <a  target="blank" href="' . asset('docs/policy.pdf') .
        '">политикой конфиденциальности</a> и <a target="blank" href="' . asset('docs/dogovr_komissii.pdf') . '">офертой комиссионного договора</a>',
    'regAlreadyReg' => 'Уже зарегистрирован.',

    'resetPassTitle'   => 'Сброс пароля',
    'resetPassEmail'   => 'E-Mail',
    'resetPassPass'    => 'Пароль',
    'resetPassConfirm' => 'Подтвердить пароль',
    'resetPassReset'   => 'Сбросить пароль',
    'resetInviteMsg'   => 'Введите Ваш e-mail, чтобы сбросить пароль.',

    'emailResetPass'  => 'Сбросить пароль',
    'emailResetEmail' => 'E-Mail',
    'emailResetSend'  => 'Отправить пароль',

    'phone' => 'Введите Ваш номер телефона'
];
