<?php

use App\Http\Controllers\DeliveryController as Delivery;

return [

    'error'       => 'Error! Try again.',
    'success'     => 'Action completed successfully',
    'saveSuccess' => 'Changes saved successfully.',

    //product errors
    'createProductError'            => 'An error occurred while adding a item to the system, try again.',
    'updateProductError'            => 'An error occurred while change ":name", try again.',
    'uploadProductImgError'         => 'An error occurred while uploading a image, try again.',
    'deleteProductError'            => 'An error occurred while deleting ":name", try again.',
    'noProductsSelectedError'       => 'Error! No items selected.',
    'noRightsForDeleteProductError' => 'An error occurred while deleting ":name". You can not delete a item with status ":status".',
    'noRightsForEditProductError'   => 'Error! You can not edit a item with status ":status".',
    'productDoesNotExistError'      => 'Error! The product №:id is not in the database.',
    'notAuthorized'                 => 'You are not authorized for this action',

    //delivery errors
    'createDeliveryError'            => 'An error occurred while processing delivery, try again.',
    'deliveryProductCountError'      => 'An error occurred while processing export/delivery, you can issue delivery/export to ' . Delivery::LIMIT . ' items at a time',
    'createExportError'              => 'An error occurred while processing export, try again.',
    'editDeliveryError'              => 'An error occurred while editing delivery, try again.',
    'editExportError'                => 'An error occurred while editing export, try again.',
    'noRightsForCreateDeliveryError' => 'An error occurred while processing export/delivery. You can not issue delivery/export of a item with the status ":status".',
    'invalidStatusEditError'         => 'An error occurred while editing export/delivery. You can not edit delivery/export of a item with the status ":status".',
    'deleteDeliveryError'            => 'An error occurred while deleting delivery №:id, try again.',
    'noDeliveriesSelectedError'      => 'Error! No deliveries selected.',
    'noRightsForDeleteDeliveryError' => 'An error occurred while deleting delivery №:id. '.
                                        'Removal of delivery is not available, when there is less '.
                                        'than a day left before delivery, and when the delivery has already been committed.',
    'deliveryDoesNotExistError'      => 'Error! The delivery №:id is not in the database.',
    'confirmDeliveryError'           => 'An error occurred while confirmed the delivery, try again.',
    'confirmExportError'             => 'An error occurred while confirmed the export, try again.',
    'noRightsForEditDeliveryError'   => 'Error! Editing delivery/export is not available, when there is less '.
                                        'than a day left before delivery/export, and when the delivery/export has already been committed.',

    //fair errors
    'fairDoesNotExistError' => 'Error! Fair №:id is not in the database.',
    'updateFairError'       => 'An error occurred while editing fair, try again.',

    //product messages
    'createProduct'                 => 'The item ":name" is added to the system successfully. Thank you for using our service!',
    'updateProduct'                 => 'The item ":name" is changed successfully. Thank you for using our service!',
    'deleteProductConfirm'          => 'Are you sure you want to remove ":name"?',
    'deleteSelectedProductsConfirm' => 'Are you sure you want to remove selected items?',
    'deleteProduct'                 => 'The ":name" item was deleted successfully.',
    'haveNotProducts'               => 'You do not have any items yet.',
    'duplicateProduct'              => 'The item ":name" is duplicated successfully. Thank you for using our service!',

    //delivery messages
    'createDelivery'                    => 'The delivery is framed successfully. Thank you for using our service!',
    'createExport'                      => 'The export is framed successfully. Thank you for using our service!',
    'editDelivery'                      => 'The delivery updated successfully. Thank you for using our service!',
    'editExport'                        => 'The export updated successfully. Thank you for using our service!',
    'deleteDeliveryConfirm'             => 'Are you sure you want to remove delivery №:id?',
    'deleteSelectedDeliveriesConfirm'   => 'Are you sure you want to remove selected deliveries?',
    'deleteDelivery'                    => 'The delivery №:id was deleted successfully.',
    'confirmDelivery'                   => 'The delivery №:id was confirmed successfully.',
    'confirmExport'                     => 'The export №:id was confirmed successfully.',
    'filteredDelivery'                  => 'Exports/deliveries filtered',

    //fair messages
    'updateFair' => 'The fair updated successfully.',

    //auth
    'emailActive'    => 'A confirmation letter has been sent to your address.',
    'emailActivated' => 'The account has been confirmed.',
    'emailChanged'   => 'The Email changed successfully.',

    //user messages
    'updatedUser'           => 'User updated success',
    'changedPword'          => 'Password changed success',
    'deletedUser'           => 'User deleted success',
    'errDeletedUser'        => 'Error deleting user',
    'errChangedPword'       => 'Error change password',
    'errUpdatedUser'        => 'Error updating user',
    'createdUser'           => 'User created success',
    'errCreatedUser'        => 'Error created user',
    'deleteUserConfirm'     => 'Are you sure you want to remove user №:id?',
    'filteredUser'          => 'Users filtered',
    'userDoesNotExistError' => 'Error! The user №:id is not in the database.',
    'invalidPhone'          => 'Invalid phone number',
    'validPhone'            => 'Number entered correctly',

    //FAQ messages && errors
    'createQuestionCategory'      => 'Question category added successfully.',
    'createQuestionCategoryError' => 'An error occurred while adding question category, try again.',
    'createQuestion'              => 'Question added successfully.',
    'createQuestionError'         => 'An error occurred while adding question, try again.',
    'updateQuestionCategory'      => 'Question category changed successfully.',
    'updateQuestionCategoryError' => 'An error occurred while editing question category, try again.',
    'updateQuestion'              => 'Question changed successfully.',
    'updateQuestionError'         => 'An error occurred while editing question, try again.',
    'deleteQuestionCategory'      => 'Question category №:id deleted successfully.',
    'deleteQuestionCategoryError' => 'An error occurred while deleting category, try again.',
    'deleteQuestion'              => 'Question №:id deleted successfully.',
    'deleteQuestionError'         => 'An error occurred while deleting question, try again.',
    'deleteQuestionConfirm'       => 'Are you sure you want to remove question №:id?',
    'deleteQuestCategoryConfirm'  => 'Are you sure you want to remove question category №:id?',

    //revenue percent
    'revenueUpdateSuccess' => 'Revenue percent updated successfully.',

    'pricelistError' => 'No products on stock.'

];
