<?php

return [

    'titleActivation'    => 'Activating the registration of a new user',
    'titleConfirmEmail'  => 'Email confirmation',
    'titleDeliveryStatus'=> 'Delivery/export status changed',
    'titleCreateDelivery'=> 'New export is issued',
    'subjectActivation'  => 'Thank you for registering,',
    'deliveryStatusText' => 'Delivery/export on :date approved.',
    'activatedText'      => 'Push the button to complete registration!',
    'confirmEmailText'   => 'Push the button to complete change email!',
    'createDeliveryText' => ':date :user issued export/delivery.',
    'confirm'            => 'Confirm',
    'resetPassText'      => 'You are receiving this email because we received a password reset request for your account.',
    'resetPassFooter'    => 'If you did not request a password reset, no further action is required.',

    'footerText'         => 'Thank you for being with us! The second life of children\'s things!',
    'address'            => '125080, Moscow, Volokolamskoye Highway, 1/1, office 5',

];
