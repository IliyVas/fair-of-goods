<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',
    'old_password'         => 'Enter your current password',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'discount.*' => [
            'numeric' => 'Fields defining the discount must be a number.',
            'min'     => 'Fields defining the discount must be at least :min.',
        ],
        'products' => [
            'required' => 'For issue of export/delivery it is necessary to choose one or several items.',
            'exists' => 'There are no selected items in the database.',
        ],
        'institution_id' => [
            'string' => 'It is necessary to choose the organization to which to transfer the item.',
        ],
        'isValidPhone' => [
            'required' => 'Enter phone number',
            'accepted' => 'Wrong format of entered phone number',
        ],
        'hasImage'     => [
            'required_without' => 'We noticed that you wanted to upload a picture of the thing. Please select an image for the thing again.',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        //product form
        'name'              => '"Name"',
        'description'       => '"Description"',
        'category_id'       => '"Category"',
        'institution_id'    => '"Organization to which to transfer a item"',
        'constitution'      => '"Constitution"',
        'size'              => '"Size"',
        'gender'            => '"For whom the item"',
        'image'             => '"Image"',
        'for_sale'          => '"The item participates in the sale"',
        'donate_after_fair' => '"Donate to charity after the fair"',
        'price'             => '"Price, RUB"',

        //delivery form
        'address'       => '"Address"',
        'apartment'     => '"Apartment"',
        'courier'       => '"Self-export"',
        'delivery_date' => '"Day of export" ("Day of delivery")',
        'delivery_time' => '"Time of export" ("Time of delivery")',

        'first_name'       => 'first name',
        'last_name'        => 'last name',
        'password_confirm' => 'password confirmation',
        'new_email'        => '"New E-mail"',

        //fair form
        'name:en'         => '"Name" (for entering English meaning)',
        'name:ru'         => '"Name" (for entering Russian meaning)',
        'description:en'  => '"Description" (for entering English meaning)',
        'description:ru'  => '"Description" (for entering Russian meaning)',
        'address:en'      => '"Address" (for entering English meaning)',
        'address:ru'      => '"Address" (for entering Russian meaning)',
        'lat'             => '"Latitude"',
        'long'            => '"Longitude"',
        'start_date'      => '"Start date"',
        'finish_date'     => '"Expiration date"',

        //FAQ
        'name_ru'     => 'Name of category in Russian',
        'name_en'     => 'Name of category in English',
        'question_en' => 'Question in English',
        'question_ru' => 'Question in Russian',
        'answer_en'   => 'Answer in English',
        'answer_ru'   => 'Answer in Russian',
        'category'    => 'Question category',
    ],

];
