<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'logout'   => 'Log Out',
    'profile'  => 'Profile',

    'email'      => 'E-Mail Address',
    'password'   => 'Password',
    'remember'   => 'Remember Me',
    'login'      => 'Sign in',
    'forgot'     => 'Forgot Your Password?',
    'loginTitle' => 'Sign in now',
    'socialLogin'=> 'or you can sign in via social network',
    'noAccount'  => 'Don\'t have an account yet?',
    'createAccount' => 'Create an account',

    'registerTitle' => 'Register now',
    'regFstName'    => 'First name',
    'regLstName'    => 'Second name',
    'regEmail'      => 'E-Mail Address',
    'regPhone'      => 'Phone',
    'regPass'       => 'Password',
    'regConfirm'    => 'Confirm Password',
    'regRegBtn'     => 'Register',
    'regInputData'  => 'Enter your personal details below',
    'regAgreement'  => 'I agree with <a  target="blank" href="' . asset('docs/policy.pdf') .
        '">the privacy policy</a> and <a target="blank" href="' . asset('docs/dogovr_komissii.pdf') . '">the offer of the commission agreement</a>',
    'regAlreadyReg' => 'Already Registered.',

    'resetPassTitle'   => 'Reset Password',
    'resetPassEmail'   => 'E-Mail Address',
    'resetPassPass'    => 'Password',
    'resetPassConfirm' => 'Confirm Password',
    'resetPassReset'   => 'Reset Password',
    'resetInviteMsg'   => 'Enter your e-mail address below to reset your password.',

    'emailResetPass'  => 'Reset Password',
    'emailResetEmail' => 'E-Mail Address',
    'emailResetSend'  => 'Send Password Reset Link',

    'phone' => 'Enter your phone number'
];
