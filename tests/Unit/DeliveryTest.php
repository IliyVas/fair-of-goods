<?php

namespace Tests\Unit;

use App\Models\Delivery;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tests\Helper;
use Tests\TestCase;

class DeliveryTest extends TestCase
{
    protected $user;
    protected $product;
    protected $delivery;

    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * Check impossibility of delivery of the product with invalid status.
     *
     * @return void
     */
    public function testImpossibilityOfDeliveryOfProductWithInvalidStatus()
    {
        $this->prepareDatabase();
        $this->product = Helper::addUserProducts(1, ProductStatus::COURIER_EXPECTED)->first();
        $this->user = User::find($this->product->user_id);
        Auth::login($this->user);

        //send request
        $response = $this->call('POST', route('deliveries.store'), [
            'products' => [$this->product->id],
            'address' => 'Test address ' . $this->product->id,
            'delivery_date' => date('Y-m-d', time() + 24*60*60),
            'delivery_time' => '7.00-10.00',
        ]);
        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());
        //check that the delivery was not issued
        $this->assertTrue(is_null($this->user->deliveries()->where('address', 'Test address ' . $this->product->id)->first()));

        Auth::logout();
    }

    /**
     * Check impossibility to remove someone else's deliveries
     *
     * @return void
     */
    public function testImpossibilityToDeleteDeliveryOfSomeoneElse()
    {
        $this->prepareDatabase();
        $this->delivery = Helper::addUserProducts(1, ProductStatus::COURIER_EXPECTED)
            ->first()
            ->deliveries
            ->first();
        $this->user = Helper::createUsers(2)
            ->where('id', '!=', $this->delivery->user_id)
            ->first();
        Auth::login($this->user);

        //send request
        $response = $this->call('POST', route('deliveries.destroy'), [
            'selected_items' => [$this->delivery->id]]);
        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());
        //check that the delivery was not deleted
        $this->assertFalse(is_null(Delivery::find($this->delivery->id)));

        Auth::logout();
    }
}
