<?php

namespace Tests\Unit\Product;

use App\Models\Category\Category;
use App\Models\Feature\Feature;
use App\Models\Institution\Institution;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tests\Helper;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    protected $user;
    protected $product;
    protected $category;
    protected $institution;
    protected $gender;

    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');

        $this->product = Helper::addUserProducts(1, ProductStatus::REGISTERED)->first();
        $this->user = Helper::createUsers(2)
            ->where('id', '!=', $this->product->user_id)
            ->first();
        $this->category = Category::where('parent_id', '!=', null)
            ->where('id', '!=', $this->product->category_id)
            ->get()
            ->random()
            ->id;
        $this->institution = Institution::where('id', '!=', $this->product->institution_id)
            ->get()
            ->random()
            ->id;
        $this->gender = Feature::getFeature(Feature::GENDER)
            ->featureValues()
            ->where('id', '!=', $this->product->getFeatureValue(Feature::GENDER, 'id'))
            ->get()
            ->random()
            ->id;
    }

    /**
     * Check impossibility to change someone else's product.
     *
     * @return void
     */
    public function testImpossibilityToChangeProductOfSomeoneElse()
    {
        $this->prepareDatabase();
        Auth::login($this->user);

        //send request
        $response = $this->call('PATCH', route('products.update', [$this->product->user_id, $this->product->id]),
            [
                'name' => 'Test product name',
                'description' => 'Test product description',
                'category_id' => $this->category,
                'institution_id' => $this->institution,
                'gender' => $this->gender,
                'for_sale' => false,
            ]);
        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());
        //check that the product was not changed
        $product = Product::find($this->product->id);
        $this->assertFalse($product->name == 'Test product name');
        $this->assertFalse($product->description == 'Test product description');
        $this->assertFalse($product->category_id == $this->category);
        $this->assertFalse($product->institution_id == $this->institution);
        $this->assertFalse($product->getFeatureValue(Feature::GENDER, 'id') == $this->gender);

        Auth::logout();
    }

    /**
     * Check admin possibility to change product.
     *
     * @return void
     */
    public function testAdminPossibilityToChangeProduct()
    {
        $this->prepareDatabase();
        //login as admin and check it
        Auth::login(User::find(1));
        $this->assertTrue(Auth::user()->isAdmin());

        //send request
        $response = $this->call('PATCH', route('users.products.update',
            ['userId' => $this->product->user_id, 'productId' => $this->product->id]),
            [
                'name' => 'Test product name',
                'description' => 'Test product description',
                'category_id' => $this->category,
                'institution_id' => $this->institution,
                'gender' => $this->gender,
                'for_sale' => false,
                'hasImage' => '1'
            ]);

        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());

        //check that the product changed
        $product = Product::find($this->product->id);
        $this->assertTrue($product->name == 'Test product name');
        $this->assertTrue($product->description == 'Test product description');
        $this->assertTrue($product->category_id == $this->category);
        $this->assertTrue($product->institution_id == $this->institution);
        $this->assertTrue($product->getFeatureValue(Feature::GENDER, 'id') == $this->gender);

        Auth::logout();
    }
}
