<?php

namespace Tests\Unit\Product;

use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tests\Helper;
use Tests\TestCase;

class RemoveProductTest extends TestCase
{
    protected $user;
    protected $product;

    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * Check impossibility to remove products with inappropriate status
     *
     * @return void
     */
    public function testImpossibilityToDeleteProductsWithInappropriateStatus()
    {
        $this->prepareDatabase();
        $this->product = Helper::addUserProducts(1, ProductStatus::COURIER_EXPECTED)->first();
        $this->user = User::find($this->product->user_id);
        Auth::login($this->user);

        //send request
        $response = $this->call('POST', route('products.destroy'), [
            'selected_items' => [$this->product->id]]);
        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());
        //check that the product was not deleted
        $this->assertFalse(is_null(Product::find($this->product->id)));

        Auth::logout();
    }

    /**
     * Check impossibility to remove someone else's products
     *
     * @return void
     */
    public function testImpossibilityToDeleteProductOfSomeoneElse()
    {
        $this->prepareDatabase();
        $this->product = Helper::addUserProducts(1, ProductStatus::REGISTERED)->first();
        $this->user = Helper::createUsers(2)->where('id', '!=', $this->product->user_id)
            ->first();
        Auth::login($this->user);

        //send request
        $response = $this->call('POST', route('products.destroy'), [
            'selected_items' => [$this->product->id]]);
        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());
        //check that the product was not deleted
        $this->assertFalse(is_null(Product::find($this->product->id)));

        Auth::logout();
    }

    /**
     * Check admin possibility to remove user product.
     *
     * @return void
     */
    public function testAdminPossibilityToDeleteProduct()
    {
        $this->prepareDatabase();
        $this->product = Helper::addUserProducts(1, ProductStatus::REGISTERED)->first();
        $this->user = User::find($this->product->user_id);
        Auth::login(User::find(1));

        //send request
        $response = $this->call('POST', route('users.products.destroy',
            ['userId' => $this->product->user_id]), ['selected_items' => [$this->product->id]]);
        //check that there was a redirect
        $this->assertEquals('302', $response->getStatusCode());
        //check that the product deleted
        $this->assertTrue(is_null(Product::find($this->product->id)));

        Auth::logout();
    }
}
