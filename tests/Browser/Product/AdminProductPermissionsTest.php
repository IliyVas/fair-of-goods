<?php

namespace Tests\Browser\Product;

use App\Models\Category\Category;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use Tests\Browser\Helper;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class AdminProductPermissionsTest extends DuskTestCase
{
    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * Check possibility to view user products list.
     *
     * @return void
     */
    public function testPossibilityToViewUserProductsList()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        $user = Helper::getUser();
        Helper::addUserProducts(15, ProductStatus::REGISTERED);

        $this->browse(function (Browser $browser) use ($user) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit user products list route and check it
                ->visitRoute('users.products.index', [$user->id])
                ->assertSee(trans('basic.itemsList'));
            //check that all user products is visible.
            foreach ($user->products()->orderBy('name')->get()->take(10) as $product) {
                $browser->assertSee($product->name);
            }
            $browser->logout();
        });
    }

    /**
     * Check possibility to view user product.
     *
     * @return void
     */
    public function testPossibilityToViewUserProduct()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        $user = Helper::getUser();
        Helper::addUserProducts(15, ProductStatus::REGISTERED);

        $this->browse(function (Browser $browser) use ($user) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit user products list route and check it
                ->visitRoute('users.products.index', [$user->id])
                ->assertSee(trans('basic.itemsList'));
            //visit product page and check it
            $product = $user->products()->orderBy('name')->first();
            $browser
                ->clickLink($product->name)
                ->assertRouteIs('users.products.show', [$user->id, $product->id])
                ->assertSee($product->name)
                ->logout();
        });
    }

    /**
     * Check possibility to view products list of all users.
     *
     * @return void
     */
    public function testPossibilityToViewProductsList()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        Helper::addUserProducts(15, ProductStatus::REGISTERED);

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit products list route and check it
                ->visitRoute('admin.products.index')
                ->assertSee(trans('basic.itemsList'));
            //check that all products is visible.
            foreach (Product::orderBy('name')->get()->take(10) as $product) {
                $browser->assertSee($product->name);
            }
            $browser->logout();
        });
    }

    /**
     * Check possibility to filter products by category.
     *
     * @return void
     */
    public function testPossibilityToFilterProductsByCategory()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        Helper::addUserProducts(15, ProductStatus::REGISTERED);
        $subcategories = Category::whereNotNull('parent_id')->get();
        $category = Category::getCategories()->random();

        $this->browse(function (Browser $browser) use ($category, $subcategories) {
            Helper::loginAsAdmin($browser);

            $selectedSubCategory = $category->subcategories->random();
            $browser
                //visit products list route and check it
                ->visitRoute('admin.products.index')
                ->assertSee(trans('basic.itemsList'))
                //filter by category and check it
                ->clickLink($category->name)
                ->clickLink($selectedSubCategory->name)
                ->assertSeeIn('#items-table', $selectedSubCategory->name);;
            //check that products with invalid category is hidden.
            foreach ($subcategories as $subcategory) {
                if ($subcategory->id !== $selectedSubCategory->id)
                    $browser->assertDontSeeIn('#items-table', $subcategory->name);
            }
            $browser->logout();
        });
    }

    /**
     * Check possibility to filter products by status.
     *
     * @return void
     */
    public function testPossibilityToFilterProductsByStatus()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        Helper::addUserProducts(5, ProductStatus::REGISTERED);
        Helper::addUserProducts(5, ProductStatus::COURIER_EXPECTED);
        $statuses = ProductStatus::getAllStatuses();
        $status = ProductStatus::REGISTERED;

        $this->browse(function (Browser $browser) use ($status, $statuses) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit products list route and check it
                ->visitRoute('admin.products.index')
                ->assertSee(trans('basic.itemsList'))
                //filter by status and check it
                ->check('statuses[]', $status)
                ->press(trans('basic.filterBtn'))
                ->assertSeeIn('#items-table', trans('basic.' . $status . 'ProductStatus'));
            //check that products with invalid status is hidden.
            foreach ($statuses as $st) {
                if ($status !== $st)
                    $browser->assertDontSeeIn('#items-table', trans('basic.' . $st . 'ProductStatus'));
            }
            $browser->logout();
        });
    }
}
