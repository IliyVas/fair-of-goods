<?php

namespace Tests\Browser\Product;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Helper;

class AddProductTest extends DuskTestCase
{
    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * Check that the product is added after the form submitted.
     */
    public function testIsProductAddedAfterFormSubmitted()
    {
        //Create database migrations before run test.
        $this->prepareDatabase();

        $this->browse(function (Browser $browser) {
            Helper::loginAsUser($browser);

            $browser
                //visit add product route and check it
                ->visitRoute('products.create')
                ->assertSee(trans('basic.createProductTitle'))
                //fill add product form and sent it
                ->type('name', 'Test product name')
                ->type('description', 'Test product description')
                ->clickLink(trans('basic.btnNext'))
                ->select('institution_id', '2')
                ->press(trans('basic.btnAddProduct'))
                //check success message and redirect
                ->assertSee(trans('messages.createProduct', ['name' => 'Test product name']))
                //logout
                ->logout();
        });
    }

    /**
     * Check is correct second step adding product.
     */
    public function testCorrectSecondStepAddingProduct()
    {
        //Create database migrations before run test.
        $this->prepareDatabase();

        $this->browse(function (Browser $browser) {
            Helper::loginAsUser($browser);

            $browser
                //visit add product route and check it
                ->visitRoute('products.create')
                ->assertSee(trans('basic.createProductTitle'))
                //check second step view if select that the product is not for sale
                ->select('for_sale', '0')
                ->clickLink(trans('basic.btnNext'))
                ->assertSee(trans('basic.institution'))
                ->assertDontSee(trans('basic.price'))
                ->assertDontSee(trans('basic.donateAfterFair'))
                ->clickLink(trans('basic.btnBack'))
                //check second step view if select that the product is for sale
                ->select('for_sale', '1')
                ->clickLink(trans('basic.btnNext'))
                ->assertSee(trans('basic.institution'))
                ->assertSee(trans('basic.donateAfterFair'))
                ->assertSee(trans('basic.price'))
                //logout
                ->logout();
        });
    }
}
