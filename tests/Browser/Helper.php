<?php

namespace Tests\Browser;

use Tests\Helper as BasicHelper;

class Helper extends BasicHelper
{
    /**
     * Login as user and check it.
     *
     * @param $browser
     */
    public static function loginAsUser($browser)
    {
       $user = self::getUser();

        $browser
            ->visitRoute('login')
            ->type('email',$user->email)
            ->type('password', self::PASSWORD)
            ->press(mb_strtoupper(trans('auth.login')))
            ->assertRouteIs('dashboard');
    }

    /**
     * Login as admin and check it.
     *
     * @param $browser
     */
    public static function loginAsAdmin($browser)
    {
        $user = self::getUser(true);

        $browser
            ->visitRoute('login')
            ->type('email', $user->email)
            ->type('password', self::PASSWORD)
            ->press(mb_strtoupper(trans('auth.login')))
            ->assertRouteIs('admin.dashboard');
    }
}
