<?php

namespace Tests\Browser;

use App\Models\Delivery;
use App\Models\Product\ProductStatus;
use Carbon\Carbon;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class DeliveryTest extends DuskTestCase
{
    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * Check possibility to issue export.
     *
     * @return void
     */
    public function testPossibilityToIssueExport()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        // get product with registered status
        $product = Helper::addUserProducts(1, ProductStatus::REGISTERED)->first();

        $this->browse(function (Browser $browser) use ($product) {
            Helper::loginAsUser($browser);

            $browser
                // visit create delivery route and check it
                ->visitRoute('deliveries.create')
                ->assertSee(trans('basic.toIssueExport'))
                // fill form and sent it
                ->click('#' . $product->id . '-selectable')
                ->keys('#os-address', 'Test address')
                ->keys('#os-delivery-date', date('d.m.Y', time() + 24*60*60))
                ->click('#os-btn')
                // check message
                ->assertSee(trans('messages.createExport'))
                ->logout();
        });
    }

    /**
     * Check possibility to issue delivery.
     *
     * @return void
     */
    public function testPossibilityToIssueDelivery()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        $product = Helper::addUserProducts(1, ProductStatus::COURIER_EXPECTED)->first();
        $delivery = $product->deliveries()->whereConfirmed(false)->first();

        $this->browse(function (Browser $browser) use ($delivery, $product) {
            Helper::loginAsAdmin($browser);
            $browser
                ->visitRoute('users.deliveries.index',
                    ['userId' => $delivery->user_id, 'deliveryId' => $delivery->id])
                ->assertSee($product->name)
                ->click('#confirm_delivery_' . $delivery->id)
                ->logout();

            Helper::loginAsUser($browser);
            $browser
                // visit create delivery route and check it
                ->visitRoute('deliveries.create')
                ->assertSee(trans('basic.toIssueExport'))
                // open from stock tab
                ->clickLink(trans('basic.fromStock'))
                // fill delivery form and sent it
                ->click('#' . $product->id . '-selectable')
                ->keys('#fs-address', 'Test address')
                ->keys('#fs-delivery-date', date('d.m.Y', time() + 24*60*60))
                ->click('#fs-btn')
                // check message
                ->assertSee(trans('messages.createDelivery'))
                ->logout();
        });
    }

    /**
     * Check possibility to confirm delivery.
     *
     * @return void
     */
    public function testPossibilityToConfirmDelivery()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        // get unconfirmed delivery
        $product = Helper::addUserProducts(1, ProductStatus::COURIER_EXPECTED)->first();
        $delivery = $product->deliveries()->whereConfirmed(false)->first();

        $this->browse(function (Browser $browser) use ($delivery, $product) {
            Helper::loginAsAdmin($browser);

            $browser
                ->visitRoute('users.deliveries.index',
                    ['userId' => $delivery->user_id, 'deliveryId' => $delivery->id])
                ->assertSee($product->name)
                ->click('#confirm_delivery_' . $delivery->id)
                // check message
                ->assertSee(trans('messages.confirmExport', ['id' => $delivery->id]))
                ->logout();
        });
    }

    /**
     * Check possibility to edit delivery.
     *
     * @return void
     */
    public function testPossibilityToEditExport()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        $delivery = Helper::addUserProducts(1, ProductStatus::COURIER_EXPECTED)
            ->first()
            ->deliveries
            ->first();
        $newProduct = Helper::addUserProducts(1, ProductStatus::REGISTERED)
            ->where('status', ProductStatus::REGISTERED)
            ->first();

        $this->browse(function (Browser $browser) use ($delivery, $newProduct) {
            Helper::loginAsUser($browser);
            $browser
                // visit edit delivery route and check it
                ->visitRoute('deliveries.edit', ['id' => $delivery->id])
                ->assertSee(trans('basic.editDelivery'))
                // fill delivery form and sent it
                ->click('#' . $newProduct->id . '-selectable')
                ->clear('delivery_date')
                ->keys('#os-delivery-date', date('d.m.Y', time() + 2*24*60*60))
                ->click('#os-btn')
                // check message and that changes saved
                ->waitForText(trans('messages.editExport'))
                ->assertSeeIn('.ms-selection', $newProduct->name)
                ->assertValue('#os-delivery-date', date('d.m.Y', time() + 2*24*60*60))
                ->logout();
        });
    }

    /**
     * Check possibility to view deliveries list of all users.
     *
     * @return void
     */
    public function testPossibilityToViewDeliveriesList()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        //create five deliveries
        Helper::addUserProducts(5, ProductStatus::COURIER_EXPECTED);

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit deliveries list route and check it
                ->visitRoute('admin.deliveries.index')
                ->assertSee(trans('basic.deliveriesList'))
                ->select('deliveries-table_length', 100);
            //check that all deliveries is visible.
            foreach (Delivery::all() as $delivery) {
                $browser
                    ->assertSee(date_create($delivery->delivery_date)->format('d.m.Y'))
                    ->assertSee($delivery->delivery_time);
            }
            $browser->logout();
        });
    }

    /**
     * Check that deliveries filter works correctly.
     *
     * @return void
     */
    public function testDeliveriesFilterIsCorrectly()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        //create ten deliveries
        Helper::addUserProducts(10, ProductStatus::COURIER_EXPECTED);

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit deliveries list route and check it
                ->visitRoute('admin.deliveries.index')
                ->assertSee(trans('basic.deliveriesList'));
            //check filter by delivery date
            $filter = Delivery::all()->min('delivery_date');
            $browser
                ->type('date_from', date_create($filter)->modify('+1 day')->format('d.m.Y'))
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('admin.deliveries.filter')
                ->select('deliveries-table_length', 100)
                ->assertDontSeeIn('#deliveries-table', date_create($filter)->format('d.m.Y'));
            $filter = Delivery::all()->max('delivery_date');
            $browser
                ->visitRoute('admin.deliveries.index')
                ->assertSee(trans('basic.deliveriesList'))
                ->type('date_to', date_create($filter)->modify('-1 day')->format('d.m.Y'))
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('admin.deliveries.filter')
                ->select('deliveries-table_length', 100)
                ->assertDontSeeIn('#deliveries-table', date_create($filter)->format('d.m.Y'));

            //check filter by type
            $browser
                ->visitRoute('admin.deliveries.index')
                ->assertSee(trans('basic.deliveriesList'))
                ->radio('type', '0')
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('admin.deliveries.filter')
                ->assertDontSeeIn('#deliveries-table', trans('basic.exportType'));

            //check filter by today checkbox
            $browser
                ->visitRoute('admin.deliveries.index')
                ->assertSee(trans('basic.deliveriesList'))
                ->check('today', '1')
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('admin.deliveries.filter');
            foreach (Delivery::all() as $delivery) {
                if ($delivery->delivery_date !== Carbon::today()->format('Y-m-d'))
                    $browser->assertDontSeeIn('#deliveries-table', date_create($delivery->delivery_date)->format('d.m.Y'));
            }
            $browser->logout();
        });
    }
}
