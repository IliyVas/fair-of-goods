<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class FAQTest extends DuskTestCase
{
    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * Check possibility to edit question category.
     *
     * @return void
     */
    public function testPossibilityToEditQuestionCategory()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                ->visitRoute('questions.categories.index')
                ->assertSeeIn('header.panel-heading', trans('basic.questionCategories'))
                ->click('a.edit')
                ->clear('name:ru')
                ->type('name:ru', 'Тестовое названия категории вопроса')
                ->clear('name:en')
                ->type('name:en', 'Test question category name')
                ->click('a.edit')
                ->waitForText(trans('messages.updateQuestionCategory'), 5)
                ->assertSee('Тестовое названия категории вопроса')
                ->assertSee('Test question category name')
                ->logout();
        });
    }

    /**
     * Check possibility to edit question.
     *
     * @return void
     */
    public function testPossibilityToEditQuestion()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                ->visitRoute('questions.index')
                ->assertSeeIn('header.panel-heading', trans('basic.questions'))
                ->click('a.edit')
                ->clear('question:ru')
                ->type('question:ru', 'Тестовый вопрос')
                ->clear('answer:en')
                ->type('answer:en', 'Test answer')
                ->click('a.edit')
                ->waitForText(trans('messages.updateQuestion'), 5)
                ->assertSee('Тестовый вопрос')
                ->assertSee('Test answer')
                ->logout();
        });
    }

    /**
     * Check possibility to delete question.
     *
     * @return void
     */
    public function testPossibilityToDeleteQuestion()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                ->visitRoute('questions.index')
                ->assertSeeIn('header.panel-heading', trans('basic.questions'))
                ->click('a[data-toggle="modal"]')
                ->press(trans('basic.yes'))
                ->assertDontSee(trans('messages.deleteQuestionError'))
                ->logout();
        });
    }

    /**
     * Check possibility to delete question category.
     *
     * @return void
     */
    public function testPossibilityToDeleteQuestionCategory()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                ->visitRoute('questions.categories.index')
                ->assertSeeIn('header.panel-heading', trans('basic.questionCategories'))
                ->click('a[data-toggle="modal"]')
                ->press(trans('basic.yes'))
                ->assertDontSee(trans('messages.deleteQuestionCategoryError'))
                ->logout();
        });
    }

    /**
     * Check possibility to add question category.
     *
     * @return void
     */
    public function testPossibilityToCreateQuestionCategory()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                ->visitRoute('questions.categories.index')
                ->assertSeeIn('header.panel-heading', trans('basic.questionCategories'))
                ->click('#editable-sample_new')
                ->type('name:ru', 'Тестовое название категории')
                ->type('name:en', 'Test category name')
                ->click('a.edit')
                ->waitForText(trans('messages.createQuestionCategory'))
                ->logout();
        });
    }

    /**
     * Check possibility to add question.
     *
     * @return void
     */
    public function testPossibilityToCreateQuestion()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        $this->browse(function (Browser $browser) {
            Helper::loginAsAdmin($browser);

            $browser
                ->visitRoute('questions.index')
                ->assertSeeIn('header.panel-heading', trans('basic.questions'))
                ->click('#editable-sample_new')
                ->type('question:ru', 'Тестовый вопрос')
                ->type('question:en', 'Test question')
                ->type('answer:ru', 'Тестовый ответ')
                ->type('answer:en', 'Test answer')
                ->click('a.edit')
                ->waitForText(trans('messages.createQuestion'))
                ->logout();
        });
    }
}
