<?php

namespace Tests\Browser;

use App\Models\Fair\Fair;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class FairTest extends DuskTestCase
{
    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * Check possibility to edit fair.
     *
     * @return void
     */
    public function testPossibilityToEditFair()
    {
        // create database migrations before run test.
        $this->prepareDatabase();

        //get fair
        $fair = Fair::all()->random();

        $this->browse(function (Browser $browser) use ($fair) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit edit route and check it
                ->visitRoute('fairs.edit', ['id' => $fair->id])
                ->assertSee(trans('basic.editFair', ['name' => $fair->name]))
                //clear text fields
                ->clear('name:ru')
                ->clear('name:en')
                ->clear('description:ru')
                ->clear('description:en')
                ->clear('address:ru')
                ->clear('address:en')
                //fill fields with new data
                ->type('name:ru', 'Измененное имя ярмарки')
                ->type('name:en', 'Changed fair name')
                ->type('description:ru', 'Измененное описание ярмарки')
                ->type('description:en', 'Changed fair description')
                ->type('address:ru', 'Измененный адрес ярмарки')
                ->type('address:en', 'Changed fair address')
                ->type('lat', '58.5555')
                ->type('long', '58.5555')
                ->type('start_date', date('d.m.Y', time() + 3*24*60*60))
                ->type('finish_date', date('d.m.Y', time() + 5*24*60*60))
                ->press(trans('basic.btnSaveChanges'))
                //check message
                ->waitForText(trans('messages.updateFair'))
                //check that entered new data saved
                ->assertInputValue('name:ru', 'Измененное имя ярмарки')
                ->assertInputValue('name:en', 'Changed fair name')
                ->assertInputValue('description:ru', 'Измененное описание ярмарки')
                ->assertInputValue('description:en', 'Changed fair description')
                ->assertInputValue('address:ru', 'Измененный адрес ярмарки')
                ->assertInputValue('address:en', 'Changed fair address')
                ->assertInputValue('lat', '58.5555')
                ->assertInputValue('long', '58.5555')
                ->assertInputValue('start_date', date('d.m.Y', time() + 3*24*60*60))
                ->assertInputValue('finish_date', date('d.m.Y', time() + 5*24*60*60));
        });
    }
}
