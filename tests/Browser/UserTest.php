<?php

namespace Tests\Browser;

use App\Models\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class UserTest extends DuskTestCase
{
    /**
     * Prepare database before run tests.
     */
    private function prepareDatabase()
    {
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * Check possibility to remove user.
     *
     * @return void
     */
    public function testPossibilityToRemoveUser()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        $user = Helper::getUser();

        $this->browse(function (Browser $browser) use ($user) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit users list route and check it
                ->visitRoute('users.index')
                ->assertSee(trans('basic.usersList'))
                //scroll table
                ->driver->executeScript('$(".table-responsive").scrollLeft(800);');
            $browser
                //delete user and check it
                ->click('#del-' . $user->id)
                ->press(trans('basic.yes'))
                ->waitForText(trans('messages.deletedUser'))
                ->assertDontSee($user->email)
                ->logout();
        });
    }

    /**
     * Check possibility to view users list.
     *
     * @return void
     */
    public function testPossibilityToViewUsersList()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        $users = Helper::createUsers(5);

        $this->browse(function (Browser $browser) use ($users) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit users list route and check it
                ->visitRoute('users.index')
                ->assertSee(trans('basic.usersList'));
            //check that all users is visible.
            foreach ($users as $user) {
                $browser->assertSee($user->email);
            }
            $browser->logout();
        });
    }

    /**
     * Check that users filter works correctly.
     *
     * @return void
     */
    public function testUsersFilterIsCorrectly()
    {
        // create database migrations before run test.
        $this->prepareDatabase();
        $users = Helper::createUsers(10);

        $this->browse(function (Browser $browser) use ($users) {
            Helper::loginAsAdmin($browser);

            $browser
                //visit users list route and check it
                ->visitRoute('users.index')
                ->assertSee(trans('basic.usersList'));
            //check filter by name
            $user = $users->random();
            $browser
                ->type('name', $user->first_name . 'test')
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('users.filter')
                ->assertDontSee($user->email);
            //check filter by age
            $user = $users->random();
            $browser
                ->visitRoute('users.index')
                ->type('age_from', $user->getAge() + 1)
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('users.filter')
                ->assertDontSee($user->email)
                ->clear('age_from')
                ->press(trans('basic.filterBtn'))
                ->assertSee($user->email)
                ->type('age_to', $user->getAge() - 1)
                ->press(trans('basic.filterBtn'))
                ->assertDontSee($user->email);
            //check filter by count of products
            $user = $users->random();
            $browser
                ->visitRoute('users.index')
                ->type('products_from', $user->productsCount() + 1)
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('users.filter')
                ->assertDontSee($user->email);
            //check filter by count of donated products
            $user = $users->random();
            $browser
                ->visitRoute('users.index')
                ->type('donate_from', $user->getNumberOfDonatedProducts() + 1)
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('users.filter')
                ->assertDontSee($user->email);
            //check filter by revenue
            $user = $users->random();
            $browser
                ->visitRoute('users.index')
                ->type('sum_from', $user->getTotalRevenue() + 1)
                ->press(trans('basic.filterBtn'))
                ->assertRouteIs('users.filter')
                ->assertDontSee($user->email);
            $browser->logout();
        });
    }
}
