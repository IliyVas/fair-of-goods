<?php

namespace Tests;

use App\Models\Category\Category;
use App\Models\Feature\Feature;
use App\Models\Institution\Institution;
use App\Models\Product\ProductStatus;
use App\Models\Setting;
use App\Models\User;
use Ultraware\Roles\Models\Role;
class Helper
{
    const USER_EMAIL  = 'test-user@test.com';
    const ADMIN_EMAIL = 'test-admin@test.com';
    const PASSWORD    = 'password';

    /**
     * @param $count
     * @param $status
     * @param bool $forSale
     * @return mixed
     */
    public static function addUserProducts($count, $status, $forSale = false)
    {
        $user = self::getUser();
        $categories = Category::getCategories();
        $institutions = Institution::all();

        for ($i = 1; $i <= (int) $count; $i++) {
            $subcategory = $categories->random()->subcategories->random();

            $data = [
                'category_id' => $subcategory->id,
                'name' => str_random(5),
                'description' => str_random(10),
                'institution_id' => $institutions->random()->id,
            ];

            $product = $user->products()->create($data);
            $product->forceFill(['status' => $status])->save();

            //attach constitution
            if ($subcategory->hasFeature(Feature::CONSTITUTION))
                $product->featureValues()->attach(Feature::getFeature(Feature::CONSTITUTION)->featureValues
                    ->random()->id);

            //attach size
            if ($subcategory->hasFeature(Feature::CLOTHES_SIZE))
                $product->featureValues()->attach(Feature::getFeature(Feature::CLOTHES_SIZE)->featureValues
                    ->random()->id);
            elseif ($subcategory->hasFeature(Feature::ACCESSORIES_SIZE))
                $product->featureValues()->attach(Feature::getFeature(Feature::ACCESSORIES_SIZE)->featureValues
                    ->random()->id);
            elseif ($subcategory->hasFeature(Feature::WOMEN_SIZE))
                $product->featureValues()->attach(Feature::getFeature(Feature::WOMEN_SIZE)->featureValues
                    ->random()->id);

            //attach gender
            $product->featureValues()->attach(Feature::getFeature(Feature::GENDER)->featureValues
                ->random()->id);

            if ($product->status === ProductStatus::COURIER_EXPECTED)
                $product->deliveries()->create([
                    'user_id' => $product->user_id,
                    'address' => 'Республика Беларусь, г.Могилев, ул.Первомайская, 20|18',
                    'delivery_date' => date('Y-m-d', time() + (rand(3, 7)*24*60*60)),
                    'delivery_time' => '10.00-13.00'
                ]);

            if ($forSale)
                $product->forceFill([
                    'for_sale' => true,
                    'donate_after_fair' => true,
                    'price' => rand(555,1000),
                    'revenue_percent' => Setting::getRevenuePercent()
                ])->save();
        }

        return $user->products;
    }

    /**
     * Get admin or user model.
     *
     * @param bool $admin
     * @return $this
     */
    public static function getUser($admin = false)
    {
        if ($admin) $email = self::ADMIN_EMAIL;
        else $email = self::USER_EMAIL;

        $user = User::whereEmail($email)->first();

        if (is_null($user)) {
            $user = (new User())->forceFill([
                'active' => true,
                'email' => $email,
                'password' => bcrypt(self::PASSWORD),
                'first_name' => 'Test',
                'phone' => '+79067896093',
            ]);

            $user->save();

            if ($admin) {
                $adminRole= Role::where('name', 'Admin')->first();
                $user->attachRole($adminRole);
            } else
                $user->addUserRole();
        }

        return $user;
    }

    /**
     * @param $count
     * @return \Illuminate\Support\Collection
     */
    public static function createUsers($count)
    {
        for ($i = 1; $i <= $count; $i++)
        {
            $user = (new User())->forceFill([
                'active' => true,
                'email' => str_random(5) . self::USER_EMAIL,
                'password' => bcrypt(self::PASSWORD),
                'first_name' => str_random(8),
                'phone' => '+79067896093',
                'birthday' => date('Y-m-d', time() - rand(18, 50)*365*24*60*60)
            ]);

            $user->save();
            $user->addUserRole();
        }

        return User::where('email', '!=', self::ADMIN_EMAIL)
            ->where('id', '!=', 1)
            ->get();
    }
}
