<?php
use App\Models\faq\QuestionCategory;
use Illuminate\Http\Request;

// auth routes
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register')->name('register');

Route::group(array('as' => 'password.'), function() {
    Route::get('password/reset/{token?}/{email?}', 'Auth\ResetPasswordController@showResetForm')->name('reset');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('reset');
});

Route::get('register/confirm/{token}', 'Auth\RegisterController@confirmEmail');
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('notActive', 'Auth\UserController@active')->name('notActive');
// auth routes end

Route::get('/', function () {
    return redirect()->route('login');
});

Route::group(['middleware' => ['checkActive']], function () {
    Route::resource('profile', 'ProfileController');
    Route::post('profile/pass/{profile}', 'ProfileController@passwords')->name('profile.pass');
    Route::post('profile/email/{profile}', 'ProfileController@email')->name('profile.email');

    Route::resource('profile', 'ProfileController');
    Route::post('profile/pass/{profile}', 'ProfileController@passwords')->name('profile.pass');

    //products
    Route::post('/product/form/category-dependent-block', 'Product\ProductFormController@categoryDependentBlock');
    Route::post('/product/form/second-step', 'Product\ProductFormController@secondStep');
    Route::post('/product/form/institution', 'Product\ProductFormController@institution');

    //phone
    Route::get('phone/form', 'ProfileController@showPhoneForm')->name('phone.form');
    Route::post('phone/save', 'ProfileController@savePhone')->name('phone.save');

    Route::group(['middleware' => ['checkRoute']], function () {
        Route::group(['middleware' => 'role:user'], function () {
            Route::get('/dashboard', function () {
                return view('user_dashboard', [
                    'nFair' => Fair::getNearestFair(),
                    'cFair' => Fair::getCurrentFair(),
                    'lFair' => Fair::getLatestFair(),
                ]);
            })->name('dashboard');

            // products
            Route::post('/products/destroy', 'Product\ProductController@destroy')->name('products.destroy');
            Route::patch('/users/{user}/products/{product}', 'Product\ProductController@update')->name('products.update');
            Route::resource('products', 'Product\ProductController', ['except' => ['destroy', 'update']]);
            Route::get('/user/{user}/pricelist/download', 'Product\ProductController@downloadPriceList')->name('pricelist.download');
            Route::get('/{user}/products/{product}/duplicate', 'Product\ProductController@duplicate')->name('products.duplicate');

            // deliveries
            Route::post('/deliveries/destroy', 'DeliveryController@destroy')->name('deliveries.destroy');
            Route::patch('/users/{user}/deliveries/{delivery}', 'DeliveryController@update')->name('deliveries.update');
            Route::post('/deliveries/create', 'DeliveryController@create')->name('deliveries.create');
            Route::resource('deliveries', 'DeliveryController', ['except' => ['show', 'destroy', 'update']]);

            //withdraw money
            Route::get('/money/withdraw', 'MoneyController@withdraw')->name('money.withdraw');
            Route::get('/money/index', 'MoneyController@index')->name('money.index');

            //FAQ
            Route::resource('faq', 'FAQController', ['only' => ['index']]);
        });

        Route::group(['middleware' => 'role:admin', 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
            Route::get('/dashboard', function () {
                return view('admin_dashboard', ['fair' => Fair::getNearestFair()]);
            })->name('admin.dashboard');

            //settings
            Route::get('settings/revenue_percent/update', 'SettingController@globalUpdateRevenuePercent')->name('settings.revenuePercent.update');
            Route::resource('settings', 'SettingController', ['only' => ['index', 'update']]);

            //deliveries list of the user
            Route::post('/users/{user}/deliveries/destroy', 'Users\DeliveryController@destroy')->name('users.deliveries.destroy');
            Route::get('users/{userId}/deliveries/{deliveryId}/confirm', 'Users\DeliveryController@confirm')
                ->name('users.deliveries.confirm');
            Route::any('/users/{user}/deliveries/create', 'Users\DeliveryController@create')->name('users.deliveries.create');
            Route::resource('users.deliveries', 'Users\DeliveryController', ['only' => ['index', 'edit', 'update', 'store']]);
            //full deliveries list
            Route::patch('/{user}/deliveries/{delivery}', 'DeliveryController@update')->name('admin.deliveries.update');
            Route::post('/deliveries/destroy', 'DeliveryController@destroy')->name('admin.deliveries.destroy');
            Route::resource('deliveries', 'DeliveryController', [
                'only' => ['index', 'edit'],
                'names' => [
                    'index'  => 'admin.deliveries.index',
                    'edit'   => 'admin.deliveries.edit'
                ]
            ]);
            Route::any('deliveries/filter', 'DeliveryController@filter')->name('admin.deliveries.filter');

            //products of the user
            Route::get('/users/{id}/products/download', 'Users\ProductController@download')->name('users.products.download');
            Route::post('/users/{userId}/products/destroy', 'Users\ProductController@destroy')->name('users.products.destroy');
            Route::any('/users/{user}/products/create', 'Users\ProductController@create')->name('users.products.create');
            Route::resource('users.products', 'Users\ProductController', ['except' => ['destroy', 'create']]);
            Route::get('/users/{user}/pricelist/download', 'Users\ProductController@downloadPriceList')->name('users.pricelist.download');
            Route::get('/users/{user}/products/{product}/duplicate', 'Users\ProductController@duplicate')->name('users.products.duplicate');
            //full products list
            Route::get('/products/download', 'ProductController@download')->name('products.download');
            Route::post('products/destroy', 'ProductController@destroy')->name('admin.products.destroy');
            Route::patch('/user/{user}/products/{product}', 'ProductController@update')->name('admin.products.update');
            Route::resource('products', 'ProductController', [
                'only' => ['index', 'show', 'edit'],
                'names' => [
                    'index'  => 'admin.products.index',
                    'show'   => 'admin.products.show',
                    'edit'   => 'admin.products.edit'
                ]
            ]);
            Route::get('products/filter/categories/{category}', 'ProductController@filterByCategory')->name('admin.products.filter.category');
            Route::any('products/filter/statuses', 'ProductController@filterByStatus')->name('admin.products.filter.status');
            Route::any('products/confirm/transfer', 'ProductController@confirm')->name('admin.products.confirm');
            Route::get('/user/{user}/products/{product}/duplicate', 'ProductController@duplicate')->name('admin.products.duplicate');

            //fairs
            Route::resource('fairs', 'FairController', ['only' => ['index', 'edit', 'update']]);

            //users
            Route::get('users/download', 'UserController@download')->name('users.download');
            Route::any('users/filter', 'UserController@filter')->name('users.filter');
            Route::get('users/{user}/money/withdraw/confirm', 'UserController@confirmWithdrawMoney')->name('users.confirmWithdrawMoney');
            Route::resource('users', 'UserController', ['only' => ['index', 'show', 'destroy']]);

            //FAQ
            Route::group(['prefix' => 'faq'], function () {
                Route::post('/questions/categories/select', function (Request $request) {
                    return view('admin.faq.question_category._select', [
                        'selected' => $request->category_id, 'categories' => QuestionCategory::all()
                    ])->render();
                });
                Route::resource('questions', 'faq\QuestionController', [
                    'except' => ['create', 'edit', 'show']
                ]);

                Route::group(['prefix' => 'questions'], function () {
                    Route::resource('categories', 'faq\QuestionCategoryController', [
                        'except' => ['create', 'edit', 'show'],
                        'names' => ['index' => 'questions.categories.index']
                    ]);
                });
            });
        });
    });
});
